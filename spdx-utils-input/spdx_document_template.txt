SPDXVersion: SPDX-<SPDXVersion>
DataLicense: CC0-1.0
SPDXID: SPDXRef-DOCUMENT
DocumentName: <PackageName>-<PackageVersion>
DocumentNamespace: http://spdx.org/spdxdocs/spdx-v<SPDXVersion>-<UUID>
Creator: Person: <SpdxCreatorPerson>
Creator: Tool: <SpdxCreatorTool>
Created: <Created>
CreatorComment: <text>This document (LICENSE.spdx) was semi-automatically generated using the toolchain from gitlab.com/emb_std/spdx-utils,
which makes use of the PCRLT tool from gitlab.com/emb_std/pcrlt-validator with backup of the REUSE tool. Afterwards it was manually adjusted/enriched. 
The tool was executed with the following relevant options:<PcrltValidatorExecutionOptions> 
(Dep5First means: if a file is targeted by multiple PCRLT/REUSE file headers, the dep5 file header will take precedence)
</text>
PackageName: <PackageName>
SPDXID: SPDXRef-package-<PackageName>
PackageVersion: <PackageVersion>
PackageFileName: <PackageName>-<PackageVersion>.zip
PackageDownloadLocation: git+https://gitlab.com/emb_std/pcrlt-validator.git
PackageVerificationCode: <PackageVerificationCode> (excludes: <PackageVerificationCodeExcludedFiles>)
PackageLicenseConcluded: MIT OR Apache-2.0 OR GPL-3.0-or-later
PackageLicenseInfoFromFiles: 0BSD
PackageLicenseInfoFromFiles: Apache-2.0
PackageLicenseInfoFromFiles: CC0-1.0
PackageLicenseInfoFromFiles: CC-BY-4.0
PackageLicenseInfoFromFiles: CC-BY-ND-4.0
PackageLicenseInfoFromFiles: GPL-3.0-or-later
PackageLicenseInfoFromFiles: MIT
PackageLicenseDeclared: MIT OR Apache-2.0 OR GPL-3.0-or-later
PackageLicenseComments: <text>The package is compliant with PCRLT (https://gitlab.com/emb_std/pcrlt) and REUSE (https://reuse.software/spec/) - i.e. copyright and license information are provided on a per-file basis.
The expression in PackageLicenseConcluded is the condensed form of the simplest package license options for the licensee (See below for further explanation).
This means that the licensee can choose any license from the available options. E.g. "MIT".
The expression in PackageLicenseDeclared is copied from the pom.xml file (Whereby, per the addendum, it is stated that "The expression is not an independent normative statement, but instead is the concluded package license based on the license information of the individual files.").
See README.md for further compliance assistance.

<AllFilesSummary>
The following license expressions were found:
<LicenseExprTable>The 15 CC-BY-ND-4.0 licensed files refer to license files in the LICENSES folder and in test resources.
Similarly, the CC-BY-ND-4.0 license in the license expression "(MIT AND CC0-1.0 AND CC-BY-ND-4.0) OR.." refers to license files in the .git.zip archives.

To determine the PackageLicenseConcluded the following process was used (simplified):
1. Concatenate the different license expressions with AND, resulting in a very long expression.
Note that the "AND" operator does not usually mean that the terms from all the combined licenses must be applied to every file in the package.
Mostly it means that the files have different licenses and they can be applied separately.
2. Simplify the expression by removing unnecessary options (terms linked with OR) and removing duplicates.
This leads to the expression "MIT AND CC0-1.0 AND CC-BY-ND-4.0 AND (MIT AND CC0-1.0 AND CC-BY-ND-4.0)"
3. Remove license expression parts resulting from files in the LICENSES folder (and other normative files) since licenses of normative files are usually not considered in the PackageLicenseConcluded.
A problem in this repository is, that those license files also occur in the test resource folder and also in the corresponding zip archives. 
This leads to the expression "MIT AND CC0-1.0 AND CC-BY-ND-4.0" or "MIT AND CC0-1.0", if the license files are considered normative files even though they are part of the test folder.
4. Remove license expression parts without additional obligations compared with the remaining parts. Since CC0-1.0 seems to have no additional obligations when combined with MIT, it can be removed. This leads to the "MIT" expression.
5. Check that the concluded license terms are possible to comply with. Since the expression contains only one term there is no problem.
6. Repeat the process to add other possible options. This leads to "MIT OR Apache-2.0 OR GPL-3.0-or-later".
</text>
PackageCopyrightText: gitlab.com/emb_std/pcrlt-validator contributors and possibly others


<AllFileInfos>

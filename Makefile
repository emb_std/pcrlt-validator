# SPDX-FileCopyrightText: 2021-2022 embeach
# 
# SPDX-License-Identifier: CC0-1.0 OR 0BSD OR MIT OR Apache-2.0
# 
# Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
# Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
# Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
# 
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#Makefile
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/spdx-utils.git@master#Makefile
# 
# Note-PCRLT:emb.7:specify-identity-name:embeach
# Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#Makefile
# Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Makefile
# 
# Note-PCRLT:CC0-1.0 OR 0BSD OR MIT OR Apache-2.0:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"
install:
	mvn install -DskipTests

download-spdx-utils-main:
	if [ -d "spdx-utils-main" ]; then echo "The folder already exists. A download does not seem to be necessary." && false ; fi
	#clone the spdx-utils repository (if spdx-utils-main does not exists)
	git clone https://gitlab.com/emb_std/spdx-utils.git tmp_spdx-utils
	#checkout specific tag version
	cd tmp_spdx-utils && git checkout tags/v0.1.3
	#copy the needed main folder
	cp -a tmp_spdx-utils/spdx-utils-main spdx-utils-main
	#the following will copy the spdx-utils config into the spdx-utils-main folder as spdx-utils.version to give a hint about the original used spdx-utils version
	cp tmp_spdx-utils/spdx-utils-input/config spdx-utils-main/spdx-utils.version
	#remove the repository
	rm -rf tmp_spdx-utils

reset-expected-test-results:
	if [ -d "tmp_test_repo1" ]; then echo "Found tmp_test_repo1" && cp tmp_test_repo1/spdx-utils-output/pcrlt.json src/test/resources/test_repo1/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo1/spdx-utils-output/pcrlt.txt src/test/resources/test_repo1/spdx-utils-output/pcrlt_expected.txt ; fi
	if [ -d "tmp_test_repo1b" ]; then echo "Found tmp_test_repo1b" && cp tmp_test_repo1b/spdx-utils-output/pcrlt.json src/test/resources/test_repo1b/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo1b/spdx-utils-output/pcrlt.txt src/test/resources/test_repo1b/spdx-utils-output/pcrlt_expected.txt ; fi
	if [ -d "tmp_test_repo1c" ]; then echo "Found tmp_test_repo1c" && cp tmp_test_repo1c/spdx-utils-output/pcrlt.json src/test/resources/test_repo1c/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo1c/spdx-utils-output/pcrlt.txt src/test/resources/test_repo1c/spdx-utils-output/pcrlt_expected.txt && cp tmp_test_repo1c/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo1c/spdx-utils-output/pcrlt_expected.spdx; fi
	if [ -d "tmp_test_repo2" ]; then echo "Found tmp_test_repo2" && cp tmp_test_repo2/spdx-utils-output/pcrlt.json src/test/resources/test_repo2/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo2/spdx-utils-output/pcrlt.txt src/test/resources/test_repo2/spdx-utils-output/pcrlt_expected.txt ; fi
	if [ -d "tmp_test_repo2b" ]; then echo "Found tmp_test_repo2b" && cp tmp_test_repo2b/spdx-utils-output/pcrlt.json src/test/resources/test_repo2b/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo2b/spdx-utils-output/pcrlt.txt src/test/resources/test_repo2b/spdx-utils-output/pcrlt_expected.txt ; fi
	if [ -d "tmp_test_repo3" ]; then echo "Found tmp_test_repo3" && cp tmp_test_repo3/spdx-utils-output/pcrlt.json src/test/resources/test_repo3/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo3/spdx-utils-output/pcrlt.txt src/test/resources/test_repo3/spdx-utils-output/pcrlt_expected.txt ; fi
	if [ -d "tmp_test_repo4" ]; then echo "Found tmp_test_repo4" && cp tmp_test_repo4/spdx-utils-output/pcrlt.json src/test/resources/test_repo4/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo4/spdx-utils-output/pcrlt.txt src/test/resources/test_repo4/spdx-utils-output/pcrlt_expected.txt ; fi
	if [ -d "tmp_test_repo5" ]; then echo "Found tmp_test_repo5" && cp tmp_test_repo5/spdx-utils-output/pcrlt.json src/test/resources/test_repo5/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo5/spdx-utils-output/pcrlt.txt src/test/resources/test_repo5/spdx-utils-output/pcrlt_expected.txt && cp tmp_test_repo5/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo5/spdx-utils-output/pcrlt_expected.spdx ; fi
	if [ -d "tmp_test_repo6" ]; then echo "Found tmp_test_repo6" && cp tmp_test_repo6/spdx-utils-output/pcrlt.json src/test/resources/test_repo6/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo6/spdx-utils-output/pcrlt.txt src/test/resources/test_repo6/spdx-utils-output/pcrlt_expected.txt && cp tmp_test_repo6/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo6/spdx-utils-output/pcrlt_expected.spdx ; fi
	if [ -d "tmp_test_repo7" ]; then echo "Found tmp_test_repo7" && cp tmp_test_repo7/spdx-utils-output/pcrlt.json src/test/resources/test_repo7/spdx-utils-output/pcrlt_expected.json && cp tmp_test_repo7/spdx-utils-output/pcrlt.txt src/test/resources/test_repo7/spdx-utils-output/pcrlt_expected.txt && cp tmp_test_repo7/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo7/spdx-utils-output/pcrlt_expected.spdx && cp tmp_test_repo7/spdx-utils-output/pcrlt.csv src/test/resources/test_repo7/spdx-utils-output/pcrlt_expected.csv ; fi
	if [ -d "tmp_test_repo8_Dep5First" ]; then echo "Found tmp_test_repo8_Dep5First" && cp tmp_test_repo8_Dep5First/spdx-utils-output/pcrlt.json src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_Dep5First.json && cp tmp_test_repo8_Dep5First/spdx-utils-output/pcrlt.txt src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_Dep5First.txt && cp tmp_test_repo8_Dep5First/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_Dep5First.spdx ; fi
	if [ -d "tmp_test_repo8_Dep5First_skip" ]; then echo "Found tmp_test_repo8_Dep5First_skip" && cp tmp_test_repo8_Dep5First_skip/spdx-utils-output/pcrlt.json src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_Dep5First_skip.json && cp tmp_test_repo8_Dep5First_skip/spdx-utils-output/pcrlt.txt src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_Dep5First_skip.txt && cp tmp_test_repo8_Dep5First_skip/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_Dep5First_skip.spdx ; fi
	if [ -d "tmp_test_repo8_OneHeaderPerFile" ]; then echo "Found tmp_test_repo_OneHeaderPerFile" && cp tmp_test_repo8_OneHeaderPerFile/spdx-utils-output/pcrlt.json src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_OneHeaderPerFile.json && cp tmp_test_repo8_OneHeaderPerFile/spdx-utils-output/pcrlt.txt src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_OneHeaderPerFile.txt && cp tmp_test_repo8_OneHeaderPerFile/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_OneHeaderPerFile.spdx ; fi
	if [ -d "tmp_test_repo8_OneHeaderPerFile_skip" ]; then echo "Found tmp_test_repo_OneHeaderPerFile_skip" && cp tmp_test_repo8_OneHeaderPerFile_skip/spdx-utils-output/pcrlt.json src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_OneHeaderPerFile_skip.json && cp tmp_test_repo8_OneHeaderPerFile_skip/spdx-utils-output/pcrlt.txt src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_OneHeaderPerFile_skip.txt && cp tmp_test_repo8_OneHeaderPerFile_skip/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo8/spdx-utils-output/pcrlt_expected_OneHeaderPerFile_skip.spdx ; fi
	if [ -d "tmp_test_repo10_OneHeaderPerFile" ]; then echo "Found tmp_test_repo10_OneHeaderPerFile" && cp tmp_test_repo10_OneHeaderPerFile/spdx-utils-output/pcrlt.json src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_OneHeaderPerFile.json && cp tmp_test_repo10_OneHeaderPerFile/spdx-utils-output/pcrlt.txt src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_OneHeaderPerFile.txt && cp tmp_test_repo10_OneHeaderPerFile/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_OneHeaderPerFile.spdx ; fi
	if [ -d "tmp_test_repo10_OneHeaderPerFile_skip" ]; then echo "Found tmp_test_repo10_OneHeaderPerFile_skip" && cp tmp_test_repo10_OneHeaderPerFile_skip/spdx-utils-output/pcrlt.json src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_OneHeaderPerFile_skip.json && cp tmp_test_repo10_OneHeaderPerFile_skip/spdx-utils-output/pcrlt.txt src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_OneHeaderPerFile_skip.txt && cp tmp_test_repo10_OneHeaderPerFile_skip/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_OneHeaderPerFile_skip.spdx ; fi
	if [ -d "tmp_test_repo10_Dep5First" ]; then echo "Found tmp_test_repo10_Dep5First" && cp tmp_test_repo10_Dep5First/spdx-utils-output/pcrlt.json src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_Dep5First.json && cp tmp_test_repo10_Dep5First/spdx-utils-output/pcrlt.txt src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_Dep5First.txt && cp tmp_test_repo10_Dep5First/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_Dep5First.spdx ; fi
	if [ -d "tmp_test_repo10_Dep5First_skip" ]; then echo "Found tmp_test_repo10_Dep5First_skip" && cp tmp_test_repo10_Dep5First_skip/spdx-utils-output/pcrlt.json src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_Dep5First_skip.json && cp tmp_test_repo10_Dep5First_skip/spdx-utils-output/pcrlt.txt src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_Dep5First_skip.txt && cp tmp_test_repo10_Dep5First_skip/spdx-utils-output/pcrlt.spdx src/test/resources/test_repo10/spdx-utils-output/pcrlt_expected_Dep5First_skip.spdx ; fi

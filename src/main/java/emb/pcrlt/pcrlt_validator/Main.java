//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/Main.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/Main.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Main.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Repository;
import org.spdx.library.InvalidSPDXAnalysisException;
import org.spdx.tools.OnlineToolException;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.IVersionProvider;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import emb.helper.HelperI;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;
import emb.pcrlt.pcrlt_validator.model.Dep5FileHeaderAttributes;
import emb.pcrlt.pcrlt_validator.model.ExposeExtended;
import emb.pcrlt.pcrlt_validator.model.FileHeader;
import emb.pcrlt.pcrlt_validator.model.FileHeaderReadOnly;
import emb.pcrlt.pcrlt_validator.model.FileStatistic;
import emb.pcrlt.pcrlt_validator.model.Issue;
import emb.pcrlt.pcrlt_validator.view.ReportHelper;
import emb.pcrlt.pcrlt_validator.view.SPDXHelper;
import emb.pcrlt.pcrlt_validator.view.FileHeaderCsv;

@Command(versionProvider = Main.ManifestVersionProvider.class)
public class Main implements Callable<Integer>, MainLight {

  @Parameters(index = "0", description = "the git repository dir.")
  private String repositoryDir;

  @Option(names = { "-h", "--help" }, usageHelp = true, description = "display a help message")
  private boolean helpRequested = false;

  @Option(names = { "-V",
      "--version" }, versionHelp = true, description = "Print version info from the pcrlt-validator.jar file's /META-INF/MANIFEST.MF and exit")
  boolean versionRequested;

  @Option(names = { "-m",
      "--max-files" }, description = "estimation of the maximal number of files the respository contains. Its a safety parameter to detect path issues and limit damages. Default 30")
  private int maxFiles = 30;

  @Option(names = { "-a", "--all" }, description = "like --spdxdoc --validation --create-csv")
  private boolean all = false;

  @Option(names = { "-v",
      "--validation" }, description = "parse and validate PCRLT annotations and create a json and a report file.")
  private boolean validation = false;

  @Option(names = { "-s", "--spdxdoc" }, description = "generate the spdx document template.")
  private boolean spdx = false;

  @Option(names = { "-c",
      "--create-csv" }, description = "Create csv report where each line represents a targeted file.")
  private boolean isCsv = false;

  @Option(names = {
      "--extendedJson" }, description = "Create extended version of the JSON report (the file name will be the JSON report file name with \".extended.json\" extension).")
  private boolean isExtendedJson = false;

  @Option(names = {
      "--pcrlt-report-file-out" }, description = "File name the PCRLT report should be saved in. Default is spdx-utils-output/pcrlt.txt (relative to target repository dir).")
  private String pcrltReportFile = "spdx-utils-output/pcrlt.txt";

  @Option(names = {
      "--spdx-file-out" }, description = "File name the generated SPDX document should be saved in. Default is spdx-utils-output/pcrlt.spdx (relative to target repository dir).")
  private String spdxDocFile = "spdx-utils-output/pcrlt.spdx";

  @Option(names = {
      "--pcrlt-json-file-out" }, description = "File name the parsed PCRLT annotations should be saved in (in JSON format). Default is spdx-utils-output/pcrlt.json (relative to target repository dir).")
  private String pcrltJsonFile = "spdx-utils-output/pcrlt.json";

  @Option(names = {
      "--pcrlt-csv-file-out" }, description = "File path and name of the PCRLT csv file. Default is spdx-utils-output/pcrlt.csv (relative to target repository dir).")
  private String pcrltCsvFile = "spdx-utils-output/pcrlt.csv";

  @Option(names = {
      "--multiple-file-header-policy" }, description = "Policy how the application should handle multiple file headers per file. Default is 'oneHeaderPerFile', which throws an exception if more than one is found. Another option is 'Dep5First', which results in dep5 headers taking precedence.")
  private String mfhPolicyStr = "OneHeaderPerFile";

  private MFHPolicy mfhPolicy = MFHPolicy.OneHeaderPerFile;

  @Option(names = {
      "--skip-parsing-unused-file-headers" }, description = "Skip parsing of file headers, that will not be added due to the multi-file-header-policy. This might reduce/hide some issues in combination with Dep5First policy.")
  private boolean skipParsingUnused = false;

  @Option(names = {
      "--package-version" }, description = "Sets the package version of the target repository (this will be used for the spdx file generation). Default is \"unknown-version\"")
  private String packageVersion = null;

  @Option(names = {
      "--package-name" }, description = "Sets the package name of the target repository (this will be used for the spdx file generation). Default is the .reuse/dep5 upstream name if available, otherwise the directory name of the target repository.")
  private String packageName = null;

  @Option(names = {
      "--spdx-creator-person" }, description = "Set name and optional contact of the creator of the spdx document for the target repository package (this will be used for the spdx file generation). Default is \"anonymous ()\".")
  private String spdxCreatorPerson = "anonymous ()";

  @Option(names = {
      "--skip-files-for-verification-code" }, description = "Java regex to exclude files from spdx document verification code (this will be used for the spdx file generation). Default is LICENSE.spdx .")
  private String skipFilesRegexForSpdxVerificationCode = "LICENSE.spdx";

  @Option(names = {
      "--max-file-header-size" }, description = "Max file Header size as number of lines. This can be used if the contents of the file contain one of the PCRLT or REUSE keywords at the beginning of a line. Default is unlimited.")
  private Integer maxFileHeaderSize = null;

  @Option(names = {
      "--skip-files-for-parsing" }, description = "Java regex to exclude files from parsing (this is recommended if the file might contain REUSE or PCRLT keywords without a file header). Default is '.*[.]spdx' .")
  private String skipFilesRegexForParsing = ".*[.]spdx";
  
  @Option(names = {
  "--spdx-version" }, description = "SPDX version used in the SPDX document (output file). The validator will raise issues, if a different version is found in source files. Default is 2.3.")
  private String spdxVersion = "2.3";
  
  /**
   * the path to the spdx_document_template.txt relative to the target repository
   */
  private final String SPDX_DOC_TEMPLATE_PATH="spdx-utils-input/spdx_document_template.txt";

  @Expose
  private final Map<String, FileHeader> fileHeaders = new TreeMap<>();

  @ExposeExtended
  @Expose
  private final Map<String, FileStatistic> fileStatistics = new TreeMap<>();

  @Expose
  private final List<Issue> issues = new ArrayList<>();

  private static MainLight main;

  Main() {
    Main.init(this);
  }

  public static void init(final MainLight main) {
    Main.main = main;
  }

  public static boolean isInit() {
    return Main.main != null;
  }

  public static void main(final String[] args)
      throws IOException, OnlineToolException, InvalidSPDXAnalysisException, NoSuchAlgorithmException, ParseException {
    final int exitCode = new CommandLine(new Main()).execute(args);
    System.exit(exitCode);

  }

  @Override
  public Integer call() throws Exception {
    // step1: parse args
    // step2: validate pcrlt if not skipped
    // step2.1: check if file is text file and has header
    // step2.2: parse header (check for unknown keywords etc.)
    // step2.3: validate header (check for advanced rules)
    // step3: create spdx doc

    // List<String> files=HelperI.DefaultHelper.helper.getFilenames(repositoryDir);
    final Git git = Git.open(new File(this.repositoryDir));
    final Repository repo = git.getRepository();
    if (!(this.validation || this.spdx || this.all)) {
      System.out.println("nothing to do. type --help for help.");
    }
    if (this.all || this.validation || this.spdx || this.isCsv) {
      this.mfhPolicy = MFHPolicy.valueOf(this.mfhPolicyStr);
      FileHeaderParser.parseFileHeaders(repo, this.mfhPolicy, this.skipParsingUnused);
      Main.consolidatePackageName(this);
      ValidationAndScoringHelper.valdiateAndScore(this);
      ValidationAndScoringHelper.validateDep5Attributes(this);
      FileStatisticHelper.createFileStatistic(this.fileStatistics, repo);
      if (this.all || this.isCsv)
        FileHeaderCsv.writeCsv(this.getFileHeadersReadOnly(), this.fileStatistics,
            this.repositoryDir + "/" + this.pcrltCsvFile);
      if (this.all || this.spdx) {
        String spdxDocTemplateStr=HelperI.DefaultHelper.helper.getFileAsString(this.repositoryDir + "/"+this.SPDX_DOC_TEMPLATE_PATH);
        SPDXHelper.generateSpdxDoc(repo, this.repositoryDir, this.maxFiles, this.spdxDocFile,spdxDocTemplateStr, this);
      }
      if (this.all || this.validation) {
        ReportHelper.generateReport(this.repositoryDir, this, this.pcrltReportFile);
        HelperI.DefaultHelper.helper.writeFile(this.repositoryDir + "/" + this.pcrltJsonFile, this.toString());
      }
      if (this.isExtendedJson) {
        HelperI.DefaultHelper.helper.writeFile(this.repositoryDir + "/" + this.pcrltJsonFile + ".extended.json",
            this.toStringExtended());
      }
    }

    return 0;
  }

  private static void consolidatePackageName(final Main main) {
    String dep5UpstreamName = null;
    final FileHeaderReadOnly fhDep5 = main.getFileHeadersReadOnly().get(FileHeaderParser.DEP5_FILE_PATH);
    if (fhDep5 != null) {
      final Dep5FileHeaderAttributes fhDep5Attrs = fhDep5.getDep5Attributes();
      if (fhDep5Attrs != null) {
        if (fhDep5Attrs.getUpstreamName() != null) {
          dep5UpstreamName = H.strip(fhDep5Attrs.getUpstreamName().getText());
        }
      }
    }
    if (main.packageName == null || main.packageName.strip().isEmpty()) {
      if (dep5UpstreamName != null && !dep5UpstreamName.strip().isEmpty()) {
        main.packageName = dep5UpstreamName;
      }
    } else {
      if (dep5UpstreamName != null && !dep5UpstreamName.strip().isEmpty()) {
        if (!main.packageName.contentEquals(dep5UpstreamName)) {
          final String issueText = "dep5 Upstream-Name differs from CLI packageName";
          final Issue iss = new Issue(FileHeaderParser.DEP5_FILE_PATH, issueText, Issue.Severity.WARNING, null,
              Issue.BlockedBadgeLevel.L3);
          System.out.println("" + iss);
          main.addIssue(iss);
        }
      }
    }
    if (main.packageName == null || main.packageName.strip().isEmpty()) {
      main.packageName = new File(main.getRepositoryDir()).getName();
    }
  }

  @Override
  public void createAndLogIssue(final Issue.Severity level, final ParCtx ctx, final String text) {// not static because
                                                                                               // main access
    final Issue iss = new Issue(ctx.file, text, level, ctx.lino);
    if (level == Issue.Severity.ERROR) {
      System.err.println("" + iss);
    } else {
      System.out.println("" + iss);
    }
    this.addIssue(iss);
  }

  @Override
  public void addIssue(final Issue issue) {
    this.issues.add(issue);
  }

  @Override
  public String toString() {
    return new GsonBuilder().setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation()
        .setExclusionStrategies(new ExclusionStrategy() {
          @Override
          public boolean shouldSkipField(final FieldAttributes f) {
            return f.getAnnotation(ExposeExtended.class) != null;// false;
          }

          @Override
          public boolean shouldSkipClass(final Class<?> incomingClass) {
            return false;
          }
        }).create().toJson(this);
  }

  @Override
  public String toStringExtended() {
    return new GsonBuilder().setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation().create()
        .toJson(this);
  }

  static MainLight getMainLight() {
    return Main.main;
  }

  static MainLightLight getMainLightLight() {
    return Main.main;
  }

  public static IssueLogger getIssueLogger() {
    return Main.main;
  }

  @Override
  public List<Issue> getIssues() {
    return this.issues;
  }

  @Override
  public Map<String, FileHeader> getFileHeaders() {
    return this.fileHeaders;
  }

  @Override
  public Map<String, FileHeaderReadOnly> getFileHeadersReadOnly() {
    final Map<String, FileHeaderReadOnly> fileHeadersRO = new LinkedHashMap<>();
    this.fileHeaders.entrySet().stream().forEachOrdered(f -> {
      fileHeadersRO.put(f.getKey(), f.getValue());
    });
    return fileHeadersRO;
  }

  @Override
  public Map<String, FileStatistic> getFileStatistics() {
    return this.fileStatistics;
  }

  @Override
  public String getRepositoryDir() {
    return this.repositoryDir;
  }

  @Override
  public MFHPolicy getMfhPolicy() {
    return this.mfhPolicy;
  }

  @Override
  public boolean getSkipParsingUnused() {
    return this.skipParsingUnused;
  }

  @Override
  public String getPackageVersion() {
    return this.packageVersion;
  }

  @Override
  public String getPackageName() {
    return this.packageName;
  }

  @Override
  public String getSpdxCreatorPerson() {
    return this.spdxCreatorPerson;
  }

  @Override
  public String getSkipFilesRegexForSpdxVerificationCode() {
    return this.skipFilesRegexForSpdxVerificationCode;
  }

  @Override
  public Integer getMaxFileHeaderSize() {
    return this.maxFileHeaderSize;
  }

  @Override
  public String getSkipFilesRegexForParsing() {
    return this.skipFilesRegexForParsing;
  }
  
  public String getSpdxVersion() {
    return this.spdxVersion;
  }

  public boolean isCsv() {
    return this.isCsv;
  }

  public static enum MFHPolicy {
    OneHeaderPerFile, Dep5First;
  }

  /**
   * based on
   * source:https://github.com/remkop/picocli/blob/main/picocli-examples/src/main/java/picocli/examples/VersionProviderDemo2.java
   * 
   * @author unknown
   *
   */
  public static class ManifestVersionProvider implements IVersionProvider {
    @Override
    public String[] getVersion() throws Exception {
      final Enumeration<URL> resources = CommandLine.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
      while (resources.hasMoreElements()) {
        final URL url = resources.nextElement();
        try {
          final Manifest manifest = new Manifest(url.openStream());
          // System.out.println(manifest);
          // System.out.println(manifest.getMainAttributes().entrySet().stream().map(e->""+e.getKey()+":"+e.getValue()).collect(Collectors.joining()));
          if (ManifestVersionProvider.isApplicableManifest(manifest)) {
            final Attributes attr = manifest.getMainAttributes();
            return new String[] { "" + ManifestVersionProvider.get(attr, "Implementation-Title"),
                "" + ManifestVersionProvider.get(attr, "Implementation-Version") };
          }
        } catch (final IOException ex) {
          return new String[] { "Unable to read from " + url + ": " + ex };
        }
      }
      return new String[0];
    }

    private static boolean isApplicableManifest(final Manifest manifest) {
      final Attributes attributes = manifest.getMainAttributes();
      return "pcrlt-validator".equals(ManifestVersionProvider.get(attributes, "Implementation-Title"));
    }

    private static Object get(final Attributes attributes, final String key) {
      return attributes.get(new Attributes.Name(key));
    }
  }
}

interface MainLight extends MainLightLight {
  Map<String, FileHeader> getFileHeaders();

  String getSkipFilesRegexForParsing();

  String toStringExtended();
}

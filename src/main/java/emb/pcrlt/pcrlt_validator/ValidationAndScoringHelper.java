//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/ValidationAndScoringHelper.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/ValidationAndScoringHelper.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding ValidationAndScoringHelper.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import emb.pcrlt.pcrlt_validator.model.Dep5FileHeaderAttributes;
import emb.pcrlt.pcrlt_validator.model.FileHeader;
import emb.pcrlt.pcrlt_validator.model.FileHeaderReadOnly;
import emb.pcrlt.pcrlt_validator.model.Issue;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityRepo;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup;
import emb.pcrlt.pcrlt_validator.model.PcrltSpec;
import emb.pcrlt.pcrlt_validator.model.PcrltSpec.PcrltSpecType;
import emb.pcrlt.pcrlt_validator.model.PcrltSrc;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH;
import emb.pcrlt.pcrlt_validator.model.Issue.Severity;

/**
 * The purpose of this class is to help with validation and scoring of the
 * FileHeaders. This includes Feature-Scores, Feature-Score-Lists and estimated
 * Compliance-Scores. See VALIDATION.md for an overview.
 * 
 * @author emb
 *
 */
public class ValidationAndScoringHelper {
  private ValidationAndScoringHelper() {

  }

  static void validateDep5Attributes(final MainLight main) {
    final IssueLogger log = Main.getIssueLogger();
    final Map<String, FileHeaderReadOnly> fileHeaders = main.getFileHeadersReadOnly();
    final FileHeaderReadOnly fhDep5 = main.getFileHeadersReadOnly().get(FileHeaderParser.DEP5_FILE_PATH);
    if (fhDep5 != null) {
      final Dep5FileHeaderAttributes fhDep5Attrs = fhDep5.getDep5Attributes();
      if (fhDep5Attrs != null) {
        if (fhDep5Attrs.getSource() != null) {
          final String source = fhDep5Attrs.getSource().getText();
          if (FileHeader.getAllResolveGitIdentityRepoUrls(fileHeaders).stream().filter(s -> s.contains(source))
              .count() < 1
              && FileHeader.getAllSrcTraceRepoUrls(fileHeaders).stream().filter(s -> s.contains(source)).count() < 1) {
            log.addIssue(new Issue(FileHeaderParser.DEP5_FILE_PATH,
                "dep5 source annotation is not contained in any known url (Source:" + source
                    + ")(known are all parsed from the pcrlt src-trace-repository and resolve-git-identity annotations)",
                Issue.Severity.INFO, fhDep5Attrs.getSource().parCtx.lino));// TODO check also src trace (without repo)
          }
        } else {
          log.addIssue(new Issue(FileHeaderParser.DEP5_FILE_PATH,
              "no dep5 source annotation (Source:.*) found - this is optional", Issue.Severity.INFO,
              fhDep5Attrs.getSource().parCtx.lino, Issue.BlockedBadgeLevel.L3));//TODO check if optional and blockedBadgeLevel.L3 is consistent
        }
        if (fhDep5Attrs.getUpstreamName() != null) {
          final String name = fhDep5Attrs.getUpstreamName().getText();
          if (FileHeader.getAllResolveGitIdentityRepoUrls(fileHeaders).stream().filter(s -> s.contains(name))
              .count() < 1
              && FileHeader.getAllSrcTraceRepoUrls(fileHeaders).stream().filter(s -> s.contains(name)).count() < 1) {
            log.addIssue(new Issue(FileHeaderParser.DEP5_FILE_PATH,
                "dep5 name annotation is not contained in any known url (Upstream-Name:" + name
                    + ")(known are all parsed from the pcrlt src-trace-repository and resolve-git-identity annotations)",
                Issue.Severity.INFO, fhDep5Attrs.getSource().parCtx.lino));// TODO check also src trace (without repo)
          }
        } else {
          log.addIssue(new Issue(FileHeaderParser.DEP5_FILE_PATH,
              "no dep5 name annotation (Upstream-Name:.*) found - this is optional", Issue.Severity.INFO,
              fhDep5Attrs.getSource().parCtx.lino));
        }
      } else {
        log.addIssue(new Issue(FileHeaderParser.DEP5_FILE_PATH,
            "no dep5 attributes (Upstream-Name,Upstream-Contact,Source) found - this is optional", Issue.Severity.INFO,
            null));// this might be unreachable
      }
      // fhDep5Attributes.//TODO check if other attributes can be found
    }

  }

  static void _validateAndCalcFeatureScore(final MainLight main) {// Feature-Score
    final Map<String, FileHeader> fileHeaders = main.getFileHeaders();
    for (final Entry<String, FileHeader> efh : fileHeaders.entrySet()) {
      final FileHeader fh = efh.getValue();
      Integer score = 0;
      final List<Integer> line = new ArrayList<>();
      if (fh != null) {
        if (H.isNotNullAndNotEmpty(fh.getReuseRightsHs())) {
          score += 128;
          line.add(H.distinctUsingEqualsIdentity(fh.getReuseRightsHs()).size());// RightsHolders
          ValidationAndScoringHelper._checkForIssuesInRightsHs(fh.getTargetFile(),fh.getReuseRightsHs());
        } else {
          line.add(0);
          final String issueText = "" + fh.getTargetFile()
              + " does not contain ReuseRightsHs (annotations) (header.type=" + fh.getType() + ")";
          System.out.println(issueText);
          main.addIssue(
              new Issue(fh.getTargetFile(), issueText, Issue.Severity.WARNING, null, Issue.BlockedBadgeLevel.L1));
        }
        if (H.isNotNullAndNotEmpty(fh.getReuseLicenseExprs())) {
          score += 64;
          line.add(fh.getReuseLicenseExprs().size());// Licenses
        } else {
          line.add(0);
          final String issueText = "" + fh.getTargetFile()
              + " does not contain reuseLicenseExprs (annotations) (header.type=" + fh.getType() + ")";
          System.out.println(issueText);
          main.addIssue(
              new Issue(fh.getTargetFile(), issueText, Issue.Severity.WARNING, null, Issue.BlockedBadgeLevel.L1));
        }
        if (H.isNotNullAndNotEmpty(fh.getPcrltSpecs())) {
          score += 32;
          line.add(fh.getPcrltSpecs().size());// Specifications(SPDX,REUSE,PCRLT)
        } else {
          line.add(0);
        }
        if (H.isNotNullAndNotEmpty(fh.getPcrltSrcs())) {
          score += 16;
          line.add(fh.getPcrltSrcs().size());// Sources/Urls
        } else {
          line.add(0);
        }
        if (H.isNotNullAndNotEmpty(fh.getPcrltIdentityGroups())) {
          score += 8;
          line.add(fh.getPcrltIdentityGroups().size());// IdentityGroups
        } else {
          line.add(0);
          final String issueText = "" + fh.getTargetFile()
              + " does not contain pcrltIdentities (annotations) (header.type=" + fh.getType() + ")";
          System.out.println(issueText);
          main.addIssue(new Issue(fh.getTargetFile(), issueText, Issue.Severity.INFO, null));
        }
        if (H.isNotNullAndNotEmpty(fh.getPcrltIdentityGroups())) {
          long count = fh.getPcrltIdentityGroups().values().stream().filter(v -> v.getRightsH2CommitMapping() != null)
              .count();
          if (count > 0) {
            score += 4;
          }
          line.add((int) count);// RightsHolders2CommitMappings
          count = fh.getPcrltIdentityGroups().values().stream()
              .filter(v1 -> H.isNotNullAndNotEmpty(v1.getPcrltIdentities()) && v1.getPcrltIdentities().values().stream()
                  .filter(v2 -> v2 != null && H.isNotNullAndNotEmpty(v2.getRepos())).count() > 0)
              .count();// count identityGroups which contain at least one identity with at least one
                       // repo
          if (count > 0) {
            score += 2;
          }
          line.add((int) count);// IdentityGroups with repository
          for (final PcrltIdentityGroup pig : fh.getPcrltIdentityGroups().values()) {
            for (final PcrltIdentity pi : pig.getPcrltIdentities().values()) {
              if (!pi.getRepos().isEmpty()) {
                boolean isRepoMatchUrl = false;
                boolean isRepoMatchFile = false;
                for (final PcrltIdentityRepo repo : pi.getRepos()) {
                  if (ValidationAndScoringHelper._isDep5Repository(repo.getRepoUrl())) {
                    isRepoMatchUrl = true;
                    if (repo.getFilePath() != null && repo.getFilePath().contentEquals(fh.getTargetFile())) {
                      isRepoMatchFile = true;
                    }
                  }
                }
                if (!isRepoMatchUrl) {
                  final String issueText = "dep5 Source annotation URL is not contained in any resolve-git-identity annotation (pcrltIdentity.name="
                      + pi.getName() + ", repositories:"
                      + pi.getRepos().stream().map(r -> r.getText()).collect(Collectors.joining(","));
                  final Issue iss = new Issue(efh.getKey(), issueText, Issue.Severity.INFO, null,
                      Issue.BlockedBadgeLevel.NONE);
                  System.out.println("" + iss);
                  main.addIssue(iss);
                }
                if (isRepoMatchUrl && !isRepoMatchFile) {
                  final String issueText = "The target file path is not contained in any resolve-git-identity annotation that matches the dep5 Source annotation URL (pcrltIdentity.name="
                      + pi.getName() + ", targetFile=" + fh.getTargetFile() + ", repositories:"
                      + pi.getRepos().stream().map(r -> r.getText()).collect(Collectors.joining(","));
                  final Issue iss = new Issue(efh.getKey(), issueText, Issue.Severity.INFO, null,
                      Issue.BlockedBadgeLevel.NONE);
                  System.out.println("" + iss);
                  main.addIssue(iss);
                }
              }
            }
          }
          count = fh.getPcrltIdentityGroups().values().stream()
              .filter(v -> H.isNotNullAndNotEmpty(v.getPcrltIdentities()) && v.getPcrltIdentities().values().stream()
                  .filter(v2 -> v2 != null && H.isNotNullAndNotEmpty(v2.getAuthorAnnotations())).count() > 0)
              .count();// count identityGroups which contain at least one identity with at least one
                       // authorAnnotation
          if (count > 0) {
            score += 2;
          }
          line.add((int) count);// IdentityGroups with authorAnnotation
        } else {
          line.add(0);
          line.add(0);
          line.add(0);
        }
        if (H.isNotNullAndNotEmpty(fh.getPcrltLicenseExprs())) {
          final long count = fh.getPcrltLicenseExprs().values().stream()
              .filter(v -> v.getPcrltLicense2CommitMapping() != null).count();
          if (count > 0) {
            score += 1;
          }
          line.add((int) count);// License2CommitMappings
        } else {
          line.add(0);
        }
        fh.setScore(score);
        fh.setScoreList(line);
      } else {
        final String issueText = "" + efh.getKey() + " is not targeted by any file header (std,.license,dep5)";
        System.out.println(issueText);
        main.addIssue(new Issue(efh.getKey(), issueText, Issue.Severity.WARNING, null, Issue.BlockedBadgeLevel.L1));
      }
    }
  }

  private static void _checkForIssuesInRightsHs(String file,List<ReuseRightsH> reuseRightsHs) {
    Collection<ReuseRightsH> distinctIdentityRHs=H.distinctUsingEqualsIdentity(reuseRightsHs);
    Collection<ReuseRightsH> distinctIdentityAndYearExprRHs=H.distinctUsingCustomEquals(reuseRightsHs, (a,b)->a.equalsIdentityAndYearExpr(b));
    if(distinctIdentityRHs.size()!=distinctIdentityAndYearExprRHs.size()) {
      final String issueText = "there seems to be multiple annotations with same rights holder (name,contact) but different yearExpr - not recommended and unlikely to be intended. reuseRightsHs="
          + reuseRightsHs.stream().map(r -> r.getText()).collect(Collectors.joining(";"));
      final Issue iss = new Issue(file, issueText, Issue.Severity.WARNING, null,
          Issue.BlockedBadgeLevel.NONE);
      System.out.println("" + iss);
      Main.getIssueLogger().addIssue(iss);
    }
  }

  static void _validateAndDetermineBadgeLevel(final MainLight main) {
    final Map<String, FileHeader> fileHeaders = main.getFileHeaders();
    final IssueLogger log = Main.getIssueLogger();
    _validatePcrltSpecAnnotations(main.getFileHeadersReadOnly());
    for (final Entry<String, FileHeader> efh : fileHeaders.entrySet()) {
      final FileHeader fh = efh.getValue();
      if (fh != null) {
        if (H.isNotNullAndNotEmpty(fh.getReuseRightsHs())) {
          if (H.isNotNullAndNotEmpty(fh.getReuseLicenseExprs())) {
            fh.setBadgeLevel(1);
            // TODO check for snippets.
            // assert no snippets
            final boolean isMultipleRightsHolders=(H.distinctUsingEqualsIdentity(fh.getReuseRightsHs())
                .size() > 1);
            boolean isRightsHolderCommitMapping = true;
            if(isMultipleRightsHolders) {
              for (final Entry<String, PcrltIdentityGroup> epig : fh.getPcrltIdentityGroups().entrySet()) {
                if (epig.getValue() != null && epig.getValue().getPcrltIdentities().size()==1) {
                  for (final Entry<String, PcrltIdentity> epi : epig.getValue().getPcrltIdentities().entrySet()) {
                    if (epi.getValue() != null && !epi.getValue().getReuseRightsHs().isEmpty()) {
                      if(epig.getValue().getRightsH2CommitMapping()==null) {
                        isRightsHolderCommitMapping=false;
                      }
                    }
                  }
                }
              }
            }
            final boolean isLicenseCommitMapping = (fh.getPcrltLicenseExprs().size() > 1)
                ? fh.getPcrltLicenseExprs().values().stream().filter(v -> v.getPcrltLicense2CommitMapping() != null)
                    .count() == fh.getPcrltLicenseExprs().size()
                : true;
            boolean isSpecs = false;
            if (H.isNotNullAndNotEmpty(fh.getPcrltSpecs())) {
              if (fh.getPcrltSpecs().stream().filter(s -> s.getType() == PcrltSpecType.SPDX).count() == 1
                  && fh.getPcrltSpecs().stream().filter(s -> s.getType() == PcrltSpecType.REUSE).count() == 1
                  && fh.getPcrltSpecs().stream().filter(s -> s.getType() == PcrltSpecType.PCRLT).count() == 1) {
                isSpecs = true;
              }
            }
            final boolean isExtAnnotations = !fh.getExtAnnotations().isEmpty();
            System.out.println("isExtAnnotations="+isExtAnnotations);
            if (isExtAnnotations || (isRightsHolderCommitMapping && isLicenseCommitMapping && isSpecs)) {
              fh.setBadgeLevel(2);
              boolean isRightsHsResolvable = true;
              for (final Entry<String, PcrltIdentityGroup> epig : fh.getPcrltIdentityGroups().entrySet()) {
                if (epig.getValue() != null && !epig.getValue().getPcrltIdentities().isEmpty()) {
                  for (final Entry<String, PcrltIdentity> epi : epig.getValue().getPcrltIdentities().entrySet()) {
                    if (epi.getValue() != null && !epi.getValue().getReuseRightsHs().isEmpty()
                        && epi.getValue().getContacts().isEmpty()
                        && (epi.getValue().getName() == null || epi.getValue().getName().length() < 8)) {
                      isRightsHsResolvable = isRightsHsResolvable && !epi.getValue().getRepos().isEmpty();// TODO
                                                                                                          // support
                                                                                                          // map-to-git-identity
                    }
                  }
                } else {
                  isRightsHsResolvable = false;// this might be unreachable
                }
              }
              System.out.println("isRightsHsResolvable=" + isRightsHsResolvable + "(" + efh.getKey() + ")");
              boolean isSrcMatchFile = false;
              boolean isSrcMatchUrl = false;

              if (fh.getPcrltSrcs() != null) {
                for (final PcrltSrc src : fh.getPcrltSrcs()) {
                  if (src.getFilePath() != null && src.getFilePath().contentEquals(efh.getKey())) {
                    isSrcMatchFile = true;
                  }
                  if (src.getRepoUrl() != null && ValidationAndScoringHelper._isDep5Repository(src.getRepoUrl())) {
                    isSrcMatchUrl = true;
                  }
                }
              }
              System.out.println("isSrcMatch=" + isSrcMatchFile + "(" + efh.getKey() + ")");
              
              if (isExtAnnotations || (isRightsHsResolvable && isSrcMatchFile && isSrcMatchUrl && _checkIssues(main,efh.getKey(),3))) {
                fh.setBadgeLevel(3);
                if(isExtAnnotations) {
                  final String issueText = "badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable="
                      + isRightsHsResolvable + " isSrcMatchFile=" + isSrcMatchFile + " isSrcMatchUrl=" + isSrcMatchUrl;
                  final Issue iss = new Issue(efh.getKey(), issueText, Issue.Severity.INFO, null,
                      Issue.BlockedBadgeLevel.NONE);
                  System.out.println("" + iss);
                  log.addIssue(iss);
                }
              } else {
                final String issueText = "something missing for badgeLevel3: isRightsHsResolvable="
                    + isRightsHsResolvable + " isSrcMatchFile=" + isSrcMatchFile + " isSrcMatchUrl=" + isSrcMatchUrl + " checkIssues="+_checkIssues(main,efh.getKey(),3)
                    + ((isExtAnnotations) ? " (note: isExtAnnotations=" + isExtAnnotations + ")" : "");
                final Issue iss = new Issue(efh.getKey(), issueText, Issue.Severity.INFO, null,
                    Issue.BlockedBadgeLevel.L3);
                System.out.println("" + iss);
                log.addIssue(iss);
              }
            } else {
              final String issueText = "something missing for badgeLevel2: isRightsHolderCommitMapping="
                  + isRightsHolderCommitMapping + " isLicenseCommitMapping=" + isLicenseCommitMapping + " isSpecs="
                  + isSpecs;
              final Issue iss = new Issue(efh.getKey(), issueText, Issue.Severity.INFO, null,
                  Issue.BlockedBadgeLevel.L3);
              System.out.println("" + iss);
              log.addIssue(iss);
            }
          } else {
            System.out.println("warning:no reuseLicenseExprs");
          }
        } else {
          System.out.println("warning:no reuseRightsHs");
        }
      }
    }
  }

  private static void _validatePcrltSpecAnnotations(Map<String, FileHeaderReadOnly> fhs) {
    for(FileHeaderReadOnly fh:fhs.values()){
      if(fh!=null) {
        for(PcrltSpec spec:fh.getPcrltSpecs()) {
          if(spec.getType()==PcrltSpecType.SPDX&&!spec.getVersion().contentEquals("v"+Main.getMainLightLight().getSpdxVersion())) {
            Main.getIssueLogger().addIssue(new Issue(fh.getTargetFile(), 
                "SPDX version is different from the expected one. expected="+"v"+Main.getMainLightLight().getSpdxVersion()+" actual="+spec.getVersion(),
                Severity.INFO, null,Issue.BlockedBadgeLevel.L3));
          }
        }
      }
    }
    Map<String,List<PcrltSpec>>specs=fhs.values().stream().filter(f->f!=null).flatMap(f2->f2.getPcrltSpecs().stream()).collect(Collectors.groupingBy(s->(s.getType()!=null)?""+s.getType():"Unknown"));
    if(specs.containsKey(""+PcrltSpecType.PCRLT)) {
      List<String> distinPcrltSpecPcrltVersions=H.distinct(specs.get(""+PcrltSpecType.PCRLT).stream().map(s->s.getVersion()).collect(Collectors.toList()));
      if(distinPcrltSpecPcrltVersions.size()>1) {
        Main.getIssueLogger().addIssue(new Issue(null, 
            "found different PCRLT versions:"+distinPcrltSpecPcrltVersions.stream().collect(Collectors.joining(",")),
            Severity.WARNING, null,Issue.BlockedBadgeLevel.NONE));
      }
    }
    if(specs.containsKey(""+PcrltSpecType.REUSE)) {
      List<String> distinPcrltSpecReuseVersions=H.distinct(specs.get(""+PcrltSpecType.REUSE).stream().map(s->s.getVersion()).collect(Collectors.toList()));
      if(distinPcrltSpecReuseVersions.size()>1) {
        Main.getIssueLogger().addIssue(new Issue(null, 
            "found different REUSE versions:"+distinPcrltSpecReuseVersions.stream().collect(Collectors.joining(",")),
            Severity.WARNING, null,Issue.BlockedBadgeLevel.NONE));
      }
    }
  }

  private static boolean _checkIssues(MainLight main, String file, int badgeLevel) {
    return main.getIssues().stream().filter(i->((i.targetFile==null)||i.targetFile.contentEquals(file))&&i.blockedBadgeLevel.isBlocking(badgeLevel)).count()==0;
  }

  static void valdiateAndScore(final MainLight main) {
    ValidationAndScoringHelper._validateAndCalcFeatureScore(main);
    ValidationAndScoringHelper._validateAndDetermineBadgeLevel(main);

  }

  private static boolean _isDep5Repository(final String sourceUrl) {
    final Dep5FileHeaderAttributes fhDep5Attrs = ValidationAndScoringHelper._getDeb5FileHeaderAttributes();
    String dep5Src = null;
    if (fhDep5Attrs != null) {
      if (fhDep5Attrs.getSource() != null) {
        dep5Src = fhDep5Attrs.getSource().getText();
      }
    }
    return (dep5Src != null) && (sourceUrl != null) && sourceUrl.contains(dep5Src);
  }

  private static Dep5FileHeaderAttributes _getDeb5FileHeaderAttributes() {
    final FileHeaderReadOnly fhDep5 = Main.getMainLightLight().getFileHeadersReadOnly()
        .get(FileHeaderParser.DEP5_FILE_PATH);
    Dep5FileHeaderAttributes fhDep5Attrs = null;
    if (fhDep5 != null) {
      fhDep5Attrs = fhDep5.getDep5Attributes();
    }
    return fhDep5Attrs;
  }
}

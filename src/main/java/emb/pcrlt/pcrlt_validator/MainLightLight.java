//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/MainLightLight.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/MainLightLight.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding MainLightLight.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.util.List;
import java.util.Map;

import emb.pcrlt.pcrlt_validator.Main.MFHPolicy;
import emb.pcrlt.pcrlt_validator.model.FileHeaderReadOnly;
import emb.pcrlt.pcrlt_validator.model.FileStatistic;
import emb.pcrlt.pcrlt_validator.model.Issue;

public interface MainLightLight extends IssueLogger {
  List<Issue> getIssues();

  Map<String, FileHeaderReadOnly> getFileHeadersReadOnly();

  String getRepositoryDir();

  String getPackageVersion();

  String getPackageName();

  MFHPolicy getMfhPolicy();

  boolean getSkipParsingUnused();

  String getSpdxCreatorPerson();

  String getSkipFilesRegexForSpdxVerificationCode();

  Integer getMaxFileHeaderSize();

  Map<String, FileStatistic> getFileStatistics();
  
  String getSpdxVersion();

}
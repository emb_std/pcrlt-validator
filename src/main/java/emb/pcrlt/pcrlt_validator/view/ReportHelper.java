//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/view/ReportHelper.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/view/ReportHelper.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding ReportHelper.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.view;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.MainLightLight;
import emb.pcrlt.pcrlt_validator.model.FileHeader;
import emb.pcrlt.pcrlt_validator.model.FileHeaderReadOnly;
import emb.pcrlt.pcrlt_validator.model.FileStatistic;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.FileType;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.TextFileStatistic;
import emb.pcrlt.pcrlt_validator.model.Issue;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup;
import emb.pcrlt.pcrlt_validator.model.PcrltSpec;
import emb.pcrlt.pcrlt_validator.model.ReuseLicenseExpr;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH;
import emb.pcrlt.pcrlt_validator.model.PcrltSpec.PcrltSpecType;

public class ReportHelper {
  public static final String pcrltSpecPcrltVersionForReport="0.4";
  private ReportHelper() {

  }

  public static void generateReport(final String repositoryDir, final MainLightLight main, final String pcrltReportFile)
      throws IOException {
    final Map<String, FileHeaderReadOnly> fileHeaders = main.getFileHeadersReadOnly();
    // String report="";
    final Formatter report = new Formatter(repositoryDir + "/" + pcrltReportFile);
    // rightsH
    final List<ReuseRightsH> rightsHs = new ArrayList<>();
    for (final Entry<String, FileHeaderReadOnly> efh : fileHeaders.entrySet()) {
      if (efh.getValue() != null) {
        if (H.isNotNullAndNotEmpty(efh.getValue().getReuseRightsHs())) {
          for (final ReuseRightsH rh : efh.getValue().getReuseRightsHs()) {
            rightsHs.add(rh);
          }
        }
      }
    }
    final LinkedHashMap<ReuseRightsH, Long> rightsHFilesSorted = new LinkedHashMap<>();
    H.distinctUsingEqualsIdentity(rightsHs).stream()
        .map(rh -> new AbstractMap.SimpleEntry<ReuseRightsH, Long>(rh,
            fileHeaders.values().stream().filter(f -> f != null)
                .filter(f -> H.containsUsingCustomEquals(f.getReuseRightsHs(), rh,(a,b)->a.equalsIdentity(b))).count()))
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .forEachOrdered(x -> rightsHFilesSorted.put(x.getKey(), x.getValue()));

    report.format("Copyright Holders:%n");
    report.format("%-60s %-50s: %-13s%n", "name", "<contact>", "numberOfFiles");
    for (final Entry<ReuseRightsH, Long> erh : rightsHFilesSorted.entrySet()) {
      report.format("%-60s %-50s: %-13d%n", erh.getKey().getName(), "<" + erh.getKey().getContact() + ">",
          erh.getValue());
    }

    // License Expr
    final List<String> licenseExprs = getLicenseExprs(fileHeaders);
    final LinkedHashMap<String, Long> licenseExprsFilesSorted=getLicenseExprFilesSorted(fileHeaders,licenseExprs);
    final LinkedHashMap<String, List<String>> licenseExprsFiles=getLicenseExprFiles(fileHeaders,licenseExprs);

    report.format("%nLicense Expressions:%n");
    report.format("%-60s %-8s%s%n", "License Expression,", "nFiles*,", "Files");
    for (final Entry<String, Long> el : licenseExprsFilesSorted.entrySet()) {
      report.format("%-60s %-8d%s%n", el.getKey(), el.getValue(),
          licenseExprsFiles.get(el.getKey()).stream().collect(Collectors.joining(",")));
    }
    report.format("*: nFiles=Number of Files");

    // pcrltIdentities
    final List<PcrltIdentity> pcrltIdentities = new ArrayList<>();
    for (final Entry<String, FileHeaderReadOnly> efh : fileHeaders.entrySet()) {
      if (efh.getValue() != null) {
        if (H.isNotNullAndNotEmpty(efh.getValue().getPcrltIdentityGroups())) {
          for (final PcrltIdentityGroup pig : efh.getValue().getPcrltIdentityGroups().values()) {
            for (final PcrltIdentity pi : pig.getPcrltIdentities().values()) {
              pcrltIdentities.add(pi);
            }
          }
        } else {

        }
      }
    }
    final LinkedHashMap<PcrltIdentity, Long> pcrltIdentitiesFilesSorted = new LinkedHashMap<>();
    H.distinctUsingEqualsIdentity(pcrltIdentities).stream()
        .map(pi -> new AbstractMap.SimpleEntry<PcrltIdentity, Long>(pi,
            fileHeaders.values().stream().filter(f -> f != null)
                .filter(f -> f.getPcrltIdentityGroups().values().stream()
                    .anyMatch(pig -> H.containsUsingCustomEquals(pig.getPcrltIdentities().values(), pi,(a,b)->a.equalsIdentity(b))))
                .count()))
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .forEachOrdered(x -> pcrltIdentitiesFilesSorted.put(x.getKey(), x.getValue()));

    report.format("%n");
    report.format("%nPCRLT Identities:%n");
    report.format("%-60s %-60s %-50s: %-13s%n", "abbrId", "name", "<contact>", "numberOfFiles");
    for (final Entry<PcrltIdentity, Long> epi : pcrltIdentitiesFilesSorted.entrySet()) {
      report.format("%-60s %-60s %-50s: %-13d%n", epi.getKey().getAbbrId(), epi.getKey().getName(),
          "<" + epi.getKey().getContacts().stream().collect(Collectors.joining(",")) + ">", epi.getValue());
    }
    // srcs
    report.format("%nDistinct repositories from src-trace-repository annotations:%n");
    for (final String repoUrl : FileHeader.getAllSrcTraceRepoUrls(main.getFileHeadersReadOnly())) {
      report.format("%-60s%n", repoUrl);
    }
    // srcs
    report.format("%nDistinct repositories from resolve-git-identity annotations:%n");
    for (final String repoUrl : FileHeader.getAllResolveGitIdentityRepoUrls(main.getFileHeadersReadOnly())) {
      report.format("%-60s%n", repoUrl);
    }
    // score
    final LinkedHashMap<String, Integer> fileScoresSorted = new LinkedHashMap<>();
    fileHeaders.entrySet().stream()
        .map(f -> new AbstractMap.SimpleEntry<String, Integer>(f.getKey(),
            (f.getValue() != null) ? f.getValue().getScore() : 0))
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .forEachOrdered(x -> fileScoresSorted.put(x.getKey(), x.getValue()));
    final LinkedHashMap<String, Integer> fileComplScoresSorted = new LinkedHashMap<>();
    fileHeaders.entrySet().stream()
        .map(f -> new AbstractMap.SimpleEntry<String, Integer>(f.getKey(),
            (f.getValue() != null) ? f.getValue().getBadgeLevel() : 0))
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .forEachOrdered(x -> fileComplScoresSorted.put(x.getKey(), x.getValue()));
    report.format("%nPCRLT File Scores:%n");
    report.format("%-119s %-48s %-30s%n", "file", "(Score1,Score2,RH,Li,Sp,Sr,IG,RH2C,IGwR,IGwA,L2C)*", "specVersions");
    for (final Entry<String, Integer> e : fileScoresSorted.entrySet()) {
      final FileHeaderReadOnly f = fileHeaders.get(e.getKey());
      final String specTypeVersions = ReportHelper.getSpecTypeVersions(f);
      final Integer complScore = (fileComplScoresSorted.get(e.getKey()) != null) ? fileComplScoresSorted.get(e.getKey())
          : 0;
      List<Integer> sl = (f != null) ? f.getScoreList() : null;
      report.format(
          "%-120s %-6d %-6d %-2d %-2d %-2d %-2d %-2d %-4d %-4d %-4d %-5d %-30s%n",// @formatter:off
          e.getKey(),
          complScore,
          e.getValue(),
          (sl != null) ? sl.get(0) : 0,
          (sl != null) ? sl.get(1) : 0,
          (sl != null) ? sl.get(2) : 0,
          (sl != null) ? sl.get(3) : 0,
          (sl != null) ? sl.get(4) : 0,
          (sl != null) ? sl.get(5) : 0,
          (sl != null) ? sl.get(6) : 0,
          (sl != null) ? sl.get(7) : 0,
          (sl != null) ? sl.get(8) : 0,
          specTypeVersions);// @formatter:on
    }
    report.format(
        "*:Score1=Badge level,Score2=Feature Score,RH=RightsHolders,Li=Licenses,Sp=Specifications(SPDX,REUSE,PCRLT),Sr=Sources/Urls,IG=IdentityGroups,RightsHolders2CommitMappings,IGwR=IdentityGroups with repository,IGwA=IdentityGroups with authorAnnotation,L2C=License2CommitMappings%n");

    Map<String, FileStatistic> fileStatistics = main.getFileStatistics();
    Function<Collection<FileStatistic>, Double> sumFileSize = list -> {
      return (double) list.stream().filter(f -> f != null).map(f -> f.fileSize).reduce((a, b) -> a + b).orElse(0L)
          / 1000;
    };
    Function<Collection<FileStatistic>, Integer> sumNLines = list -> {
      long countOrig = list.size();
      long countText = list.stream().filter(f -> f != null && f.fileType == FileType.TEXT).count();
      Integer res = null;
      if (countOrig == countText) {
        res = list.stream().map((FileStatistic f) -> ((TextFileStatistic) f).nLines).reduce((a, b) -> a + b).orElse(0);
      }
      return res;
    };
    Function<Collection<FileStatistic>, Double> sumN80CharLines = list -> {
      long countOrig = list.size();
      long countText = list.stream().filter(f -> f != null && f.fileType == FileType.TEXT).count();
      Double res = null;
      if (countOrig == countText) {
        res = list.stream().map((FileStatistic f) -> ((TextFileStatistic) f).n80CharLines).reduce((a, b) -> a + b)
            .orElse(0d);
      }
      return res;
    };
    int maxLeftChars = 45;
    report.format("%n");
    report.format("%-" + maxLeftChars + "s%-9s%-13s%-13s%-13s%n", "File Statistics: ", "nFiles,  ", "Size[KB],    ",
        "nLines,      ", "n80CharLines ");
    report.format("%-" + maxLeftChars + "s%-8d %-12.3f %-12s %-12s%n", "  All Files:", fileStatistics.size(),
        sumFileSize.apply(fileStatistics.values()),
        (sumNLines.apply(fileStatistics.values()) != null)
            ? String.format("%-8d", sumNLines.apply(fileStatistics.values()))
            : "",
        (sumN80CharLines.apply(fileStatistics.values()) != null)
            ? String.format("%-8.2f", sumN80CharLines.apply(fileStatistics.values()))
            : "");
    report.format("%-" + maxLeftChars + "s%n", "  Files grouped by LICENSES folder:");//this includes all files with path starting with "LICENSES/" i.e. not in test resources
    List<FileStatistic> fileStatisticLicenses = fileStatistics.values().stream()
        .filter(f -> f != null && f.filePath.startsWith("LICENSES/")).collect(Collectors.toList());
    report.format("%-" + maxLeftChars + "s%-8d %-12.3f %-12s %-12s%n", "    Files in LICENSES folder:",
        fileStatisticLicenses.size(), sumFileSize.apply(fileStatisticLicenses),
        (sumNLines.apply(fileStatisticLicenses) != null) ? String.format("%-8d", sumNLines.apply(fileStatisticLicenses))
            : "",
        (sumN80CharLines.apply(fileStatisticLicenses) != null)
            ? String.format("%-8.2f", sumN80CharLines.apply(fileStatisticLicenses))
            : "");
    List<FileStatistic> fileStatisticNonLicenses = fileStatistics.values().stream()
        .filter(f -> f != null && !f.filePath.startsWith("LICENSES/")).collect(Collectors.toList());
    report.format("%-" + maxLeftChars + "s%-8d %-12.3f %-12s %-12s%n", "    Files not in LICENSES folder:",
        fileStatisticNonLicenses.size(), sumFileSize.apply(fileStatisticNonLicenses),
        (sumNLines.apply(fileStatisticNonLicenses) != null)
            ? String.format("%-8d", sumNLines.apply(fileStatisticNonLicenses))
            : "",
        (sumN80CharLines.apply(fileStatisticNonLicenses) != null)
            ? String.format("%-8.2f", sumN80CharLines.apply(fileStatisticNonLicenses))
            : "");
    Map<String, List<FileStatistic>> fileStatisticsGroupedByType = fileStatisticNonLicenses.stream()
        .collect(Collectors.groupingBy(f -> "" + f.fileType));
    report.format("%-" + maxLeftChars + "s%n", "      Files grouped by file type (TEXT,BINARY):");
    for (Entry<String, List<FileStatistic>> e : fileStatisticsGroupedByType.entrySet()) {
      report.format("%-" + maxLeftChars + "s%-8d %-12.3f %-12s %-12s%n", "        Files of type " + e.getKey() + ":",
          e.getValue().size(), sumFileSize.apply(e.getValue()),
          (sumNLines.apply(e.getValue()) != null) ? String.format("%-8d", sumNLines.apply(e.getValue())) : "",
          (sumN80CharLines.apply(e.getValue()) != null) ? String.format("%-8.2f", sumN80CharLines.apply(e.getValue()))
              : "");
    }
    Map<String, List<FileStatistic>> fileStatisticsGroupedByExtension = fileStatisticNonLicenses.stream()
        .collect(Collectors.groupingBy(f -> f.fileExtension));
    report.format("%-" + maxLeftChars + "s%n", "      Files grouped by file extension:");
    for (Entry<String, List<FileStatistic>> e : fileStatisticsGroupedByExtension.entrySet()) {
      report.format("%-" + maxLeftChars + "s%-8d %-12.3f %-12s %-12s%n",
          "        Files with extension " + ((e.getKey().isEmpty()) ? "None" : "\"." + e.getKey() + "\"") + ":",
          e.getValue().size(), sumFileSize.apply(e.getValue()),
          (sumNLines.apply(e.getValue()) != null) ? String.format("%-8d", sumNLines.apply(e.getValue())) : "",
          (sumN80CharLines.apply(e.getValue()) != null) ? String.format("%-8.2f", sumN80CharLines.apply(e.getValue()))
              : "");
    }

    List<List<Integer>> rows = FileHeaderCsv.getFileHeaderStatisticsAsInts(fileHeaders, fileStatistics);
    Function<Integer, List<Integer>> getColumn = (i) -> {
      return rows.stream().map(r -> r.get(i)).collect(Collectors.toList());
    };
    report
        .format("%nPCRLT File Header Statistics (where the number of records (fileHeaders) is " + rows.size() + "):%n");

    List<String> columns = Arrays.asList(new String[] { "badgeLevel,", "nSpecs,", "nSrcs,", "nLics,",
        "nL2Commits,", "nLBackgrounds,", "nRHAnnotations,", "nRhs, ", "nRHBackgrounds,", "nRH2Commits,",
        "nIdentityGroups,", "nIdentities,", "nIdentiesExplicitly,", "nResolveAnnotations,", "nMap2GitIdAnnotations,",
        "nAuthorAnnotations,", "nAuthors,", "nAuthorBackgrounds,", "nAuthor2Commits,", "nPersDataPolicies,",
        "nAddEtcPolicies,", "nExtAnnotations" });
    String formatStr = "";
    for (String column : columns) {
      formatStr += "%-" + column.length() + "s";
    }
    formatStr += "%n";
    report.format("%-10s", "");
    report.format(formatStr, (Object[]) columns.toArray(new String[0]));
    String formatInt = "";
    for (String column : columns) {
      formatInt += "%-" + (column.length() - 1) + "d ";
    }
    formatInt += "%n";
    String formatDouble = "";
    for (String column : columns) {
      formatDouble += "%-" + (column.length() - 1) + ".3f ";
    }
    formatDouble += "%n";
    report.format("%-10s", "  Sum");
    report.format(formatInt,
        (Object[]) IntStream.range(0, rows.get(0).size())
            .mapToObj(i -> getColumn.apply(i).stream().reduce(0, (i1,i2)->i1+i2))
            .collect(Collectors.toList()).toArray(new Integer[0]));
    
    report.format("%-10s", "  Max");

    report.format(formatInt, (Object[]) IntStream.range(0, rows.get(0).size())
        .mapToObj(i -> Collections.max(getColumn.apply(i))).collect(Collectors.toList()).toArray(new Integer[0]));

    report.format("%-10s", "  AVR");
    report.format(formatDouble,
        (Object[]) IntStream.range(0, rows.get(0).size())
            .mapToObj(i -> getColumn.apply(i).stream().collect(Collectors.averagingInt(ii -> ii)))
            .collect(Collectors.toList()).toArray(new Double[0]));

    report.format("%-10s", "  Min");
    report.format(formatInt, (Object[]) IntStream.range(0, rows.get(0).size())
        .mapToObj(i -> Collections.min(getColumn.apply(i))).collect(Collectors.toList()).toArray(new Integer[0]));

    report.format("%nPCRLT Issues:%n");
    report.format("%-120s %-13s %-13s %-13s %-200s%n", "file", "severity", "lino","blockedLevel*", "issueText");
    for (final Issue i : main.getIssues()) {
      report.format("%-120s %-13s %-13s %-13s %-200s%n", i.targetFile, i.level, i.lino, i.blockedBadgeLevel, i.text);
    }
    report.format("*: blockedLevel refers to the lowest PCRLT badge/level (L1=Bronze, L2=Silver, L3=Gold) whose requirements are not met.%n");

    report.format("%n");
    int minBadgeLevel = IntStream.range(0, rows.get(0).size()).mapToObj(i -> Collections.min(getColumn.apply(i)))
        .collect(Collectors.toList()).get(0);
    boolean hasErrorIssues = main.getIssues().stream().filter(i -> i.level == Issue.Severity.ERROR).count() > 0;
    String badge = (minBadgeLevel == 1) ? "Bronze"
        : (minBadgeLevel == 2) ? "Silver" : (minBadgeLevel == 3) ? "Gold" : "batchNotFound";
    
    if (hasErrorIssues || minBadgeLevel == 0) {
      report.format("%s%n", "Unfortunately, the Git repository is currently not compliant with PCRLTv"+pcrltSpecPcrltVersionForReport+" :-(\n(See PCRLT report "+pcrltReportFile+" in the target repository)");
    } else {
      
      report.format("%s%n", "Congratulations! The Git repository is compliant with the *" + badge
          + "* badge requirements of version "+pcrltSpecPcrltVersionForReport+" of the PCRLT specification :-)\nNote: The PCRLT specification has 3 badges (Bronze, Silver, Gold) to evaluate how well the repository is documented according to the PCRLT spec.\nNote: PCRLT stands for Privacy-aware Content, Rights Holder and License Tracing.");
    }

    report.close();
  }

  private static String getSpecTypeVersions(final FileHeaderReadOnly f) {
    String specTypeVersions = "";
    if (f != null && f.getPcrltSpecs() != null) {
      final Map<Object, List<PcrltSpec>> specs = f.getPcrltSpecs().stream()
          .collect(Collectors.groupingBy(s -> (s.getType() != null) ? s.getType() : "null"));
      specTypeVersions += (specs.containsKey(PcrltSpecType.SPDX))
          ? PcrltSpecType.SPDX.toString() + specs.get(PcrltSpecType.SPDX).get(0).getVersion()
          : "x";
      specTypeVersions += " ";
      specTypeVersions += (specs.containsKey(PcrltSpecType.REUSE))
          ? PcrltSpecType.REUSE.toString() + specs.get(PcrltSpecType.REUSE).get(0).getVersion()
          : "x";
      specTypeVersions += " ";
      specTypeVersions += (specs.containsKey(PcrltSpecType.PCRLT))
          ? PcrltSpecType.PCRLT.toString() + specs.get(PcrltSpecType.PCRLT).get(0).getVersion()
          : "x";
    }
    return specTypeVersions;
  }
  public static List<String> getLicenseExprs(Map<String, FileHeaderReadOnly> fileHeaders){
    final List<String> licenseExprs = new ArrayList<>();
    for (final Entry<String, FileHeaderReadOnly> efh : fileHeaders.entrySet()) {
      if (efh.getValue() != null) {
        if (H.isNotNullAndNotEmpty(efh.getValue().getReuseLicenseExprs())) {
          for (final ReuseLicenseExpr l : efh.getValue().getReuseLicenseExprs().values()) {
            licenseExprs.add(l.getLicenseExpr());
          }
        } else {

        }
      }
    }
    return licenseExprs;
  }
  public static LinkedHashMap<String, Long> getLicenseExprFilesSorted(Map<String, FileHeaderReadOnly> fileHeaders,List<String> licenseExprs){
    final LinkedHashMap<String, Long> licenseExprsFilesSorted = new LinkedHashMap<>();
    H.distinct(licenseExprs).stream()
        .map(l -> new AbstractMap.SimpleEntry<String, Long>(l,
            fileHeaders.values().stream().filter(f -> f != null).filter(f -> f.getReuseLicenseExprs().containsKey(l))
                .count()))
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .forEachOrdered(x -> licenseExprsFilesSorted.put(x.getKey(), x.getValue()));
    return licenseExprsFilesSorted;
  }
  public static LinkedHashMap<String, List<String>> getLicenseExprFiles(Map<String, FileHeaderReadOnly> fileHeaders,List<String> licenseExprs) {
    final LinkedHashMap<String, List<String>> licenseExprsFiles = new LinkedHashMap<>();
    H.distinct(licenseExprs).stream()
        .map(l -> new AbstractMap.SimpleEntry<String, List<String>>(l,
            fileHeaders.values().stream().filter(f -> f != null).filter(f -> f.getReuseLicenseExprs().containsKey(l))
                .map(f -> f.getTargetFile()).collect(Collectors.toList())))
        .forEachOrdered(x -> licenseExprsFiles.put(x.getKey(), x.getValue()));
    return licenseExprsFiles;
  }
}

//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/view/SPDXHelper.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/view/SPDXHelper.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding SPDXHelper.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.view;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.jgit.dircache.DirCacheIterator;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.spdx.library.InvalidSPDXAnalysisException;
import org.spdx.library.model.SpdxPackageVerificationCode;
import org.spdx.tools.GenerateVerificationCode;
import org.spdx.tools.OnlineToolException;
import org.spdx.utility.verificationcode.JavaSha1ChecksumGenerator;

import emb.helper.HelperI;
import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.Main;
import emb.pcrlt.pcrlt_validator.Main.ManifestVersionProvider;
import emb.pcrlt.pcrlt_validator.MainLightLight;
import emb.pcrlt.pcrlt_validator.model.FileHeader;
import emb.pcrlt.pcrlt_validator.model.FileHeaderReadOnly;
import emb.pcrlt.pcrlt_validator.model.FileStatistic;
import emb.pcrlt.pcrlt_validator.model.Issue;
import emb.pcrlt.pcrlt_validator.model.FileHeader.FileHeaderType;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.FileType;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.TextFileStatistic;
import picocli.CommandLine.IVersionProvider;

public class SPDXHelper {
  public static final String SPDX_CREATOR_TOOL_NAME = "pcrlt-validator";// hard coded - this has to be adjusted if the
                                                                        // tool name changes
  

  private SPDXHelper() {
  }

  public static void generateSpdxDoc(final Repository repo, final String repositoryDir, final int maxFiles,
      final String spdxDocFile, final String spdxDocTemplateStr,final MainLightLight main)
      throws IOException, NoSuchAlgorithmException, OnlineToolException, InvalidSPDXAnalysisException {

    final AbstractTreeIterator treeIterator = new DirCacheIterator(repo.readDirCache());
    final TreeWalk treeWalk = new TreeWalk(repo);
    treeWalk.addTree(treeIterator);
    treeWalk.setRecursive(true);
    final String tmpCopyBaseDir = repositoryDir + "/.tmpCopyFilesForVerificationCode";
    if (new File(tmpCopyBaseDir).exists()) {
      HelperI.DefaultHelper.helper.deleteDirRecursively(tmpCopyBaseDir, maxFiles);
    }
    String spdxDoc = "";
    spdxDoc += spdxDocTemplateStr//
        + "\n";
    // System.out.println(spdxDoc);

    int fileCount = 1;
    final JavaSha1ChecksumGenerator fileChecksumGenerator = new JavaSha1ChecksumGenerator();
    FileHeaderReadOnly currFH = null;
    final String sha1Dummy = "000000000000000000000000000000000000000000";
    final Map<String, SPDXFile> spdxFiles = new LinkedHashMap<>();
    while (treeWalk.next()) {
      final String currFileWithRelPath = treeWalk.getPathString();
      System.out.println(currFileWithRelPath);
      final Path tmpCopyDest = Paths.get(tmpCopyBaseDir + "/" + currFileWithRelPath);
      final Path tmpCopyDestParentDir = tmpCopyDest.getParent();
      if (tmpCopyDestParentDir != null) {
        Files.createDirectories(tmpCopyDestParentDir);
      }
      final File tmpCopySrcFile = new File(repositoryDir + "/" + currFileWithRelPath);
      if (tmpCopySrcFile.exists()) {
        final Path tmpCopySrc = tmpCopySrcFile.toPath();
        Files.copy(tmpCopySrc, tmpCopyDest);
        final SPDXFile spdxFile = new SPDXFile();
        spdxFile.fileName = currFileWithRelPath;
        spdxFile.spdxId = "SPDXRef-f" + fileCount;
        final String sha1 = fileChecksumGenerator.getFileChecksum(new File(repositoryDir + "/" + currFileWithRelPath));
        spdxFile.fileChecksum = sha1;
        currFH = main.getFileHeadersReadOnly().get(currFileWithRelPath);
        if (currFH != null && !currFH.getReuseLicenseExprs().isEmpty()
            && H.distinct(currFH.getReuseLicenseExprs().keySet()).size() == 1) {
          spdxFile.licenseConcluded = currFH.getReuseLicenseExprs().keySet().iterator().next();
        } else {
          spdxFile.licenseConcluded = "NoAssertion";
        }
        if (currFH != null && !currFH.getReuseLicenseExprs().isEmpty()) {
          spdxFile.licenseFileInfos = new ArrayList<>(currFH.getReuseLicenseExprs().keySet());
        } else {
          spdxFile.licenseFileInfos = new ArrayList<>();
        }
        if (currFH != null && !currFH.getReuseRightsHs().isEmpty()) {
          spdxFile.fileCopyrightTexts = FileHeader.filterReuseRigthsHForSPDX(currFH.getReuseRightsHs()).stream()
              .map(r -> r.getText()).collect(Collectors.toList());
        } else {
          spdxFile.fileCopyrightTexts = new ArrayList<>();
        }
        if (currFH != null) {
          if (currFH.getType() == FileHeaderType.std) {
            spdxFile.licenseComment = "License and Copyright Infos were automatically extracted from the file itself during SPDX-Document generation";
          } else if (currFH.getType() == FileHeaderType.dotLicense) {
            spdxFile.licenseComment = "License and Copyright Infos were automatically extracted from the "
                + spdxFile.fileName + ".license file during SPDX-Document generation";
          } else if (currFH.getType() == FileHeaderType.dep5) {
            spdxFile.licenseComment = "License and Copyright Infos were automatically extracted from the .reuse/dep5 file during SPDX-Document generation (the target file is explicitly listed there)";
          } else {
            Main.getIssueLogger().addIssue(new Issue(spdxFile.fileName,
                "unknown FileHeaderType:" + currFH.getType() + "(not implemented)", Issue.Severity.ERROR, null));
          }
        }
        spdxFiles.put(currFileWithRelPath, spdxFile);
        fileCount += 1;
      } // if the file does not exists, an issue has already been thrown by the parser.
    }
    treeWalk.close();

    final String uuid = UUID.randomUUID().toString();
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<UUID>", uuid);
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<SPDXVersion>", main.getSpdxVersion());
    final String created = Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<Created>", created);
    final String[] pcrltValidatorNameAndVersionArr = SPDXHelper.getPcrltValidatorNameAndVersion();
    final String pcrltValidatorVersion = (pcrltValidatorNameAndVersionArr != null
        && pcrltValidatorNameAndVersionArr.length > 1 && !pcrltValidatorNameAndVersionArr[1].strip().isEmpty())
            ? pcrltValidatorNameAndVersionArr[1]
            : "unknown-version";
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<SpdxCreatorTool>",
        SPDXHelper.SPDX_CREATOR_TOOL_NAME + "-" + pcrltValidatorVersion);
    String targetPackageVersion = main.getPackageVersion();
    final String targetPackageName = main.getPackageName();
    if (targetPackageVersion == null || targetPackageVersion.strip().isEmpty()) {
      targetPackageVersion = "unknown-version";
    }
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<PackageVersion>", targetPackageVersion);
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<PackageName>", targetPackageName);
    final String pcrltValidatorExecutionOptions = "--multiple-file-header-policy=" + main.getMfhPolicy()
        + ((main.getSkipParsingUnused()) ? " " + "--skip-parsing-unused-file-headers" : "");
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<PcrltValidatorExecutionOptions>",
        pcrltValidatorExecutionOptions);
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<SpdxCreatorPerson>", main.getSpdxCreatorPerson());
    
    final String allFilesSummary=getAllFilesSummary(main);
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<AllFilesSummary>", ""+allFilesSummary);
    final String licenseExprTable=getLicenseExprTable(main);
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<LicenseExprTable>", licenseExprTable);
    final SpdxPackageVerificationCode verificationCode = GenerateVerificationCode
        .generateVerificationCode(tmpCopyBaseDir, main.getSkipFilesRegexForSpdxVerificationCode());
    // see also
    // https://github.com/spdx/tools-java/blob/master/src/main/java/org/spdx/tools/GenerateVerificationCode.java
    // (printVerificationCode)
    System.out.println("Verification code value: " + verificationCode.getValue());
    final Collection<String> excludedFiles = verificationCode.getExcludedFileNames();
    if (excludedFiles != null && excludedFiles.size() > 0) {
      excludedFiles.stream().forEachOrdered(f -> {
        System.out.println("\t" + f);
      });
    } else {
      System.out.println("No excluded files");
    }
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<PackageVerificationCode>", verificationCode.getValue());
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<PackageVerificationCodeExcludedFiles>",
        (excludedFiles != null) ? excludedFiles.stream().collect(Collectors.joining(", ")) : "");
    spdxDoc += "\n";
    String spdxDocAllFileInfos="";
    for (final SPDXFile f : spdxFiles.values()) {
      spdxDocAllFileInfos += "\n";
      spdxDocAllFileInfos += "FileName: ./" + f.fileName + "\n";
      spdxDocAllFileInfos += "SPDXID: " + f.spdxId + "\n";
      spdxDocAllFileInfos += "FileChecksum: SHA1: "
          + ((excludedFiles != null && !excludedFiles.contains("./" + f.fileName)) ? f.fileChecksum : sha1Dummy) + "\n";
      spdxDocAllFileInfos += "LicenseConcluded: " + f.licenseConcluded + "\n";
      if (!f.licenseFileInfos.isEmpty()) {
        for (final String l : f.licenseFileInfos) {
          spdxDocAllFileInfos += "LicenseInfoInFile: " + l + "\n";
        }
      } else {
        spdxDocAllFileInfos += "LicenseInfoInFile: \n";
      }
      if (!f.fileCopyrightTexts.isEmpty()) {
        for (final String c : f.fileCopyrightTexts) {
          spdxDocAllFileInfos += "FileCopyrightText: <text>" + c + "</text>\n";
        }
      } else {
        spdxDocAllFileInfos += "FileCopyrightText: <text></text>\n";
      }
      if (f.licenseComment != null && !f.licenseComment.strip().isEmpty()) {
        spdxDocAllFileInfos += "LicenseComment: <text>" + f.licenseComment + "</text>\n";
      }
    }
    spdxDoc = SimpleStringTemplateHelper.replace(spdxDoc, "<AllFileInfos>", spdxDocAllFileInfos);
    HelperI.DefaultHelper.helper.writeFile(repositoryDir + "/" + spdxDocFile, spdxDoc);
  }
  
  public static String[] getPcrltValidatorNameAndVersion() {
    String[] pcrltValidatorNameAndVersion = null;
    final IVersionProvider versProv = new ManifestVersionProvider();
    try {
      pcrltValidatorNameAndVersion = versProv.getVersion();
    } catch (final Exception e) {
      e.printStackTrace();
    }
    return pcrltValidatorNameAndVersion;
  }
  private static String getAllFilesSummary(MainLightLight main) {
    Map<String, FileStatistic> fileStatistics = main.getFileStatistics();
    Function<Collection<FileStatistic>, Double> sumFileSize = list -> {
      return (double) list.stream().filter(f -> f != null).map(f -> f.fileSize).reduce((a, b) -> a + b).orElse(0L)
          / 1000;
    };
    Function<Collection<FileStatistic>, Double> sumN80CharLines = list -> {
      long countOrig = list.size();
      long countText = list.stream().filter(f -> f != null && f.fileType == FileType.TEXT).count();
      Double res = null;
      if (countOrig == countText) {
        res = list.stream().map((FileStatistic f) -> ((TextFileStatistic) f).n80CharLines).reduce((a, b) -> a + b)
            .orElse(0d);
      }
      return res;
    };
    Map<String, List<FileStatistic>> fileStatisticsGroupedByType = fileStatistics.values().stream()
        .collect(Collectors.groupingBy(f -> "" + f.fileType));
    int nFiles=fileStatistics.size();
    int nFilesText=0;
    int nFilesBinary=0;
    double totalSize=sumFileSize.apply(fileStatistics.values());
    double totalSizeText=0;
    double totalSizeBinary=0;
    Double n80CharLinesText=0d;
    if(fileStatisticsGroupedByType.containsKey("TEXT")) {
      nFilesText=fileStatisticsGroupedByType.get("TEXT").size();
      totalSizeText=sumFileSize.apply(fileStatisticsGroupedByType.get("TEXT"));
      n80CharLinesText=sumN80CharLines.apply(fileStatisticsGroupedByType.get("TEXT"));
      assert n80CharLinesText!=null;//only nullable for binary
    }
    if(fileStatisticsGroupedByType.containsKey("BINARY")) {
      nFilesBinary=fileStatisticsGroupedByType.get("BINARY").size();
      totalSizeBinary=sumFileSize.apply(fileStatisticsGroupedByType.get("BINARY"));
    }
    return String.format("The repository contains %d files (%d text, %d binary) with a total of %.2f KB (%.2f text, %.2f binary) where the text files have a total of %.2f 80CharLines.\n"
        +"(n80CharLines are calculated by counting the printable characters devided by a standard line length of 80 characters)",nFiles,nFilesText,nFilesBinary,totalSize,totalSizeText,totalSizeBinary,n80CharLinesText);
  }
  private static String getLicenseExprTable(MainLightLight main) {
    final Map<String, FileHeaderReadOnly> fileHeaders = main.getFileHeadersReadOnly();
    Map<String, FileStatistic> fs = main.getFileStatistics();
    final List<String> licenseExprs = ReportHelper.getLicenseExprs(fileHeaders);
    final int maxLength=(licenseExprs.isEmpty())?18:Collections.max(licenseExprs.stream().map(l->(l==null)?0:l.length()).collect(Collectors.toList()));
    final LinkedHashMap<String, Long> licenseExprFilesSorted=ReportHelper.getLicenseExprFilesSorted(fileHeaders,licenseExprs);
    final LinkedHashMap<String, List<String>> licenseExprFiles=ReportHelper.getLicenseExprFiles(fileHeaders,licenseExprs);
    String licenseExprTable=String.format("%-"+maxLength+"s %-20s %-30s %-12s %-60s%n", "License Expression", "nFiles (text,binary)", "size (text,binary)","n80CharLines","extensions");
    for (final Entry<String, Long> el : licenseExprFilesSorted.entrySet()) {
      int nFiles=licenseExprFiles.get(el.getKey()).size();
      List<FileStatistic>fileStats=licenseExprFiles.get(el.getKey()).stream().filter(f->(fs.get(f)!=null)).map(f->fs.get(f)).collect(Collectors.toList());
      long nFilesText=fileStats.stream().filter(fst->fst.fileType==FileType.TEXT).count();
      long nFilesBinary=fileStats.stream().filter(fst->fst.fileType==FileType.BINARY).count();
      String nFilesStr=String.format("%d (%d, %d)", nFiles,nFilesText,nFilesBinary);
      double size=(double)fileStats.stream().map(fst->fst.fileSize).reduce(0l, (a,b)->a+b)/1000;
      double sizeText=(double)fileStats.stream().filter(fst->fst.fileType==FileType.TEXT).map(fst->fst.fileSize).reduce(0l, (a,b)->a+b)/1000;
      double sizeBinary=(double)fileStats.stream().filter(fst->fst.fileType==FileType.BINARY).map(fst->fst.fileSize).reduce(0l, (a,b)->a+b)/1000;
      String sizeStr=String.format("%.2fKB (%.2f, %.2f)", size,sizeText,sizeBinary);
      double n80CharLines=fileStats.stream().filter(fst->fst.fileType==FileType.TEXT)
          .map((FileStatistic f) -> ((TextFileStatistic) f).n80CharLines).reduce((a, b) -> a + b)
          .orElse(0d);
      String extensions=fileStats.stream().collect(Collectors.groupingBy(fst->fst.fileExtension)).keySet().stream().map(e->(e=="")?"\"\"":"."+e).sorted().collect(Collectors.joining(","));
      licenseExprTable+=String.format("%-"+maxLength+"s %-20s %-30s %-12.2f %-60s%n", el.getKey(), nFilesStr,sizeStr,n80CharLines,extensions);
    }
    return licenseExprTable;
  }

  static class SPDXFile {
    String fileName;
    String spdxId;
    String fileChecksum;
    String licenseConcluded;
    List<String> licenseFileInfos;
    List<String> fileCopyrightTexts;
    String licenseComment;
  }
}

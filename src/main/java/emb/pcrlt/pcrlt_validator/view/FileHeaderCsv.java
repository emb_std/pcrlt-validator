//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/view/FileHeaderCsv.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/view/FileHeaderCsv.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding FileHeaderCsv.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.view;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity;
import emb.pcrlt.pcrlt_validator.model.FileHeader.FileHeaderType;
import emb.pcrlt.pcrlt_validator.model.FileHeaderReadOnly;
import emb.pcrlt.pcrlt_validator.model.FileStatistic;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.TextFileStatistic;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltAuthor;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityRepo;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltPDataPolicy;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltRightsHARMGPolicy;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup;

public class FileHeaderCsv {
  public String targetFile;
  public String tFileType;
  public String tFileExtension;
  public String tFileSize;
  public String tFileNLines;
  public String tFileN80CharLines;
  public FileHeaderType type;
  public int badgeLevel;
  public int nSpecs;
  public int nSrcs;
  public int nLs;
  public int nL2Commits;
  public int nLBackgrounds;
  public int nRHAnnotations;
  public int nRhs;
  public int nRHBackgrounds;
  public int nRH2Commits;
  public int nIdentityGroups;
  public int nIdentities;
  public int nIdentiesExplicitly; // (specify-identity)
  public int nResolveAnnotations;
  public int nMap2GitIdAnnotations;
  public int nAuthorAnnotations;
  public int nAuthors;
  public int nAuthorBackgrounds;
  public int nAuthor2Commits;
  public int nPersDataPolicies;
  public int nAddEtcPolicies;
  public int nExtAnnotations;

  public FileHeaderCsv(final String targetFile, final FileHeaderReadOnly f, final FileStatistic fs) {
    this.targetFile = targetFile;
    if (fs != null) {
      this.tFileType = "" + fs.fileType;
      this.tFileExtension = fs.fileExtension;
      this.tFileSize = "" + ((fs.fileSize < 1000) ? String.format("%3dB", fs.fileSize)
          : (fs.fileSize < 1000000) ? String.format("%6.2fKB", (double) fs.fileSize / 1000)
              : String.format("%6.2fMB", (double) fs.fileSize / 1000000));
      if (fs instanceof TextFileStatistic) {
        TextFileStatistic tfs = (TextFileStatistic) fs;
        this.tFileNLines = String.format("%7d", tfs.nLines);
        this.tFileN80CharLines = String.format("%11.3f", tfs.n80CharLines);
      }

    }
    if (f != null) {
      this.type = f.getType();
      this.nSpecs = f.getPcrltSpecs().size();
      this.nSrcs = f.getPcrltSrcs().size();
      this.nLs = f.getPcrltLicenseExprs().size();
      this.nL2Commits = (int) f.getPcrltLicenseExprs().values().stream()
          .filter(l -> l.getPcrltLicense2CommitMapping() != null).count();
      this.nLBackgrounds = (int) f.getPcrltLicenseExprs().values().stream()
          .filter(l -> l.getPcrltLicenseBackground() != null).count();
      this.nRHAnnotations = f.getReuseRightsHs().size();
      this.nRhs = H.distinctUsingEqualsIdentity(f.getReuseRightsHs()).size();
      this.nRHBackgrounds = (int) f.getPcrltIdentityGroups().values().stream()
          .filter(pig -> pig.getCopyrightBackground() != null).count();
      this.nRH2Commits = (int) f.getPcrltIdentityGroups().values().stream()
          .filter(pig -> pig.getRightsH2CommitMapping() != null).count();
      this.nIdentityGroups = f.getPcrltIdentityGroups().size();
      this.nIdentities = (int) f.getPcrltIdentityGroups().values().stream()
          .filter(pig -> pig.getPcrltIdentities().size() == 1).count();
      this.nIdentiesExplicitly = (int) f.getPcrltIdentityGroups().values().stream()
          .filter(pig -> (pig.getPcrltIdentities().size() == 1)
              && !pig.getPcrltIdentities().values().iterator().next().isImplicit())
          .count();
      final List<PcrltIdentityRepo> resolveAnnotations = new ArrayList<>();
      final List<PcrltIdentity> map2GitIdAnnotations = new ArrayList<>();
      final List<PcrltAuthor> authorAnnotations = new ArrayList<>();
      final List<PcrltPDataPolicy> pDataPolicies = new ArrayList<>();
      final List<PcrltRightsHARMGPolicy> nAddEtcPolicies = new ArrayList<>();
      for (final PcrltIdentityGroup pig : f.getPcrltIdentityGroups().values().stream()
          .filter(pig -> pig.getPcrltIdentities().size() == 1).collect(Collectors.toList())) {
        PcrltIdentity pi = pig.getPcrltIdentities().values().iterator().next();
        resolveAnnotations.addAll(pi.getRepos());
        if (pi.getMappedGitIdentity() != null) {
          map2GitIdAnnotations.add(pi.getMappedGitIdentity());
        }
        authorAnnotations.addAll(pi.getAuthorAnnotations());
        if (pi.getPDataPolicy() != null) {
          pDataPolicies.add(pi.getPDataPolicy());
        }
        if (pi.getRightsHARMGPolicy() != null) {
          nAddEtcPolicies.add(pi.getRightsHARMGPolicy());
        }
      }
      this.nResolveAnnotations = resolveAnnotations.size();
      this.nMap2GitIdAnnotations = map2GitIdAnnotations.size();
      this.nAuthorAnnotations = authorAnnotations.size();
      this.nAuthors = H.distinctUsingEqualsIdentity(authorAnnotations).size();
      this.nAuthorBackgrounds = (int) f.getPcrltIdentityGroups().values().stream()
          .filter(pig -> pig.getAuthorBackground() != null).count();
      this.nAuthor2Commits = (int) f.getPcrltIdentityGroups().values().stream()
          .filter(pig -> pig.getAuthor2CommitMapping() != null).count();
      this.nPersDataPolicies = pDataPolicies.size();
      this.nAddEtcPolicies = nAddEtcPolicies.size();
      this.nExtAnnotations = f.getExtAnnotations().size();
      this.badgeLevel = f.getBadgeLevel();
    }
  }

  /**
   * @param fileHeaders
   * @param csvFile
   * @throws IOException
   */
  public static void writeCsv(final Map<String, FileHeaderReadOnly> fileHeaders,
      final Map<String, FileStatistic> fileStatistics, final String csvFile) throws IOException {

    List<List<String>> records = getCsvRecords(fileHeaders, fileStatistics);
    try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(csvFile));
        //setQuote(null) is to use padding without quotes, padding is used to have a more stable file size of the csv file
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.builder().setQuote(null)
            .setHeader(// @formatter:off
                    "TargetFile",
                    "TFileType",
                    "TFileExtension",
                    "TFileSize",
                    "TFileNLines",
                    "TFileN80CharLines",
                    "FileHeaderType",
                    "badgeLevel",
                    "nSpecs",
                    "nSrcs",
                    "nLs",
                    "nL2Commits",
                    "nLBackgrounds",
                    "nRHAnnotations",
                    "nRhs",
                    "nRHBackgrounds",
                    "nRH2Commits",
                    "nIdentityGroups",
                    "nIdentities",
                    "nIdentiesExplicitly",
                    "nResolveAnnotations",
                    "nMap2GitIdAnnotations",
                    "nAuthorAnnotations",
                    "nAuthors",
                    "nAuthorBackgrounds",
                    "nAuthor2Commits",
                    "nPersDataPolicies",
                    "nAddEtcPolicies",
                    "nExtAnnotations")// @formatter:on
            .build());) {
      for (final List<String> record : records) {
        csvPrinter.printRecord(record);
      }
      csvPrinter.flush();
    }
  }

  static List<List<String>> getCsvRecords(final Map<String, FileHeaderReadOnly> fileHeaders,
      Map<String, FileStatistic> fileStatistics) {
    List<List<String>> records = new ArrayList<>();
    for (final Entry<String, FileHeaderReadOnly> ef : fileHeaders.entrySet()) {
      final FileHeaderCsv fc = new FileHeaderCsv(ef.getKey(), ef.getValue(), fileStatistics.get(ef.getKey()));
      List<String> record = Arrays.asList(
          new String[] {// @formatter:off
          fc.targetFile,
          "" + fc.tFileType,
          "" + fc.tFileExtension,
          "" + fc.tFileSize,
          "" + fc.tFileNLines,
          "" + fc.tFileN80CharLines,
          "" + fc.type,
          "" + fc.badgeLevel,
          "" + fc.nSpecs,
          "" + fc.nSrcs,
          "" + fc.nLs,
          "" + fc.nL2Commits,
          "" + fc.nLBackgrounds,
          "" + fc.nRHAnnotations,
          "" + fc.nRhs,
          "" + fc.nRHBackgrounds,
          "" + fc.nRH2Commits,
          "" + fc.nIdentityGroups,
          "" + fc.nIdentities,
          "" + fc.nIdentiesExplicitly,
          "" + fc.nResolveAnnotations,
          "" + fc.nMap2GitIdAnnotations,
          "" + fc.nAuthorAnnotations,
          "" + fc.nAuthors,
          "" + fc.nAuthorBackgrounds,
          "" + fc.nAuthor2Commits,
          "" + fc.nPersDataPolicies,
          "" + fc.nAddEtcPolicies,
          "" + fc.nExtAnnotations,
          });// @formatter:on
      records.add(record);
    }
    return records;
  }

  public static List<FileHeaderCsv> getFileHeaderStatistics(final Map<String, FileHeaderReadOnly> fileHeaders,
      final Map<String, FileStatistic> fileStatistics) {
    return fileHeaders.entrySet().stream()
        .map(ef -> new FileHeaderCsv(ef.getKey(), ef.getValue(), fileStatistics.get(ef.getKey())))
        .collect(Collectors.toList());
  }

  public static List<List<Integer>> getFileHeaderStatisticsAsInts(final Map<String, FileHeaderReadOnly> fileHeaders,
      final Map<String, FileStatistic> fileStatistics) {
    return getFileHeaderStatistics(fileHeaders, fileStatistics).stream().map(fc -> {
      return Arrays.asList(
          new Integer[] {// @formatter:off
          fc.badgeLevel,
          fc.nSpecs,
          fc.nSrcs,
          fc.nLs,
          fc.nL2Commits,
          fc.nLBackgrounds,
          fc.nRHAnnotations,
          fc.nRhs,
          fc.nRHBackgrounds,
          fc.nRH2Commits,
          fc.nIdentityGroups,
          fc.nIdentities,
          fc.nIdentiesExplicitly,
          fc.nResolveAnnotations,
          fc.nMap2GitIdAnnotations,
          fc.nAuthorAnnotations,
          fc.nAuthors,
          fc.nAuthorBackgrounds,
          fc.nAuthor2Commits,
          fc.nPersDataPolicies,
          fc.nAddEtcPolicies,
          fc.nExtAnnotations,// @formatter:on
      });
    }).collect(Collectors.toList());
  }
}

//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/FileHeaderParser.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/FileHeaderParser.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding FileHeaderParser.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.MalformedInputException;
import java.nio.file.NoSuchFileException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.jgit.dircache.DirCacheIterator;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import emb.helper.HelperI;
import emb.pcrlt.pcrlt_validator.H.StatefulProxy;
import emb.pcrlt.pcrlt_validator.Keyword.KeywordImplementer;
import emb.pcrlt.pcrlt_validator.Main.MFHPolicy;
import emb.pcrlt.pcrlt_validator.model.ExternalAnnotations;
import emb.pcrlt.pcrlt_validator.model.FileHeader;
import emb.pcrlt.pcrlt_validator.model.FileHeader.FileHeaderType;
import emb.pcrlt.pcrlt_validator.model.Issue;

import emb.pcrlt.pcrlt_validator.model.PcrltIdentity;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltAuthor;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltAuthor.PcrltAuthorParams;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityParams;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityRepo;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityRepo.PcrltIdentityRepoParams;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityResolveDescription;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltPDataPolicy;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltPDataPolicy.PcrltPDataPolicyParams;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltRightsHARMGPolicy;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltRightsHARMGPolicy.PcrltRightsHARMGPolicyParams;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltAuthor2CommitMapping;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltAuthorBackground;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltCopyrightBackground;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltRightsH2CommitMapping;
import emb.pcrlt.pcrlt_validator.model.PcrltLicenseExpr.PcrltLicense2CommitMapping;
import emb.pcrlt.pcrlt_validator.model.PcrltLicenseExpr.PcrltLicenseBackground;
import emb.pcrlt.pcrlt_validator.model.PcrltSpec;
import emb.pcrlt.pcrlt_validator.model.PcrltSpec.PcrltSpecParams;
import emb.pcrlt.pcrlt_validator.model.PcrltSpec.PcrltSpecType;
import emb.pcrlt.pcrlt_validator.model.PcrltSrc;
import emb.pcrlt.pcrlt_validator.model.PcrltSrc.PcrltSrcParams;
import emb.pcrlt.pcrlt_validator.model.ReuseLicenseExpr;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH;
import emb.pcrlt.pcrlt_validator.model.Dep5FileHeaderAttributes;
import emb.pcrlt.pcrlt_validator.model.Dep5FileHeaderAttributes.Dep5FileHeaderAttribute;
import emb.pcrlt.pcrlt_validator.model.Dep5FileHeaderAttributes.Dep5FileHeaderAttributesParams;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH.ReuseRightsHParams;

public class FileHeaderParser implements KeywordImplementer {
  private final FileHeader fileheader;
  private final IssueLogger log = Main.getIssueLogger();
  static {
    Keyword
        .registerImplementedKeywords(// @formatter:off
        Keyword.and,
        Keyword.abbreviationAllowed,
        Keyword.addAllowed,
        Keyword.author,
        Keyword.authors,
        Keyword.compliantWithSpec,
        Keyword.conditions,
        Keyword.contactAllowed,
        Keyword.copyrightBackground,
        Keyword.copyrightHolderAddRemoveModifyGroupPolicy,
        Keyword.groupAllowed,
        Keyword.licenseBackground,
        Keyword.mapCopyrightHolderToGitCommits,
        Keyword.mapLicenseToGitCommits,
        Keyword.mapToGitIdentity,
        Keyword.modifyAllowed,
        Keyword.nameAllowed,
        Keyword.noPermissions,
        Keyword.personalDataPolicy,
        Keyword.removeAllowed,
        Keyword.resolveGitIdentity,
        Keyword.resolveIdentity,
        Keyword.specifyIdentity,
        Keyword.specifyIdentityAbbreviation,
        Keyword.specifyIdentityContact,
        Keyword.specifyIdentityName,
        Keyword.srcTrace,
        Keyword.srcTraceRepository,
        Keyword.files,
        Keyword.spdxFileCopyrightText,
        Keyword.spdxLicenseIdentifier,
        Keyword.notePcrlt,
        Keyword.beginFileHeaderFor,
        Keyword.endFileHeaderFor,
        Keyword.copyright,
        Keyword.license,
        Keyword.externalAnnotations,
        Keyword.authorBackground,
        Keyword.upstreamName,//experimental support
        Keyword.upstreamContact,//experimental support
        Keyword.source,//experimental support
        Keyword.mapAuthorToGitCommits
        );// @formatter:on
  }
  public static final String DEP5_FILE_PATH = ".reuse/dep5";

  public FileHeaderParser(final FileHeader fileHeader) {
    this.fileheader = fileHeader;
  }

  static void parseFileHeaders(final Repository repo, final MFHPolicy mfhPolicy, final boolean skipUnused)
      throws IOException, NoSuchAlgorithmException, ParseException {
    final Map<String, FileHeader> fileHeaders = Main.getMainLight().getFileHeaders();
    final String dep5Path = Main.getMainLightLight().getRepositoryDir() + "/" + FileHeaderParser.DEP5_FILE_PATH;
    String dep5FileContent = "";
    if ((new File(dep5Path)).exists()) {
      dep5FileContent = HelperI.DefaultHelper.helper.getFileAsString(dep5Path);
    } else {
      Main.getIssueLogger()
          .addIssue(new Issue(FileHeaderParser.DEP5_FILE_PATH, "The file " + FileHeaderParser.DEP5_FILE_PATH
              + " is expected to be present in PCRLT compliant repositories but was not found. MFHPolicy=" + mfhPolicy,
              Issue.Severity.WARNING, null));
    }
    if (mfhPolicy == MFHPolicy.Dep5First) {
      FileHeaderParser.parseAndValidateDep5(fileHeaders, mfhPolicy, dep5FileContent);
      FileHeaderParser.parseAndValidateStdAndDotLicense(fileHeaders, repo, mfhPolicy, skipUnused);
    } else if (mfhPolicy == MFHPolicy.OneHeaderPerFile) {
      FileHeaderParser.parseAndValidateStdAndDotLicense(fileHeaders, repo, mfhPolicy, skipUnused);
      FileHeaderParser.parseAndValidateDep5(fileHeaders, mfhPolicy, dep5FileContent);
    } else {
      Main.getIssueLogger().addIssue(
          new Issue(null, "this multi file header policy is not implemented:" + mfhPolicy, Issue.Severity.ERROR, null));
    }

    FileHeader.mergePcrltIdentitiesInFileHeaders(fileHeaders);
    FileHeaderParser.checkFileExistences(fileHeaders);
    FileHeaderParser.linkDep5PcrltSpecs(fileHeaders);
  }

  private static void linkDep5PcrltSpecs(final Map<String, FileHeader> fileHeaders) {
    final FileHeader fhdep5 = fileHeaders.get(FileHeaderParser.DEP5_FILE_PATH);
    for (final Entry<String, FileHeader> efh : fileHeaders.entrySet()) {
      final FileHeader fh = efh.getValue();
      if (fh != null) {
        if (fh.getType() == FileHeaderType.dep5) {
          if (fh.getPcrltSpecs() != null && fhdep5 != null) {
            fh.setPcrltSpecs(fhdep5.getPcrltSpecs());
          } else {
            final String issueText = "no specs for: " + fh.getTargetFile() + ", because no specs found in "
                + FileHeaderParser.DEP5_FILE_PATH + " file header.";
            System.out.println(issueText);
            Main.getIssueLogger().addIssue(new Issue(efh.getKey(), issueText, Issue.Severity.INFO, null));
          }
        }
      }
    }
  }

  private static void checkFileExistences(final Map<String, FileHeader> fileHeaders) {
    H.filterMapInplaceAndConsumeTheRemoved(fileHeaders,
        k -> !(new File(Main.getMainLight().getRepositoryDir() + "/" + k)).exists(),
        k -> Main.getIssueLogger().addIssue(new Issue(k, "targetFile not found", Issue.Severity.ERROR, null)));
  }

  private static void parseAndValidateStdAndDotLicense(final Map<String, FileHeader> fileHeaders, final Repository repo,
      final MFHPolicy mfhPolicy, final boolean skipUnused)
      throws NoWorkTreeException, CorruptObjectException, IOException, ParseException {
    final IssueLogger log = Main.getIssueLogger();
    final AbstractTreeIterator treeIterator = new DirCacheIterator(repo.readDirCache());
    final TreeWalk treeWalk = new TreeWalk(repo);
    treeWalk.addTree(treeIterator);
    treeWalk.setRecursive(true);
    while (treeWalk.next()) {
      final String currFileWithRelPath = treeWalk.getPathString();
      System.out.println(currFileWithRelPath);
      if (!fileHeaders.containsKey(currFileWithRelPath)) {
        fileHeaders.put(currFileWithRelPath, null);// this is needed in case no file header is found
      }
      assert (fileHeaders.get(currFileWithRelPath) == null)
          || currFileWithRelPath.equals(fileHeaders.get(currFileWithRelPath).getTargetFile());
      try {
        final FileHeaderType fh_type = (currFileWithRelPath.matches(".*[.]license$"))
            ? FileHeaderType.valueOf("dotLicense")
            : FileHeaderType.valueOf("std");
        final String fh_TargetFile = (fh_type.equals(FileHeaderType.dotLicense))
            ? currFileWithRelPath.substring(0, currFileWithRelPath.length() - 8)
            : currFileWithRelPath;
        final FileHeader fh = new FileHeader(fh_TargetFile, fh_type);
        if (FileHeaderParser.isSkipBasedOnPath(currFileWithRelPath)) {
          System.out.println("skipped file (based on path regex):" + currFileWithRelPath);
        } else {
          if (skipUnused && fileHeaders.containsKey(fh.getTargetFile()) && fileHeaders.get(fh.getTargetFile()) != null
              && !FileHeader.newHasPrecedenceOverOld(fh, fileHeaders.get(fh.getTargetFile()), mfhPolicy, null)) {// skip
                                                                                                                 // condition
            System.out.println("skipped file:" + currFileWithRelPath);// no issue is raised, because otherwise all dep5
                                                                      // files would raise issue, because without
                                                                      // parsing it is not possible to detect if the
                                                                      // file contain a std fileheader. skipping is used
                                                                      // to reduce issues not to increase.
          } else {
            final FileHeaderParser fhp = new FileHeaderParser(fh);
            final String currFileContent = HelperI.DefaultHelper.helper
                .getFileAsString(Main.getMainLightLight().getRepositoryDir() + "/" + currFileWithRelPath);
            // System.out.println(currFileContent);
            final List<String> lines = Arrays.asList(currFileContent.split("\n"));
            if (fh.getType() == FileHeaderType.std
                && fh.getTargetFile().contentEquals(FileHeaderParser.DEP5_FILE_PATH)) {
              FileHeaderParser.parseDep5Attributes(fh, lines);
            }
            final Integer maxFileHeaderSizeNullable = Main.getMainLightLight().getMaxFileHeaderSize();
            final int maxFileHeaderSize = (maxFileHeaderSizeNullable != null) ? maxFileHeaderSizeNullable
                : lines.size();
            for (int i = 0; i < lines.size() && i <= maxFileHeaderSize; i++) {
              if (lines.get(i).matches(".{0,3}" + Keyword.spdxFileCopyrightText + ":.*")) {
                fhp.parseReuseFileCopyrightText(lines.get(i), false, i);
              }
              if (lines.get(i).matches(".{0,3}" + Keyword.spdxLicenseIdentifier + ":.*")) {
                fhp.parseReuseLicenseIdentifier(lines.get(i), false, i);
              }
              if (lines.get(i).matches(".{0,3}" + Keyword.notePcrlt + ".*")) {
                // System.out.println(lines.get(i));
                if (lines.get(i).contains("<text>")) {
                  final String[] textValueArr = lines.get(i).split("<text>", -1);
                  String textValue = textValueArr[textValueArr.length - 1];
                  for (int j = i; j < lines.size(); j++) {
                    if (j == i) {
                      if (textValue.contains("</text>")) {// cope with case <text>some string in one line</text>
                        final String[] textValueArrEnd = textValue.split("</text>", -1);
                        textValue += textValueArrEnd[0];
                        break;
                      }
                    } else {
                      if (!lines.get(j).contains("</text>")) {
                        textValue += lines.get(j) + "\n";
                      } else {
                        final String[] textValueArrEnd = lines.get(j).split("</text>", -1);
                        textValue += textValueArrEnd[0];
                        break;
                      }
                    }
                  }
                  fhp.parsePcrltAnnotation(lines.get(i), textValue, i);
                  // System.out.println("found text value:"+textValue);
                } else {
                  fhp.parsePcrltAnnotation(lines.get(i), null, i);
                }
              }
              if (currFileWithRelPath.endsWith("dep5") && lines.get(i).matches("^" + Keyword.files + ":.*")) {// old:
                                                                                                              // .equals(".reuse/dep5")
                break;
              }
            }
            System.out.println("ended loop through lines of:" + currFileWithRelPath);
            // System.out.println("fileheader:"+fileHeader);
            if (!fh.isEmpty()) {
              if (!fileHeaders.containsKey(fh.getTargetFile()) || fileHeaders.get(fh.getTargetFile()) == null) {
                fileHeaders.put(fh.getTargetFile(), fh);
              } else if (FileHeader.newHasPrecedenceOverOld(fh, fileHeaders.get(fh.getTargetFile()), mfhPolicy, null,
                  skipUnused)) {// in case of skipUnused==false issues should be thrown, in case of
                                // skipUnused==true, the issues were already thrown by the former call of
                                // newHasPrecedenceOverOld
                final FileHeader fh_old = fileHeaders.get(fh.getTargetFile());// seems to be unreachable code..
                log.addIssue(new Issue(fh.getTargetFile(),
                    "multiple file headers found - where new has precendence over old; old=" + fh_old.getTargetFile()
                        + "(" + fh_old.getType() + ");new=" + fh.getTargetFile() + "(" + fh.getType() + "); policy="
                        + mfhPolicy,
                    Issue.Severity.WARNING, null));
                fileHeaders.put(fh.getTargetFile(), fh);
              } else {
                // ignore
                final FileHeader fh_old = fileHeaders.get(fh.getTargetFile());
                log.addIssue(new Issue(fh.getTargetFile(),
                    "multiple file headers found - where old has precendence over new (or undecidable precedence); old="
                        + fh_old.getTargetFile() + "(" + fh_old.getType() + ");new=" + fh.getTargetFile() + "("
                        + fh.getType() + "); policy=" + mfhPolicy,
                    Issue.Severity.WARNING, null));
              }
            }
          }
        }
      } catch (final MalformedInputException e) {

      } catch (final NoSuchFileException e) {
        log.addIssue(new Issue(currFileWithRelPath,
            "The file " + currFileWithRelPath + " was found in the git index but not in the repository dir",
            Issue.Severity.ERROR, null));
      }
    }
    treeWalk.close();
  }

  static void parseDep5Attributes(final FileHeader fh, final List<String> lines) {
    final StatefulProxy<ParCtx> sp = new StatefulProxy<>();// helper object for convenience during if requests
    final Dep5FileHeaderAttributesParams dep5FHAParms = new Dep5FileHeaderAttributesParams();
    for (int i = 0; i < lines.size(); i++) {// iterate over all lines in dep5 file. (this could be embedded in
                                            // parseAndValidateDep5 but was outsourced to reduce complexity)
      if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(false, fh.getTargetFile(), Keyword.upstreamName,
          lines.get(i), null, i)) != null) {
        final ParCtx ctx = sp.returnLastInput();
        dep5FHAParms.upstreamName = new Dep5FileHeaderAttribute(ctx, ctx.text);
      } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(false, fh.getTargetFile(), Keyword.upstreamContact,
          lines.get(i), null, i)) != null) {
        final ParCtx ctx = sp.returnLastInput();
        dep5FHAParms.upstreamContact = new Dep5FileHeaderAttribute(ctx, ctx.text);
      } else if (sp.saveAndReturn(
          FileHeaderParser.parseAnnotation(false, fh.getTargetFile(), Keyword.source, lines.get(i), null, i)) != null) {
        final ParCtx ctx = sp.returnLastInput();
        dep5FHAParms.source = new Dep5FileHeaderAttribute(ctx, ctx.text);
      }
    }
    fh.setDep5Attributes(new Dep5FileHeaderAttributes(dep5FHAParms));
  }

  static void parseAndValidateDep5(final Map<String, FileHeader> fileHeaders, final MFHPolicy mfhPolicy,
      final String dep5FileContent)// only package visibility to allow testing
      throws IOException, NoSuchAlgorithmException, ParseException {
    // Map<String, FileHeader> fileHeaders = Main.getMainLight().getFileHeaders();
    final IssueLogger log = Main.getIssueLogger();
    System.out.println(FileHeaderParser.DEP5_FILE_PATH);
    // fileHeaders.put(currFileWithRelPath, null);//has already std fileheader

    // System.out.println(currFileContent);
    final List<String> lines = Arrays.asList(dep5FileContent.split("\n"));
    List<FileHeader> targetFileHeaderGroup = null;// the group of files specified by "Files:.*"
    boolean isPcrltHeaderStarted = false;// if the pcrlt header section has started for a specific targetfile.
    int groupIndex = -1;// the index of the matched target files (in the argetFileHeaerGroup)??
    boolean isCopyrightStarted=false;//to support copyright holders in multiple lines. copyright keywords are expected to occur before license keywords.
    for (int i = 0; i < lines.size(); i++) {// iterate over all lines in dep5 file.
      if (targetFileHeaderGroup == null && !lines.get(i).matches("^" + Keyword.files + ":.*")) {
        continue;// if targetFileHeaderGroup does not yet exists and no "Files:.*" line is found,
                 // continue with next line
      }
      if (lines.get(i).matches("^" + Keyword.files + ":.*")) {
        if (targetFileHeaderGroup != null) {// this section handles the last targetFileHeaderGroup (the very last group
                                            // is handled after the lines loop). if targetFileHeaderGroup is not null
                                            // iterate over the targetFileHeaderGroup file headers
          for (final FileHeader f : targetFileHeaderGroup) {
            if (!fileHeaders.containsKey(f.getTargetFile()) || fileHeaders.get(f.getTargetFile()) == null) {
              fileHeaders.put(f.getTargetFile(), f);// register the targetFileHeaderGroup fileHeader.
            } else if (FileHeader.newHasPrecedenceOverOld(f, fileHeaders.get(f.getTargetFile()), mfhPolicy, i)) {
              final FileHeader fh_old = fileHeaders.get(f.getTargetFile());
              log.addIssue(new Issue(f.getTargetFile(),
                  "multiple file headers found - where new has precendence over old; old=" + fh_old.getTargetFile()
                      + "(" + fh_old.getType() + ");new=" + f.getTargetFile() + "(" + f.getType() + "); policy="
                      + mfhPolicy,
                  Issue.Severity.WARNING, i));
              fileHeaders.put(f.getTargetFile(), f);// register the targetFileHeaderGroup fileHeader if precedence is
                                                    // true.
            } else {
              // ignore
              final FileHeader fh_old = fileHeaders.get(f.getTargetFile());
              log.addIssue(new Issue(f.getTargetFile(),
                  "multiple file headers found - where old has precendence over new (or undecidable precedence); old="
                      + fh_old.getTargetFile() + "(" + fh_old.getType() + ");new=" + f.getTargetFile() + "("
                      + f.getType() + "); policy=" + mfhPolicy,
                  Issue.Severity.WARNING, i));
              continue;
            }
          }
        }
        // start new file header
        isPcrltHeaderStarted = false;// set to false after "Files:.*" was found
        final String filesRegex = "^" + Keyword.files + ":(.*)$";// regex with group for the file-list
        final Pattern pattern = Pattern.compile(filesRegex);
        // System.out.println("going to parse:"+raw);
        if (lines.get(i).contains("*")) {
          final String issueText = "This parser currently only supports space separated file lists - no asterix expansions"
              + lines.get(i);
          final Issue iss = new Issue(FileHeaderParser.DEP5_FILE_PATH, issueText, Issue.Severity.ERROR, i);
          System.err.println("" + iss);
          log.addIssue(iss);
        }
        final Matcher matcher = pattern.matcher(lines.get(i));
        if (matcher.matches()) {// if "^Files:(.*)$" is found, create new targetFileHeaderGroup
          targetFileHeaderGroup = new ArrayList<>();
          final String[] files = matcher.group(1).strip().split("\\s+");// split by space - this means the files have to
                                                                        // be
          // specified one by one
          for (final String f : files) {
            final FileHeaderType fh_type = FileHeaderType.valueOf("dep5");
            final Integer fh_lino = i;
            final FileHeader fh = new FileHeader(f, fh_type, fh_lino);
            targetFileHeaderGroup.add(fh);// create file header for all files found and add them to the
                                          // targetFileHeaderGroup
          }
          groupIndex = 0;// set to 0 every time a new "^Files:(.*)$" line is found.
        }
      }
      if (lines.get(i).matches("^" + Keyword.copyright + ":.*")) {
        isCopyrightStarted=true;
        for (final FileHeader fh : targetFileHeaderGroup) {
          (new FileHeaderParser(fh)).parseReuseFileCopyrightText(lines.get(i), true, i);
        }
      } else if (lines.get(i).matches("^" + Keyword.license + ":.*")) {
        isCopyrightStarted=false;
        for (final FileHeader fh : targetFileHeaderGroup) {
          (new FileHeaderParser(fh)).parseReuseLicenseIdentifier(lines.get(i), true, i);
        }
      }else if(isCopyrightStarted) {
        for (final FileHeader fh : targetFileHeaderGroup) {
          (new FileHeaderParser(fh)).parseReuseFileCopyrightText(lines.get(i), true, i);
        }
      }
      if (groupIndex < targetFileHeaderGroup.size()) {
        final String beginFileHeaderStr = ".{0,3}" + Keyword.notePcrlt + ":\\s*" + Keyword.beginFileHeaderFor + ":\\s*"
            + targetFileHeaderGroup.get(groupIndex).getTargetFile()+"\\s*";
        if (lines.get(i).matches(beginFileHeaderStr)) {
          System.out.println("found:" + beginFileHeaderStr);
          isPcrltHeaderStarted = true;
        }
        final String endFileHeaderStr = ".{0,3}" + Keyword.notePcrlt + ":\\s*" + Keyword.endFileHeaderFor + ":\\s*"
            + targetFileHeaderGroup.get(groupIndex).getTargetFile()+"\\s*";
        if (lines.get(i).matches(endFileHeaderStr)) {
          System.out.println("found:" + endFileHeaderStr);
          isPcrltHeaderStarted = false;
          groupIndex += 1;
        }
      }
      if (isPcrltHeaderStarted) {
        final FileHeaderParser fhp = new FileHeaderParser(targetFileHeaderGroup.get(groupIndex));
        if (lines.get(i).matches(".{0,3}" + Keyword.spdxFileCopyrightText + ":.*")) {
          fhp.parseReuseFileCopyrightText(lines.get(i), false, i);
        }
        if (lines.get(i).matches(".{0,3}" + Keyword.spdxLicenseIdentifier + ":.*")) {
          fhp.parseReuseLicenseIdentifier(lines.get(i), false, i);
        }
        if (lines.get(i).matches(".{0,3}" + Keyword.notePcrlt + ".*")) {
          // System.out.println(lines.get(i));
          if (lines.get(i).contains("<text>")) {
            final String[] textValueArr = lines.get(i).split("<text>", -1);
            String textValue = textValueArr[textValueArr.length - 1];
            for (int j = i; j < lines.size(); j++) {
              if (j == i) {
                if (textValue.contains("</text>")) {// cope with case <text>some string in one line</text>
                  final String[] textValueArrEnd = textValue.split("</text>", -1);
                  textValue += textValueArrEnd[0];
                  break;
                }
              } else {
                if (!lines.get(j).contains("</text>")) {
                  textValue += lines.get(j) + "\n";
                } else {
                  final String[] textValueArrEnd = lines.get(j).split("</text>", -1);
                  textValue += textValueArrEnd[0];
                  break;
                }
              }
            }
            fhp.parsePcrltAnnotation(lines.get(i), textValue, i);
            // System.out.println("found text value:"+textValue);
          } else {
            fhp.parsePcrltAnnotation(lines.get(i), null, i);
          }
        }
      }
    }
    if (targetFileHeaderGroup != null) {// this handles the very last header group (this is not inside the loop)
      for (final FileHeader f : targetFileHeaderGroup) {
        if (!fileHeaders.containsKey(f.getTargetFile()) || fileHeaders.get(f.getTargetFile()) == null) {
          fileHeaders.put(f.getTargetFile(), f);
        } else if (FileHeader.newHasPrecedenceOverOld(f, fileHeaders.get(f.getTargetFile()), mfhPolicy, f.getLino())) {
          final FileHeader fh_old = fileHeaders.get(f.getTargetFile());
          log.addIssue(new Issue(f.getTargetFile(),
              "multiple file headers found - where new has precendence over old; old=" + fh_old.getTargetFile() + "("
                  + fh_old.getType() + ");new=" + f.getTargetFile() + "(" + f.getType() + "); policy=" + mfhPolicy,
              Issue.Severity.WARNING, f.getLino()));
          fileHeaders.put(f.getTargetFile(), f);
        } else {
          final FileHeader fh_old = fileHeaders.get(f.getTargetFile());
          log.addIssue(new Issue(f.getTargetFile(),
              "multiple file headers found - where old has precendence over new (or undecidable precedence); old="
                  + fh_old.getTargetFile() + "(" + fh_old.getType() + ");new=" + f.getTargetFile() + "(" + f.getType()
                  + "); policy=" + mfhPolicy,
              Issue.Severity.WARNING, f.getLino()));
          continue;
        }
      }
    }
  }

  void parseReuseFileCopyrightText(final String raw, final boolean isDep5, final int lino) {
    System.out.println("going to parse:" + raw);
    if (!isDep5) {
      final ParCtx ctx = FileHeaderParser.parseAnnotation(false, this.fileheader.getTargetFile(),
          Keyword.spdxFileCopyrightText, raw, null, lino);
      final String reuseRightsHRegex = ".{0,3}(" + Keyword.spdxFileCopyrightText
          + ":((?:\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)(?:\\s?,\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)*)?(?:(.*?)(?:<(.*)>)?))$";
      final Pattern pattern = Pattern.compile(reuseRightsHRegex);
      final Matcher matcher = pattern.matcher(raw);
      if (matcher.matches()) {
        // System.out.println("number of groups: "+matcher.groupCount());
        final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
        rightsHParms.text = matcher.group(1);
        rightsHParms.year = matcher.group(2);
        rightsHParms.name = matcher.group(3);
        rightsHParms.contact = matcher.group(4);
        rightsHParms.isDep5 = isDep5;
        final ReuseRightsH rh = new ReuseRightsH(ctx, rightsHParms);
        if (rh.hasIdentifier()) {
          this.fileheader.addReuseFileCopyrightText(rh);
        } // else: issue has already been raised by reuserightsh constructor
        System.out.println("parsed ReuseRightsH:" + rh);
      } else {// seems to be impossible to reach..
        throw new IllegalStateException("seems to be impossible to reach");
      }
    } else {
      final ParCtx ctx = FileHeaderParser.parseAnnotation(false, this.fileheader.getTargetFile(), Keyword.copyright,
          raw, null, lino);
      final String reuseRightsHRegex = "^(" + Keyword.copyright
          + ":((?:\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)(?:\\s?,\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)*)?(.*?)(?:<(.*)>)?)$";
      final Pattern pattern = Pattern.compile(reuseRightsHRegex);
      final Matcher matcher = pattern.matcher(raw);
      if (matcher.matches()) {
        // System.out.println("number of groups: "+matcher.groupCount());
        final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
        rightsHParms.text = matcher.group(1);
        rightsHParms.year = matcher.group(2);
        rightsHParms.name = matcher.group(3);
        rightsHParms.contact = matcher.group(4);
        rightsHParms.isDep5 = isDep5;
        final ReuseRightsH rh = new ReuseRightsH(ctx, rightsHParms);
        if (rh.hasIdentifier()) {
          this.fileheader.addReuseFileCopyrightText(rh);
        }
        System.out.println("parsed ReuseRightsH:" + rh);
      } else {// seems to be impossible to reach..
        System.out.println("this seems to be the 2+ line of a multiline copyright expression:" + raw);
        //throw new IllegalStateException("seems to be impossible to reach");
        final String reuseRightsHRegex2 = "^(\\s*((?:\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)(?:\\s?,\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)*)?(?:(.*?)(?:<(.*)>)?))$";
        final Pattern pattern2 = Pattern.compile(reuseRightsHRegex2);
        final Matcher matcher2 = pattern2.matcher(raw);
        if (matcher2.matches()) {
          // System.out.println("number of groups: "+matcher.groupCount());
          final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
          rightsHParms.text = matcher2.group(1);
          rightsHParms.year = matcher2.group(2);
          rightsHParms.name = matcher2.group(3);
          rightsHParms.contact = matcher2.group(4);
          rightsHParms.isDep5 = isDep5;
          System.out.println("parsed possible ReuseRightsH:" + rightsHParms);
          final ParCtx ctx2=new ParCtx(this.fileheader.getTargetFile(), raw, null, null, null, rightsHParms.text, lino);
          final ReuseRightsH rh = new ReuseRightsH(ctx2, rightsHParms);
          if (rh.hasIdentifier()) {
            this.fileheader.addReuseFileCopyrightText(rh);
          } // else: issue has already been raised by reuserightsh constructor
          System.out.println("parsed ReuseRightsH:" + rh);
        }
      }
    }
    System.out.println("found reuseRightsH:" + raw);
  }

  private void parseReuseLicenseIdentifier(final String raw, final boolean isDep5, final int lino) {
    if (!isDep5) {
      final ParCtx ctx = FileHeaderParser.parseAnnotation(false, this.fileheader.getTargetFile(),
          Keyword.spdxLicenseIdentifier, raw, null, lino);
      final String licenseExpr = ctx.text;
      final ReuseLicenseExpr rl = new ReuseLicenseExpr(ctx, licenseExpr, isDep5);
      this.fileheader.addReuseLicenseIdentifier(rl);
    } else {
      final ParCtx ctx = FileHeaderParser.parseAnnotation(false, this.fileheader.getTargetFile(), Keyword.license, raw,
          null, lino);
      final String licenseExpr = ctx.text;
      final ReuseLicenseExpr rl = new ReuseLicenseExpr(ctx, licenseExpr, isDep5);
      this.fileheader.addReuseLicenseIdentifier(rl);
    }
    System.out.println("found reuseLicenseExpr:" + raw);
  }

  private void parsePcrltAnnotation(final String raw, final String textValue, final int lino) throws ParseException {
    final StatefulProxy<ParCtx> sp = new StatefulProxy<>();// helper object for convenience during if requests
    if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(false, this.fileheader.getTargetFile(),
        Keyword.compliantWithSpec, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String[] lineArgs = ctx.text.split(":");
      System.out.println("parts:" + lineArgs.length);
      if (lineArgs.length != 3) {// last one is https:
        this.log.createAndLogIssue(Issue.Severity.WARNING, ctx,
            "compliant-with-spec annotation is erroneous1:" + ctx.line);
      } else {
        final PcrltSpecParams specParms = new PcrltSpecParams();
        specParms.text = ctx.text;
        specParms.url = (lineArgs[1] + ":" + lineArgs[2]).strip();
        final String typeVersion = lineArgs[0];
        if (typeVersion.contains("SPDX")) {
          specParms.type = PcrltSpecType.SPDX;
          specParms.version = typeVersion.split("SPDX")[1].strip();
        } else if (typeVersion.contains("REUSE")) {
          specParms.type = PcrltSpecType.REUSE;
          specParms.version = typeVersion.split("REUSE")[1].strip();
        } else if (typeVersion.contains("PCRLT")) {
          specParms.type = PcrltSpecType.PCRLT;
          specParms.version = typeVersion.split("PCRLT")[1].strip();
        } else {
          this.log.createAndLogIssue(Issue.Severity.WARNING, ctx,
              "compliant-with-spec annotation is erroneous2:" + ctx.line);
        }
        final PcrltSpec pcrltSpec = new PcrltSpec(ctx, specParms);
        this.fileheader.addPcrltSpec(pcrltSpec, lino);
      }
      System.out.println("found pcrltSpec:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(false, this.fileheader.getTargetFile(),
        Keyword.srcTraceRepository, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltSrcParams srcParms = new PcrltSrcParams();
      srcParms.text = ctx.text;
      srcParms.type = ctx.keyword;
      final String[] lineArgs = ctx.text.split(":");
      if (lineArgs.length < 4) {// one is https: or http: the url might contain more ":"
        this.log.createAndLogIssue(Issue.Severity.INFO, ctx, "" + srcParms.type
            + " annotation text has too few parts (split by ':')(noParts=," + lineArgs.length + "line=" + raw);
      } else {
        srcParms.url = Arrays.asList(lineArgs).stream().skip(2).collect(Collectors.joining(":"));// the url might
                                                                                                 // contain
        // more ":"
        srcParms.year = lineArgs[0].strip();
        srcParms.access = lineArgs[1].strip();
        final String[] repoParts = srcParms.url.split("#");
        if (repoParts.length > 1) {
          srcParms.filePath = repoParts[repoParts.length - 1].strip();
          srcParms.repoUrl = repoParts[0].strip();
        } else {
          this.log.createAndLogIssue(Issue.Severity.WARNING, ctx,
              "" + Keyword.srcTraceRepository + " annotation seems to have no #-prefixed filePath included:" + raw);
        }
      }
      final PcrltSrc pcrltSrc = new PcrltSrc(ctx, srcParms);
      this.fileheader.addPcrltSrc(pcrltSrc);
      System.out.println("found pcrltSrc:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(false, this.fileheader.getTargetFile(),
        Keyword.srcTrace, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltSrcParams srcParms = new PcrltSrcParams();
      srcParms.text = ctx.text;
      srcParms.type = ctx.keyword;
      final String[] lineArgs = ctx.text.split(":");
      if (lineArgs.length < 4) {// one is https: or http: the url might contain more ":"
        this.log.createAndLogIssue(Issue.Severity.WARNING, ctx, "" + srcParms.type
            + " annotation text has too few parts (split by ':')(noParts=," + lineArgs.length + "line=" + raw);
      } else {
        srcParms.url = Arrays.asList(lineArgs).stream().skip(2).collect(Collectors.joining(":"));// the url might
                                                                                                 // contain
        // more ":"
        srcParms.year = lineArgs[1].strip();
        srcParms.access = lineArgs[2].strip();
      }
      final PcrltSrc pcrltSrc = new PcrltSrc(ctx, srcParms);
      this.fileheader.addPcrltSrc(pcrltSrc);
      System.out.println("found pcrltSrc:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.specifyIdentityName, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      identityParms.abbrId = ctx.id;
      identityParms.name = ctx.text;
      final PcrltIdentity pi = new PcrltIdentity(ctx, identityParms);
      this.fileheader.addPcrltIdentity(pi);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.specifyIdentityContact, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      identityParms.abbrId = ctx.id;
      identityParms.contact = ctx.text;
      final PcrltIdentity pi = new PcrltIdentity(ctx, identityParms);
      this.fileheader.addPcrltIdentity(pi);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.specifyIdentityAbbreviation, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      identityParms.abbrId = ctx.id;
      identityParms.abbr = ctx.text;
      final PcrltIdentity pi = new PcrltIdentity(ctx, identityParms);
      this.fileheader.addPcrltIdentity(pi);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.specifyIdentity, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      identityParms.abbrId = ctx.id;
      final String identityRegex = "(.*?)(<(.*)>)?$";
      final Pattern pattern = Pattern.compile(identityRegex);
      final Matcher matcher = pattern.matcher(ctx.text);
      if (matcher.matches()) {
        identityParms.name = matcher.group(1);
        identityParms.contact = matcher.group(3);
        final PcrltIdentity pi = new PcrltIdentity(ctx, identityParms);
        this.fileheader.addPcrltIdentity(pi);
      } else {
        this.log.createAndLogIssue(Issue.Severity.ERROR, ctx, "could not parse:" + raw);
      }
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.resolveGitIdentity, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String[] repoParts = ctx.text.split("#");
      final PcrltIdentityRepoParams repoParms = new PcrltIdentityRepoParams();
      repoParms.text = ctx.text;
      repoParms.abbrId = ctx.id;
      if (repoParts.length > 1) {
        repoParms.filePath = repoParts[repoParts.length - 1].strip();
        repoParms.repoUrl = repoParts[0].strip();
      } else {
        this.log.createAndLogIssue(Issue.Severity.INFO, ctx,
            "" + ctx.keyword + " annotation seems to have no #-prefixed filePath included:" + ctx.text);
      }
      final PcrltIdentityRepo pcrltIdentityRepo = new PcrltIdentityRepo(ctx, repoParms);
      this.fileheader.addPcrltIdentityRepo(pcrltIdentityRepo);
      System.out.println("found PcrltIdentityRepo:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.resolveIdentity, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltIdentityResolveDescription pcrltIdentityResolveDescription = new PcrltIdentityResolveDescription(ctx,
          ctx.id, ctx.text);
      this.fileheader.addPcrltIdentityResolveDescription(pcrltIdentityResolveDescription);
      System.out.println("found PcrltIdentityRepo:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.mapToGitIdentity, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String abbrIdGit = ctx.text;
      this.fileheader.addMappedGitIdentity(ctx, ctx.id, abbrIdGit);
      System.out.println("found PcrltIdentityRepo:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.personalDataPolicy, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltPDataPolicyParams pdpParms = new PcrltPDataPolicyParams();
      pdpParms.abbrId = ctx.id;
      final List<String> args2 = Arrays.asList(ctx.text.split(",")).stream().map(s -> s.strip())
          .collect(Collectors.toList());
      pdpParms.isAbbreviationAllowed = args2.contains("" + Keyword.abbreviationAllowed);
      pdpParms.isNameAllowed = args2.contains("" + Keyword.nameAllowed);
      pdpParms.isEmailAllowed = args2.contains("" + Keyword.contactAllowed);
      final PcrltPDataPolicy pcrltPDataPolicy = new PcrltPDataPolicy(ctx, pdpParms);
      this.fileheader.addPcrltPDataPolicy(pcrltPDataPolicy);
      System.out.println("found PcrltPDataPolicy" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.copyrightHolderAddRemoveModifyGroupPolicy, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltRightsHARMGPolicyParams rhARMGPolicyParms = new PcrltRightsHARMGPolicyParams();
      rhARMGPolicyParms.abbrId = ctx.id;
      final List<String> args2 = Arrays.asList(ctx.text.split(",")).stream().map(s -> s.strip())
          .collect(Collectors.toList());
      rhARMGPolicyParms.isNoPermissions = args2.contains("" + Keyword.noPermissions);
      rhARMGPolicyParms.isAddAllowed = args2.contains("" + Keyword.addAllowed);
      rhARMGPolicyParms.isRemoveAllowed = args2.contains("" + Keyword.removeAllowed);
      rhARMGPolicyParms.isModifyAllowed = args2.contains("" + Keyword.modifyAllowed);
      rhARMGPolicyParms.isGroupAllowed = args2.contains("" + Keyword.groupAllowed);
      rhARMGPolicyParms.isExplicitPDataCondition = args2.get(args2.size() - 1)
          .matches("" + Keyword.conditions + ":\\s?" + Keyword.personalDataPolicy);
      final PcrltRightsHARMGPolicy rhARMGPolicy = new PcrltRightsHARMGPolicy(ctx, rhARMGPolicyParms);
      if (!rhARMGPolicy.isValid()) {
        final String issueText = "PcrltRightsHARMGPolicy: something not valid";
        final Issue iss = new Issue(this.fileheader.getTargetFile(), issueText, Issue.Severity.ERROR, lino);
        System.err.println("" + iss);
        this.log.addIssue(iss);
      }
      this.fileheader.addPcrltRightsHARMGPolicy(rhARMGPolicy);
      System.out.println("found PcrltRightsHARMGPolicy:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.mapCopyrightHolderToGitCommits, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String abbrIdExpr = ctx.id;
      final PcrltRightsH2CommitMapping r2c = new PcrltRightsH2CommitMapping(ctx, abbrIdExpr,
          (textValue != null) ? textValue : ctx.text);
      final List<String> abbrIds = FileHeaderParser.parseAbbrIdExpr(abbrIdExpr);
      this.fileheader.addPcrltRightsH2CommitMapping(abbrIdExpr, abbrIds, r2c);
      System.out.println("found PcrltRightsH2CommitMapping:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.mapAuthorToGitCommits, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String abbrIdExpr = ctx.id;
      final PcrltAuthor2CommitMapping r2c = new PcrltAuthor2CommitMapping(ctx, abbrIdExpr,
          (textValue != null) ? textValue : ctx.text);
      final List<String> abbrIds = FileHeaderParser.parseAbbrIdExpr(abbrIdExpr);
      this.fileheader.addPcrltAuthor2CommitMapping(abbrIdExpr, abbrIds, r2c);
      System.out.println("found PcrltAuthor2CommitMapping:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.copyrightBackground, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String abbrIdExpr = ctx.id;
      final PcrltCopyrightBackground cb = new PcrltCopyrightBackground(ctx, abbrIdExpr,
          (textValue != null) ? textValue : ctx.text);
      final List<String> abbrIds = FileHeaderParser.parseAbbrIdExpr(abbrIdExpr);
      this.fileheader.addPcrltCopyrightBackground(abbrIdExpr, abbrIds, cb);
      System.out.println("found PcrltCopyrightBackground:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.authorBackground, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String abbrIdExpr = ctx.id;
      final PcrltAuthorBackground ab = new PcrltAuthorBackground(ctx, abbrIdExpr,
          (textValue != null) ? textValue : ctx.text);
      final List<String> abbrIds = FileHeaderParser.parseAbbrIdExpr(abbrIdExpr);
      this.fileheader.addPcrltAuthorBackground(abbrIdExpr, abbrIds, ab);
      System.out.println("found PcrltAuthorBackground:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.mapLicenseToGitCommits, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String licenseExpr = ctx.id;
      final PcrltLicense2CommitMapping l2c = new PcrltLicense2CommitMapping(ctx, licenseExpr,
          (textValue != null) ? textValue : ctx.text);
      this.fileheader.addPcrltLicense2CommitMapping(licenseExpr, l2c);
      System.out.println("found PcrltLicense2CommitMapping:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.licenseBackground, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final String licenseExpr = ctx.id;
      final PcrltLicenseBackground lb = new PcrltLicenseBackground(ctx, licenseExpr,
          (textValue != null) ? textValue : ctx.text);
      this.fileheader.addPcrltLicenseBackground(licenseExpr, lb);
      System.out.println("found PcrltLicenseBackground:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(), Keyword.author,
        raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final PcrltAuthorParams authorParms = new PcrltAuthorParams();
      authorParms.abbrId = ctx.id;
      authorParms.text = ctx.text;

      final String pcrltAuthorRegex = "(((?:\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)(?:\\s?,\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)*)?(.*?)(?:<(.*)>)?)$";
      final Pattern pattern = Pattern.compile(pcrltAuthorRegex);
      System.out.println("going to parse:" + ctx.text);
      final Matcher matcher = pattern.matcher(ctx.text);
      if (matcher.matches()) {
        // System.out.println("number of groups: "+matcher.groupCount());
        authorParms.year = matcher.group(2);
        authorParms.name = matcher.group(3);
        authorParms.contact = matcher.group(4);
        final PcrltAuthor author = new PcrltAuthor(ctx, authorParms);
        System.out.println("parsed author:" + author);
        this.fileheader.addPcrltAuthor(new PcrltIdentity(author));
      }
      System.out.println("end parsing author:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(), Keyword.authors,
        raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();

      for (final String line : textValue.lines().collect(Collectors.toList())) {
        if (!line.strip().isEmpty()) {
          final String pcrltAuthorRegex = "(?:\\s*|.{0,3}\\s*)(((?:\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)(?:\\s?,\\s?[0-9]{4,4}\\s?(?:-\\s?[0-9]{4,4})?)*)?(.*?)(?:<(.*)>)?)$";
          final Pattern pattern = Pattern.compile(pcrltAuthorRegex);
          System.out.println("going to parse:" + line);
          final Matcher matcher = pattern.matcher(line);
          if (matcher.matches()) {
            // System.out.println("number of groups: "+matcher.groupCount());
            final PcrltAuthorParams authorParms = new PcrltAuthorParams();
            authorParms.abbrId = null;
            authorParms.text = matcher.group(1);
            authorParms.year = matcher.group(2);
            authorParms.name = matcher.group(3);
            authorParms.contact = matcher.group(4);
            final PcrltAuthor a = new PcrltAuthor(ctx, authorParms);
            System.out.println("parsed author:" + a);
            this.fileheader.addPcrltAuthor(new PcrltIdentity(a));
          }
        }
      }
      System.out.println("end parsing authors:" + raw + "\ntextvalue:" + textValue);
    } else if (raw.contains("" + Keyword.beginFileHeaderFor) || raw.contains("" + Keyword.endFileHeaderFor)) {
      System.out.println("found fileheaderstartend-probably-dep5:" + raw);
    } else if (sp.saveAndReturn(FileHeaderParser.parseAnnotation(true, this.fileheader.getTargetFile(),
        Keyword.externalAnnotations, raw, textValue, lino)) != null) {
      final ParCtx ctx = sp.returnLastInput();
      final ExternalAnnotations extAnn = new ExternalAnnotations(ctx, ctx.text);
      this.fileheader.addExternalAnnotations(extAnn);
    } else {
      final String issueText = "could not parse annotation (not implemented or erroneous):" + raw;
      final Issue iss = new Issue(this.fileheader.getTargetFile(), issueText, Issue.Severity.ERROR, lino);
      System.err.println("" + iss);
      this.log.addIssue(iss);
    }
  }

  /**
   * returns the first argument (AbbrId or AbbrIdExpr or licenseExpr)
   * 
   * @param raw
   * @return
   */
  public static String parseId(final String raw) {
    final String[] args = raw.split(":", -1);
    return args[1].strip();
  }

  public static List<String> parseAbbrIdExpr(final String abbrIdExpr) {
    return Arrays.asList(abbrIdExpr.split("" + Keyword.and)).stream().map(s -> s.strip()).collect(Collectors.toList());
  }

  public static ParCtx parseAnnotation(final boolean isIdBased, final String file, final Keyword keyword,
      final String raw, final String textValue, final Integer lino) {
    final String[] args = raw.split("" + keyword + ":", -1);
    if (args.length < 2) {
      return null;
    } else {
      final String abbrId = (isIdBased) ? FileHeaderParser.parseId(raw) : null;
      final String text = args[1];
      return new ParCtx(file, raw, textValue, abbrId, keyword, text, lino);
    }
  }

  static boolean isSkipBasedOnPath(final String file) {
    return file.matches(Main.getMainLight().getSkipFilesRegexForParsing());
  }

  /**
   * ParserContext
   * 
   * @author user
   *
   */
  public static class ParCtx {
    @Expose
    public final String file;
    @Expose
    public final String line;
    @Expose
    public final String multilineText;// textvalue
    @Expose
    public final String id;
    @Expose
    public final Keyword keyword;
    @Expose
    public final String text;// without keyword - usually the text after the first keyword (i.e. text in
    // .*<keyword>:<text>)
    @Expose
    public final Integer lino;// line number

    public ParCtx(final String file, final String line, final String multilineText, final String id,
        final Keyword keyword, final String text, final Integer lino) {
      this.file = file;
      this.line = line;
      this.multilineText = multilineText;
      this.id = id;
      this.keyword = keyword;
      this.text = text;
      this.lino = lino;
    }

    @Override
    public String toString() {
      return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
    }
  }
}

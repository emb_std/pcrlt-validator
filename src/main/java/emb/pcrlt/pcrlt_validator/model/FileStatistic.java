//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/FileStatistic.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/FileStatistic.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding FileStatistic.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

public abstract class FileStatistic {
  public final String filePath;
  @Expose
  public final FileType fileType;// text, binary
  public final String fileExtension;// null or e.g. ".txt"
  // @Expose//this might be os dependent
  public final long fileSize;// byte - should be translated later to KB, MB

  public FileStatistic(String filePath, FileType fileType, String fileExtension, long fileSize) {
    this.filePath = filePath;
    this.fileType = fileType;
    this.fileExtension = fileExtension;
    this.fileSize = fileSize;
  }

  public static class BinaryFileStatistic extends FileStatistic {
    public BinaryFileStatistic(String filePath, FileType fileType, String fileExtension, long fileSize) {
      super(filePath, fileType, fileExtension, fileSize);
    }
  }

  public static class TextFileStatistic extends FileStatistic {
    public TextFileStatistic(String filePath, FileType fileType, String fileExtension, long fileSize, int nLines,
        double n80CharLines) {
      super(filePath, fileType, fileExtension, fileSize);
      this.nLines = nLines;
      this.n80CharLines = n80CharLines;
    }

    @Expose
    public final int nLines;
    @Expose
    public final double n80CharLines;

  }

  public static enum FileType {
    TEXT, BINARY
  }
}

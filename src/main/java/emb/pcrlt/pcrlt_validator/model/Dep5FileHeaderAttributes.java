//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/Dep5FileHeaderAttributes.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/Dep5FileHeaderAttributes.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Dep5FileHeaderAttributes.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;
import emb.pcrlt.pcrlt_validator.H;

public class Dep5FileHeaderAttributes {
  @Expose
  private final Dep5FileHeaderAttribute upstreamName;
  @Expose
  private final Dep5FileHeaderAttribute upstreamContact;
  @Expose
  private final Dep5FileHeaderAttribute source;

  public Dep5FileHeaderAttributes(final Dep5FileHeaderAttributesParams dep5FHAParms) {
    this.upstreamName = dep5FHAParms.upstreamName;
    this.upstreamContact = dep5FHAParms.upstreamContact;
    this.source = dep5FHAParms.source;
  }

  public Dep5FileHeaderAttribute getUpstreamName() {
    return this.upstreamName;
  }

  public Dep5FileHeaderAttribute getUpstreamContact() {
    return this.upstreamContact;
  }

  public Dep5FileHeaderAttribute getSource() {
    return this.source;
  }

  public static class Dep5FileHeaderAttributesParams {
    public Dep5FileHeaderAttribute upstreamName;
    public Dep5FileHeaderAttribute upstreamContact;
    public Dep5FileHeaderAttribute source;
  }

  public static class Dep5FileHeaderAttribute extends Annotation {
    @Expose
    private final String text;

    public Dep5FileHeaderAttribute(final ParCtx parCtx, final String text) {
      super(parCtx);
      this.text = H.strip(text);
    }

    public String getText() {
      return this.text;
    }
  }

}

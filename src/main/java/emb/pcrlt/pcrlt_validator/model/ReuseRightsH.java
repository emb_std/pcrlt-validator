//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/ReuseRightsH.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/ReuseRightsH.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding ReuseRightsH.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.EqualsIdentity;
import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.Keyword;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class ReuseRightsH extends IdAnnotation implements EqualsIdentity{
  /**
   * note: year and text are ignored in equals comparison
   */
  @Expose
  private final String text;
  /**
   * note: year and text are ignored in equals comparison
   */
  @Expose
  private final String year;
  @Expose
  private final String name;
  @Expose
  private final String contact;
  // PcrltIdentity pcrltIdentity;
  @Expose
  private final boolean isDep5;// coming from Copyright:.* line
  static {
    Keyword.registerImplementedKeywords(Keyword.spdxFileCopyrightText, Keyword.copyright);
  }

  public String getName() {
    return this.name;
  }

  public String getContact() {
    return this.contact;
  }

  public String getText() {
    return this.text;
  }

  public Boolean getIsDep5() {
    return this.isDep5;
  }

  public ReuseRightsH(final ParCtx parCtx, final ReuseRightsHParams rightsHParms) {
    super(parCtx, null);
    this.text = H.strip(rightsHParms.text);
    this.year = H.strip(rightsHParms.year);
    this.name = H.strip(rightsHParms.name);
    this.contact = H.strip(rightsHParms.contact);
    this.isDep5 = rightsHParms.isDep5;
    if (!hasIdentifier()) {
      this.log.createAndLogIssue(Issue.Severity.ERROR, parCtx,
          "no identifier: no values for ReuseRightsH could be found in:" + parCtx.line);
    }
  }

  /**
   * year and text are ignored
   */
  @Override
  public boolean equalsIdentity(final Object o) {
    if (o == this) {
      return true;
    }
    /*
     * Check if o is an instance of Complex or not "null instanceof [type]" also
     * returns false
     */
    if (!(o instanceof ReuseRightsH)) {
      System.out.println("this is unexpected usage");
      return false;
    }
    final ReuseRightsH r = (ReuseRightsH) o;
    return H.equals(r.getName(), this.getName()) && H.equals(r.getContact(), this.getContact());
  }
  public boolean equalsIdentityAndYearExpr(Object o) {
    if (!(o instanceof ReuseRightsH)) {
      System.out.println("this is unexpected usage");
      return false;
    }
    final ReuseRightsH r = (ReuseRightsH) o;
    return H.equals(r.getName(), this.getName()) && H.equals(r.getContact(), this.getContact()) && H.equals(r.year,this.year);
  }

  public boolean hasIdentifier() {
    return this.getName() != null || this.getContact() != null;
  }

  public static class ReuseRightsHParams {
    @Expose
    public String text;
    @Expose
    public String year;
    @Expose
    public String name;
    @Expose
    public String contact;
    @Expose
    public boolean isDep5;
    @Override
    public String toString() {
      return new GsonBuilder().setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation().create()
          .toJson(this);
    }
  }

  

}
//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltSpec.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltSpec.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding PcrltSpec.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.H;
//import emb.pcrlt.pcrlt_validator.Main;
//import emb.pcrlt.pcrlt_validator.model.Issue.Level;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class PcrltSpec extends Annotation {// compliance specification
  @Expose
  private final String text;
  @Expose
  private final PcrltSpec.PcrltSpecType type;
  @Expose
  private final String version;
  @Expose
  private final String url;

  public String getText() {
    return this.text;
  }

  public PcrltSpecType getType() {
    return this.type;
  }

  public String getVersion() {
    return this.version;
  }

  public String getUrl() {
    return this.url;
  }

  public PcrltSpec(final ParCtx parCtx, final PcrltSpecParams specParms) {
    super(parCtx);
    this.text = H.strip(specParms.text);
    this.type = specParms.type;
    this.version = H.strip(specParms.version);
    this.url = H.strip(specParms.url);
  }

  public static enum PcrltSpecType {
    SPDX, REUSE, PCRLT
  }

  public static class PcrltSpecParams {
    public String text;
    public PcrltSpecType type;
    public String version;
    public String url;
  }
}
//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltIdentity.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltIdentity.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding PcrltIdentity.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.EqualsIdentity;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

/**
 * PcrltIdentity refers to a natural or legal person. There are different
 * sources for information that might be linked to an identity. The most
 * important attribute is the abbrId. If the abbrId is equal the referred
 * persons are assumed to be equal. The abbrId is expected to be the full or
 * abbreviated name. If the abbrIds are not equal but one of the abbrIds were
 * given implicitly using the identity name, then the persons are assumed equal
 * if the name is equal. If the abbrIds are not equal and not implicitly set,
 * then the persons are assumed to be different even, if the names are equal. If
 * the name is null the contact or the abbr will be used to distinguish. for the
 * detailed algorithm to decide if equality should be assumed, see
 * isReferringToSamePcrltIdentity(..) If the name of a person changes (i.e.
 * after marriage) the two need to have different PcrltIdentity objects, but
 * they can be linked using the map-identity keyword.
 * 
 * @author user
 *
 */
public class PcrltIdentity extends IdAnnotation implements EqualsIdentity {
  /**
   * secondary identifier. specified implicitly by IdAnnotation or explicitly by
   * specify-identity-.. keyword
   */
  @Expose
  private String name;
  @Expose
  private final List<String> contacts = new ArrayList<>();
  @Expose
  private final List<String> abbrs = new ArrayList<>();
  /**
   * primary identifier. specified implicitly by IdAnnotation or explicitly by
   * specify-identity-.. keyword if abbrId not explicit specified (in case of
   * SPDX-FileCopyrightText and PCRLT-Note:authors keyword) the parsed name is
   * used. if abbrId is later specified with a matching name, the new abbrId is
   * replaced and the isImplicit.. flag will be changed.
   */
  // @Expose
  // String abbrId;
  @Expose
  private final boolean isImplicitByAbbrId;
  @Expose
  private final boolean isImplicitByRightsH;
  @Expose
  private final boolean isImplicitByAuthor;
  @Expose
  private final List<PcrltIdentity.PcrltIdentityRepo> repos = new ArrayList<>();
  @Expose
  private final List<PcrltIdentity.PcrltIdentityResolveDescription> resolveDescrs = new ArrayList<>();
  @Expose
  private final List<ReuseRightsH> reuseRightsHs = new ArrayList<>();
  @Expose
  private final List<PcrltIdentity.PcrltAuthor> authorAnnotations = new ArrayList<>();
  @Expose
  private PcrltIdentity.PcrltPDataPolicy pDataPolicy;
  @Expose
  private PcrltIdentity.PcrltRightsHARMGPolicy rightsHARMGPolicy;
  @Expose
  private PcrltIdentity mappedGitIdentity;

  public List<PcrltIdentity.PcrltIdentityRepo> getRepos() {
    return this.repos;
  }

  public String getName() {
    return this.name;
  }

  private void setName(final String name) {
    this.name = name;
  }

  public List<String> getContacts() {
    return this.contacts;
  }

  private void setContacts(final List<String> contacts) {
    H.emptyListAndAddAll(this.contacts, contacts);
  }

  public List<ReuseRightsH> getReuseRightsHs() {
    return this.reuseRightsHs;
  }

  private void setReuseRightsHs(final List<ReuseRightsH> reuseRightsHs) {
    H.emptyListAndAddAll(this.reuseRightsHs, reuseRightsHs);
  }

  public List<PcrltIdentity.PcrltAuthor> getAuthorAnnotations() {
    return this.authorAnnotations;
  }

  private void setAuthorAnnotations(final List<PcrltAuthor> authorAnnotations) {
    H.emptyListAndAddAll(this.authorAnnotations, authorAnnotations);
  }

  public List<String> getAbbrs() {
    return this.abbrs;
  }

  private void setAbbrs(final List<String> abbrs) {
    H.emptyListAndAddAll(this.abbrs, abbrs);
  }

  public boolean isImplicitByAbbrId() {
    return this.isImplicitByAbbrId;
  }

  public boolean isImplicitByRightsH() {
    return this.isImplicitByRightsH;
  }

  public boolean isImplicitByAuthor() {
    return this.isImplicitByAuthor;
  }

  public boolean isImplicit() {
    return this.isImplicitByAbbrId || this.isImplicitByRightsH || this.isImplicitByAuthor;
  }

  public PcrltIdentity getMappedGitIdentity() {
    return this.mappedGitIdentity;
  }

  public PcrltPDataPolicy getPDataPolicy() {
    return this.pDataPolicy;
  }

  public PcrltRightsHARMGPolicy getRightsHARMGPolicy() {
    return this.rightsHARMGPolicy;
  }

  public PcrltIdentity(final ParCtx parCtx, final PcrltIdentityParams identityParms) {
    super(parCtx, identityParms.abbrId);
    if (identityParms.abbrId == null || identityParms.abbrId.strip().isEmpty()) {
      throw new IllegalArgumentException("abbrId==null||abbrId.strip().isEmpty() not allowed");
    }
    this.name = H.strip(identityParms.name);
    if (identityParms.contact != null && !identityParms.contact.strip().isEmpty()) {
      this.contacts.add(identityParms.contact.strip());
    }
    if (identityParms.abbr != null && !identityParms.abbr.strip().isEmpty()) {
      this.abbrs.add(identityParms.abbr.strip());
    }
    this.isImplicitByAuthor = false;
    this.isImplicitByRightsH = false;
    this.isImplicitByAbbrId = identityParms.isImplicitByAbbrId;
  }

  public PcrltIdentity(final ReuseRightsH rrh) {
    super(rrh.parCtx, PcrltIdentity.createAbbrIdByNameAndContact(rrh.getName(), rrh.getContact()));
    this.setName(rrh.getName());
    if (rrh.getContact() != null) {
      assert !rrh.getContact().isEmpty();
      this.getContacts().add(rrh.getContact());
    }
    this.isImplicitByRightsH = true;
    this.isImplicitByAuthor = false;
    this.isImplicitByAbbrId = false;
    this.reuseRightsHs.add(rrh);
  }

  public PcrltIdentity(final PcrltAuthor pa) {
    super(pa.parCtx, null);// abbrId is handled below
    this.setName(pa.getName());
    this.isImplicitByRightsH = false;
    this.isImplicitByAbbrId = false;
    if (pa.getContact() != null) {
      assert !pa.getContact().isEmpty();
      this.getContacts().add(pa.getContact());
    }
    if (pa.getAbbrId() != null) {
      this.setAbbrId(pa.getAbbrId());
      this.isImplicitByAuthor = false;
    } else {
      this.isImplicitByAuthor = true;
      this.setAbbrId(PcrltIdentity.createAbbrIdByNameAndContact(pa.getName(), pa.getContact()));
    }
    this.authorAnnotations.add(pa);
  }

  static String createAbbrIdByNameAndContact(final String name, final String contact) {
    String abbrId;
    if ((name != null && !name.strip().isEmpty()) || (contact != null && !contact.strip().isEmpty())) {
      abbrId = (name != null) ? name.strip() : "";
      if (contact != null) {
        abbrId += contact.strip();
      }
    } else {
      throw new IllegalArgumentException("name and contact is null");// seems to be unreachable
    }
    return abbrId;
  }

  public void addRepo(final PcrltIdentity.PcrltIdentityRepo repo) {
    this.repos.add(repo);
  }

  public void addResolveDescription(final PcrltIdentity.PcrltIdentityResolveDescription resolveDescr) {
    this.resolveDescrs.add(resolveDescr);
  }

  public void addPDataPolicy(final PcrltIdentity.PcrltPDataPolicy pDataPolicy) {
    if (this.pDataPolicy != null) {
      final String issueText = "" + pDataPolicy.parCtx.keyword + " was already specified for " + this.getAbbrId()
          + " old:" + this.pDataPolicy + ", blocked:" + pDataPolicy;
      this.log.addIssue(new Issue(pDataPolicy.parCtx.file, issueText, Issue.Severity.WARNING, pDataPolicy.parCtx.lino));
    } else {
      this.pDataPolicy = pDataPolicy;
    }
  }

  public void addRightsHARMGPolicy(final PcrltIdentity.PcrltRightsHARMGPolicy rightsHARMGPolicy) {
    if (this.rightsHARMGPolicy != null) {
      final String issueText = "" + rightsHARMGPolicy.parCtx.keyword + " was already specified for " + this.getAbbrId()
          + " old:" + this.rightsHARMGPolicy + ", blocked:" + rightsHARMGPolicy;
      this.log.addIssue(
          new Issue(rightsHARMGPolicy.parCtx.file, issueText, Issue.Severity.WARNING, rightsHARMGPolicy.parCtx.lino));
    } else {
      this.rightsHARMGPolicy = rightsHARMGPolicy;
    }
  }

  public void addMappedGitIdentity(final PcrltIdentity pi) {
    this.mappedGitIdentity = pi;
  }

  /**
   * isImplicitByAbbrId and isImplicitByRightsH are ignored
   */
  @Override
  public boolean equalsIdentity(final Object o) {
    if (o == this) {
      return true;
    }
    /*
     * "null instanceof [type]" also returns false
     */
    if (!(o instanceof PcrltIdentity)) {
      System.out.println("this is unexpected usage");
      return false;
    }
    final PcrltIdentity pi = (PcrltIdentity) o;
    return H.equals(pi.getAbbrId(), this.getAbbrId()) && H.equals(pi.getName(), this.getName())
        && H.equals(pi.getContacts(), this.getContacts()) && H.equals(pi.abbrs, this.abbrs);
  }

  public void ensureValidity() {
    this.setAbbrId(this.getAbbrId().strip());
    this.setName(H.strip(this.getName()));
    this.setContacts(this.getContacts().stream().map(s -> s.strip()).collect(Collectors.toList()));
    this.setAbbrs(this.abbrs.stream().map(s -> s.strip()).collect(Collectors.toList()));
    if ((this.isImplicitByRightsH && this.reuseRightsHs.size() == 0)
        || (this.isImplicitByAuthor && this.authorAnnotations.size() == 0)
        || (!this.isAbbrIdFinal() && this.getName() == null && this.getContacts().isEmpty())) {
      throw new IllegalStateException("somethin wrong");
    }
  }

  /**
   * some examples: p1: spdxcopyright: John Doe p2: specify-identity: John Doe
   * <c@d> should be interpreted as refering to same identity p1: spdxcopyright:
   * John Doe <a@b> p2: specify-identity: John Doe <c@d> should be interpreted as
   * refering to different identities p1: spdxcopyright: <c@d> p2:
   * specify-identity: John Doe <c@d> should be interpreted as refering to same
   * identity p1: spdxcopyright: <c@d> p2: spdxcopyright: John Doe <c@d> should be
   * interpreted as refering to different identities (note that contact does not
   * have to be email address - could be website e.g.) p1: spdxcopyright: John Doe
   * p2: spdxcopyright: John Doe <c@d> should be interpreted as refering to
   * different identities (note that contact does not have to be email address -
   * could be website e.g.) this means a specify-identity based pi is allowed to
   * have *more* contacts or a name but not less than an implicit pi. it is
   * possible that two pi will be assumed to be identical at a later stage if some
   * more attributes were collected. (i.e. IdentityController.merge(..) has to be
   * called multiple times until the size stops to decrease)
   * 
   * @param other
   * @return
   */
  public boolean isReferringToSamePcrltIdentity(final PcrltIdentity other) {
    // assert name,contacts,abbrs, abbrId is either null or not empty strings.
    ensureValidity();
    other.ensureValidity();
    boolean res = false;
    PcrltIdentity stable;
    PcrltIdentity pi2;
    if (this.isAbbrIdFinal() || !other.isAbbrIdFinal()) {
      stable = this;
      pi2 = other;
    } else {
      stable = other;
      pi2 = this;
    }
    assert stable.isAbbrIdFinal() || (!stable.isAbbrIdFinal() && !pi2.isAbbrIdFinal());
    if (H.equals(stable.getAbbrId(), pi2.getAbbrId())) {
      res = true;
    } else if (!stable.isAbbrIdFinal() && !pi2.isAbbrIdFinal()) {
      if (H.equals(stable.getName(), pi2.getName()) && H.equals(stable.getContacts(), pi2.getContacts())
          && H.equals(stable.abbrs, pi2.abbrs)) {
        res = true;// seems to be unreachable
      }
    } else {
      assert stable.isAbbrIdFinal();
      if (!pi2.isAbbrIdFinal()) {
        if (pi2.getName() != null) {
          if (H.equals(stable.getName(), pi2.getName())
              && (pi2.getContacts().isEmpty() || H.intersection(stable.getContacts(), pi2.getContacts()).size() > 0)) {
            res = true;
          }
        } else {
          if (H.intersection(stable.getContacts(), pi2.getContacts()).size() > 0) {
            res = true;
          }
        }
      }
    }
    return res;
  }

  public void integrate(final PcrltIdentity pid) {
    if (!this.isReferringToSamePcrltIdentity(pid)) {
      throw new IllegalArgumentException("wrong use - identities are different");
    }
    if (!this.isAbbrIdFinal() && pid.isAbbrIdFinal()) {
      throw new IllegalArgumentException("this case is not implemented");
    }
    if (this.getName() == null) {
      this.setName(pid.getName());
    } else if (pid.getName() == null) {
      // nothing to do
    } else if (!H.equals(this.getName(), pid.getName())) {
      final String issueText = "name differs:this.name=" + this.getName() + ",pid.name=" + pid.getName()
          + "this.parCtx=" + this.parCtx + "pid.parCtx=" + pid.parCtx;
      final Issue iss = new Issue(pid.parCtx.file, issueText, Issue.Severity.ERROR, pid.parCtx.lino);
      System.err.println("" + iss);
      this.log.addIssue(iss);
    }
    // TODO maybe warn if contacts differ
    this.getContacts().addAll(pid.getContacts());
    this.setContacts(H.distinct(this.getContacts()));
    this.abbrs.addAll(pid.abbrs);
    this.setAbbrs(H.distinct(this.abbrs));
    this.reuseRightsHs.addAll(pid.reuseRightsHs);
    this.setReuseRightsHs(H.distinct(this.reuseRightsHs));
    this.authorAnnotations.addAll(pid.authorAnnotations);
    this.setAuthorAnnotations(H.distinct(this.authorAnnotations));
    // this .isImplicit.. flags do not need to be changed - at least this seems to
    // be the case.
  }

  public boolean isAbbrIdFinal() {
    return !this.isImplicitByRightsH && !this.isImplicitByAuthor;
  }

  public static class PcrltIdentityParams extends IdAnnotationParams {
    public String name;
    public String contact;
    public String abbr;
    public boolean isImplicitByAbbrId;
  }

  public static class PcrltAuthor extends IdAnnotation implements EqualsIdentity {
    @Expose
    private final String text;
    @Expose
    private final String year;
    @Expose
    private final String name;
    @Expose
    private final String contact;

    public String getText() {
      return this.text;
    }

    public String getYear() {
      return this.year;
    }

    public String getName() {
      return this.name;
    }

    public String getContact() {
      return this.contact;
    }

    public PcrltAuthor(final ParCtx parCtx, final PcrltAuthorParams authorParms) {
      super(parCtx, authorParms.abbrId);
      // this.abbrId//null is possible (if author comes from authors keyword)
      this.text = H.strip(authorParms.text);
      this.year = H.strip(authorParms.year);
      this.name = H.strip(authorParms.name);
      this.contact = H.strip(authorParms.contact);
    }

    @Override
    public boolean equalsIdentity(final Object o) {
      if (o == this) {
        return true;
      }
      /*
       * Check if o is an instance of Complex or not "null instanceof [type]" also
       * returns false
       */
      if (!(o instanceof PcrltAuthor)) {
        System.out.println("this is unexpected usage");
        return false;
      }
      final PcrltAuthor a = (PcrltAuthor) o;
      return H.equals(a.name, this.name) && H.equals(a.contact, this.contact);
    }

    public static class PcrltAuthorParams extends IdAnnotationParams {
      public String text;
      public String year;
      public String name;
      public String contact;
    }
  }

  public static class PcrltIdentityRepo extends IdAnnotation {
    @Expose
    private final String text;// usually url
    @Expose
    private final String repoUrl;
    @Expose
    private final String filePath;

    public String getText() {
      return this.text;
    }

    public String getRepoUrl() {
      return this.repoUrl;
    }

    public String getFilePath() {
      return this.filePath;
    }

    public PcrltIdentityRepo(final ParCtx parCtx, final PcrltIdentityRepoParams repoParms) {
      super(parCtx, repoParms.abbrId);
      this.text = H.strip(repoParms.text);
      this.repoUrl = H.strip(repoParms.repoUrl);
      this.filePath = H.strip(repoParms.filePath);
    }

    public static class PcrltIdentityRepoParams extends IdAnnotationParams {
      public String text;
      public String repoUrl;
      public String filePath;
    }
  }

  public static class PcrltIdentityResolveDescription extends IdAnnotation {
    private final String text;// some description how the identity could be resolved

    public String getText() {
      return this.text;
    }

    public PcrltIdentityResolveDescription(final ParCtx parCtx, final String abbrId, final String text) {
      super(parCtx, abbrId);
      this.text = H.strip(text);
    }
  }

  public static class PcrltPDataPolicy extends IdAnnotation {
    @Expose
    private final boolean isAbbreviationAllowed;
    @Expose
    private final boolean isNameAllowed;
    @Expose
    private final boolean isEmailAllowed;// TODO maybe rename to isContactAllowed because contact-channel can also be
    // website etc.

    public PcrltPDataPolicy(final ParCtx parCtx, final PcrltPDataPolicyParams pDataPolicyParms) {
      super(parCtx, pDataPolicyParms.abbrId);
      this.isAbbreviationAllowed = pDataPolicyParms.isAbbreviationAllowed;
      this.isNameAllowed = pDataPolicyParms.isNameAllowed;
      this.isEmailAllowed = pDataPolicyParms.isEmailAllowed;
    }

    @Override
    public String toString() {
      return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
    }

    public static class PcrltPDataPolicyParams extends IdAnnotationParams {
      public boolean isAbbreviationAllowed;
      public boolean isNameAllowed;
      public boolean isEmailAllowed;
    }
  }

  public static class PcrltRightsHARMGPolicy extends IdAnnotation {
    @Expose
    private final boolean isNoPermissions;
    @Expose
    private final boolean isAddAllowed;
    @Expose
    private final boolean isRemoveAllowed;
    @Expose
    private final boolean isModifyAllowed;
    @Expose
    private final boolean isGroupAllowed;
    @Expose
    private final boolean isExplicitPDataCondition;

    public PcrltRightsHARMGPolicy(final ParCtx parCtx, final PcrltRightsHARMGPolicyParams rightsHARMGPolicyParams) {
      super(parCtx, rightsHARMGPolicyParams.abbrId);
      this.isNoPermissions = rightsHARMGPolicyParams.isNoPermissions;
      this.isAddAllowed = rightsHARMGPolicyParams.isAddAllowed;
      this.isRemoveAllowed = rightsHARMGPolicyParams.isRemoveAllowed;
      this.isModifyAllowed = rightsHARMGPolicyParams.isModifyAllowed;
      this.isGroupAllowed = rightsHARMGPolicyParams.isGroupAllowed;
      this.isExplicitPDataCondition = rightsHARMGPolicyParams.isExplicitPDataCondition;
    }

    public boolean isValid() {
      if (this.isNoPermissions) {
        return !this.isAddAllowed && !this.isRemoveAllowed && !this.isModifyAllowed && !this.isGroupAllowed;
      } else {
        return true;
      }
    }

    @Override
    public String toString() {
      return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
    }

    public static class PcrltRightsHARMGPolicyParams extends IdAnnotationParams {
      public boolean isNoPermissions = true;
      public boolean isAddAllowed;
      public boolean isRemoveAllowed;
      public boolean isModifyAllowed;
      public boolean isGroupAllowed;
      public boolean isExplicitPDataCondition;
    }
  }
}
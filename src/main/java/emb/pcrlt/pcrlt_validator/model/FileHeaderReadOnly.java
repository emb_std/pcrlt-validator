//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/FileHeaderReadOnly.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/FileHeaderReadOnly.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding FileHeaderReadOnly.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import java.util.List;
import java.util.Map;

import emb.pcrlt.pcrlt_validator.model.FileHeader.FileHeaderType;

public interface FileHeaderReadOnly {
  public List<ReuseRightsH> getReuseRightsHs();

  public Map<String, ReuseLicenseExpr> getReuseLicenseExprs();

  public Map<String, PcrltLicenseExpr> getPcrltLicenseExprs();

  public List<PcrltSpec> getPcrltSpecs();

  public List<PcrltSrc> getPcrltSrcs();

  public Map<String, PcrltIdentityGroup> getPcrltIdentityGroups();

  public List<Integer> getScoreList();

  public Integer getBadgeLevel();

  public Integer getScore();

  public String getTargetFile();

  public FileHeaderType getType();

  public List<ExternalAnnotations> getExtAnnotations();

  public Dep5FileHeaderAttributes getDep5Attributes();

}
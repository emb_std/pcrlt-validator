//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/FileHeader.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/FileHeader.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding FileHeader.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.IdentityController;
import emb.pcrlt.pcrlt_validator.IssueLogger;
import emb.pcrlt.pcrlt_validator.Keyword;
import emb.pcrlt.pcrlt_validator.Main;
import emb.pcrlt.pcrlt_validator.Main.MFHPolicy;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltRightsHARMGPolicy;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltAuthor2CommitMapping;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltAuthorBackground;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltCopyrightBackground;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup.PcrltRightsH2CommitMapping;
import emb.pcrlt.pcrlt_validator.model.PcrltLicenseExpr.PcrltLicense2CommitMapping;
import emb.pcrlt.pcrlt_validator.model.PcrltLicenseExpr.PcrltLicenseBackground;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

/**
 * FileHeader is database for all annotations referring to targetFile. Further
 * more it is controller for those annotations that are not coped with by
 * IdentityController.
 * 
 * @author user
 *
 */
public class FileHeader implements FileHeaderReadOnly {
  @Expose
  private final String targetFile;

  @ExposeExtended
  @Expose
  private final List<ReuseRightsH> reuseRightsHs = new ArrayList<>();

  @ExposeExtended
  @Expose
  private final Map<String, ReuseLicenseExpr> reuseLicenseExprs = new LinkedHashMap<>();

  @Expose
  private final Map<String, PcrltLicenseExpr> pcrltLicenseExprs = new LinkedHashMap<>();

  @Expose
  private List<PcrltSpec> pcrltSpecs = new ArrayList<>();

  @Expose
  private final List<PcrltSrc> pcrltSrcs = new ArrayList<>();
  @Expose
  private final List<ExternalAnnotations> extAnnotations = new ArrayList<>();
  @Expose
  private final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();

  @Expose
  private final FileHeaderType type;
  @Expose
  private Dep5FileHeaderAttributes dep5Attributes;
  private Integer score;// Feature-Score
  private List<Integer> scoreList;// Feature-Score-List
  @Expose
  private Integer badgeLevel = 0;// see badge/level in PCRLTv0.4
  private final IssueLogger log;// to submit issues
  // private IdentityController idController;
  private Integer lino;

  @Override
  public FileHeaderType getType() {
    return this.type;
  }

  @Override
  public String getTargetFile() {
    return this.targetFile;
  }

  @Override
  public List<ReuseRightsH> getReuseRightsHs() {
    return this.reuseRightsHs;
  }

  @Override
  public Map<String, ReuseLicenseExpr> getReuseLicenseExprs() {
    return this.reuseLicenseExprs;
  }

  @Override
  public Map<String, PcrltLicenseExpr> getPcrltLicenseExprs() {
    return this.pcrltLicenseExprs;
  }

  @Override
  public List<PcrltSpec> getPcrltSpecs() {
    return this.pcrltSpecs;
  }

  @Override
  public List<PcrltSrc> getPcrltSrcs() {
    return this.pcrltSrcs;
  }

  @Override
  public Map<String, PcrltIdentityGroup> getPcrltIdentityGroups() {
    return this.pcrltIdentityGroups;
  }

  @Override
  public List<Integer> getScoreList() {
    return this.scoreList;
  }

  @Override
  public Integer getBadgeLevel() {
    return this.badgeLevel;
  }

  @Override
  public Integer getScore() {
    return this.score;
  }

  public void setScore(final Integer score) {
    this.score = score;
  }

  public void setScoreList(final List<Integer> scoreList) {
    this.scoreList = scoreList;
  }

  public void setBadgeLevel(final Integer badgeLevel) {
    this.badgeLevel = badgeLevel;
  }

  public void setPcrltSpecs(final List<PcrltSpec> pcrltSpecs) {
    this.pcrltSpecs = pcrltSpecs;
  }

  public Integer getLino() {
    return this.lino;
  }

  public Dep5FileHeaderAttributes getDep5Attributes() {
    return this.dep5Attributes;
  }

  public void setDep5Attributes(final Dep5FileHeaderAttributes dep5Attributes) {
    this.dep5Attributes = dep5Attributes;
  }

  @Override
  public List<ExternalAnnotations> getExtAnnotations() {
    return this.extAnnotations;
  }

  public FileHeader(final String targetFile, final FileHeaderType type) {
    this.log = Main.getIssueLogger();
    this.targetFile = targetFile;
    this.type = type;
    this.lino = null;
  }

  public FileHeader(final String targetFile, final FileHeaderType type, final Integer lino) {
    this(targetFile, type);
    this.lino = lino;
  }

  public void addReuseFileCopyrightText(final ReuseRightsH reuseRightsH) {
    this.reuseRightsHs.add(reuseRightsH);
    IdentityController.registerNewPcrltIdentity(this.pcrltIdentityGroups, new PcrltIdentity(reuseRightsH));
  }

  public boolean isEmpty() {
    return this.reuseRightsHs.isEmpty() && this.reuseLicenseExprs.isEmpty() && getPcrltSpecs().isEmpty()
        && this.pcrltSrcs.isEmpty() && this.pcrltIdentityGroups.isEmpty();
  }

  public void addReuseLicenseIdentifier(final ReuseLicenseExpr reuseLicenseExpr) {
    final String licenseExpr = reuseLicenseExpr.getLicenseExpr();
    if (!reuseLicenseExpr.getIsDep5()) {
      final PcrltLicenseExpr pcrltLicenseExpr = new PcrltLicenseExpr(reuseLicenseExpr);
      reuseLicenseExpr.setPcrltLicenseExpr(pcrltLicenseExpr);
      if (!this.reuseLicenseExprs.containsKey(licenseExpr)) {
        this.reuseLicenseExprs.put(licenseExpr, reuseLicenseExpr);
      } else {
        this.reuseLicenseExprs.get(licenseExpr).setNext(reuseLicenseExpr);
      }
      if (!this.pcrltLicenseExprs.containsKey(licenseExpr)) {
        this.pcrltLicenseExprs.put(licenseExpr, pcrltLicenseExpr);
      } else {
        this.pcrltLicenseExprs.get(licenseExpr).setNext(pcrltLicenseExpr);
      }
    } else {
      final PcrltLicenseExpr pcrltLicenseExpr = new PcrltLicenseExpr(reuseLicenseExpr);
      reuseLicenseExpr.setPcrltLicenseExpr(pcrltLicenseExpr);
      this.reuseLicenseExprs.put(licenseExpr, reuseLicenseExpr);
      this.pcrltLicenseExprs.put(licenseExpr, pcrltLicenseExpr);
    }
  }

  public void addMappedGitIdentity(final ParCtx parCtx, final String abbrId, final String abbrIdGit) {
    IdentityController.registerNewImplicitById(parCtx, this.pcrltIdentityGroups, abbrId, true);
    IdentityController.registerNewImplicitById(parCtx, this.pcrltIdentityGroups, abbrIdGit, true);
    this.pcrltIdentityGroups.get(abbrId).getPcrltIdentities().get(abbrId)
        .addMappedGitIdentity(getPcrltIdentity(abbrIdGit));
  }

  public void addPcrltIdentityRepo(final PcrltIdentity.PcrltIdentityRepo repo) {
    IdentityController.registerNewImplicitById(repo.parCtx, this.pcrltIdentityGroups, repo.getAbbrId(), true);
    this.pcrltIdentityGroups.get(repo.getAbbrId()).getPcrltIdentities().get(repo.getAbbrId()).addRepo(repo);
  }

  public void addPcrltIdentityResolveDescription(final PcrltIdentity.PcrltIdentityResolveDescription descr) {
    IdentityController.registerNewImplicitById(descr.parCtx, this.pcrltIdentityGroups, descr.getAbbrId(), true);
    this.pcrltIdentityGroups.get(descr.getAbbrId()).getPcrltIdentities().get(descr.getAbbrId())
        .addResolveDescription(descr);
  }

  public void addPcrltPDataPolicy(final PcrltIdentity.PcrltPDataPolicy pdp) {
    IdentityController.registerNewImplicitById(pdp.parCtx, this.pcrltIdentityGroups, pdp.getAbbrId(), true);
    this.pcrltIdentityGroups.get(pdp.getAbbrId()).getPcrltIdentities().get(pdp.getAbbrId()).addPDataPolicy(pdp);
  }

  public void addPcrltRightsHARMGPolicy(final PcrltRightsHARMGPolicy prHARMGPolicy) {
    IdentityController.registerNewImplicitById(prHARMGPolicy.parCtx, this.pcrltIdentityGroups,
        prHARMGPolicy.getAbbrId(), true);
    this.pcrltIdentityGroups.get(prHARMGPolicy.getAbbrId()).getPcrltIdentities().get(prHARMGPolicy.getAbbrId())
        .addRightsHARMGPolicy(prHARMGPolicy);
  }

  public void addPcrltRightsH2CommitMapping(final String abbrIdExpr, final List<String> abbrIds,
      final PcrltRightsH2CommitMapping r2c) {
    IdentityController.registerNew(r2c.parCtx, this.pcrltIdentityGroups, abbrIdExpr, abbrIds, true);
    this.pcrltIdentityGroups.get(abbrIdExpr).addRightsH2CommitMapping(r2c);

  }

  public void addPcrltAuthor2CommitMapping(final String abbrIdExpr, final List<String> abbrIds,
      final PcrltAuthor2CommitMapping r2c) {
    IdentityController.registerNew(r2c.parCtx, this.pcrltIdentityGroups, abbrIdExpr, abbrIds, true);
    this.pcrltIdentityGroups.get(abbrIdExpr).addAuthor2CommitMapping(r2c);
  }

  public void addPcrltCopyrightBackground(final String abbrIdExpr, final List<String> abbrIds,
      final PcrltCopyrightBackground cb) {
    IdentityController.registerNew(cb.parCtx, this.pcrltIdentityGroups, abbrIdExpr, abbrIds, true);
    this.pcrltIdentityGroups.get(abbrIdExpr).addCopyrightBackground(this.targetFile, cb);

  }

  public void addPcrltAuthorBackground(final String abbrIdExpr, final List<String> abbrIds,
      final PcrltAuthorBackground ab) {
    IdentityController.registerNew(ab.parCtx, this.pcrltIdentityGroups, abbrIdExpr, abbrIds, true);
    this.pcrltIdentityGroups.get(abbrIdExpr).addAuthorBackground(this.targetFile, ab);

  }

  public void addPcrltLicense2CommitMapping(final String licenseExpr, final PcrltLicense2CommitMapping l2c) {
    this.pcrltLicenseExprs.putIfAbsent(licenseExpr, new PcrltLicenseExpr(l2c.parCtx, licenseExpr));// this should be
                                                                                                   // possible
    // as well as copyright
    // holder. in case the
    // content is in git
    // history only.
    this.pcrltLicenseExprs.get(licenseExpr).addLicense2CommitMapping(this.targetFile, l2c, l2c.parCtx.lino);

  }

  public void addPcrltLicenseBackground(final String licenseExpr, final PcrltLicenseBackground lb) {
    this.pcrltLicenseExprs.putIfAbsent(licenseExpr, new PcrltLicenseExpr(lb.parCtx, licenseExpr));
    this.pcrltLicenseExprs.get(licenseExpr).addLicenseBackground(this.targetFile, lb);

  }

  public void addPcrltSpec(final PcrltSpec pcrltSpec, final Integer lino) {
    if (this.type == FileHeaderType.dep5) {
      final String issueText = "IllegalArgumentException:spec should not be placed in every dep5 header. just in the .reuse/dep5 std header (top)";
      final Issue iss = new Issue(this.targetFile, issueText, Issue.Severity.ERROR, null);
      System.out.println("" + iss);
      this.log.addIssue(iss);
    }
    getPcrltSpecs().add(pcrltSpec);
  }

  public void addPcrltSrc(final PcrltSrc pcrltSrc) {
    this.pcrltSrcs.add(pcrltSrc);
  }

  /**
   * filters reuseRightsHs in a special way into a new list.
   * The method ReuseRightsH::equalsIdentity is used to determine if the rightsH refer to the same identity.
   * The filtered list will contain only and all different rightsHs.
   * The filtered list will prefer "SPDX-FileCopyrightText:" based rightsHs over "Copyright:" based ones, if they refer to the same identity.
   * @param reuseRightsHs
   * @return
   */
  public static List<ReuseRightsH> filterReuseRigthsHForSPDX(final List<ReuseRightsH> reuseRightsHs) {
    final List<ReuseRightsH> filtered = new ArrayList<>();
    for (final ReuseRightsH r : reuseRightsHs) {
      if (!H.containsUsingCustomEquals(filtered, r,(a,b)->a.equalsIdentity(b))) {
        filtered.add(r);
      } else {
        if (!r.getIsDep5()) {//"Copyright:" expressions will be skipped if an identical "SPDX-FileCopyrightText:" expression is already found.
          //otherwise if "SPDX-FileCopyrightText:" expression is found it will be used to replace the old.
          final ReuseRightsH r_old = H.getUsingCustomEquals(filtered, r,(a,b)->a.equalsIdentity(b));
          filtered.remove(filtered.indexOf(r_old));
          filtered.add(r);
          //what if r is found multiple times? (in case of "SPDX-FileCopyrightText:" the last one will dominate.
          // in case of "Copyright:" the first one will dominate. but multiple "Copyright:" expression for one file are not allowed (but might be still possible)).
          //what if r is found multiple times with different yearExpr? this will lead to ignoring the yearExpr but it will cause an warning issue in the ValidationAndScoringHelper.checkForIssuesInRightsHs
        }
      }
    }
    return filtered;
  }

  public PcrltIdentity getPcrltIdentity(final String abbrId) {
    return this.pcrltIdentityGroups.get(abbrId).getPcrltIdentities().get(abbrId);
  }

  public static enum FileHeaderType {
    std, dotLicense, dep5
  }

  public void addExternalAnnotations(final ExternalAnnotations extAnn) {
    this.extAnnotations.add(extAnn);
  }

  public void addPcrltIdentity(final PcrltIdentity pi) {
    IdentityController.registerNewPcrltIdentity(this.pcrltIdentityGroups, pi);
  }

  public void addPcrltAuthor(final PcrltIdentity pi) {
    IdentityController.registerNewPcrltIdentity(this.pcrltIdentityGroups, pi);
  }

  @Override
  public String toString() {
    return new GsonBuilder().setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation().create()
        .toJson(this);
  }

  public static void mergePcrltIdentitiesInFileHeaders(final Map<String, FileHeader> fileHeaders) {
    for (final FileHeader fh : fileHeaders.values().stream().filter(o -> o != null).collect(Collectors.toList())) {
      IdentityController.mergePcrltIdentities(fh.pcrltIdentityGroups, fh.targetFile, null);
    }
  }

  public static boolean newHasPrecedenceOverOld(final FileHeader fh_new, final FileHeader fh_old,
      final MFHPolicy mfhPolicy, final Integer lino) {
    return FileHeader.newHasPrecedenceOverOld(fh_new, fh_old, mfhPolicy, lino, false);
  }

  public static boolean newHasPrecedenceOverOld(final FileHeader fh_new, final FileHeader fh_old,
      final MFHPolicy mfhPolicy, final Integer lino, final boolean ignoreIssues) {
    //
    if (mfhPolicy == MFHPolicy.Dep5First) {
      if (fh_new.getType() == FileHeaderType.dep5 && fh_old.getType() != FileHeaderType.dep5) {
        return true;
      } else if (fh_new.getType() != FileHeaderType.dep5 && fh_old.getType() == FileHeaderType.dep5) {
        return false;
      } else {
        if (!ignoreIssues) {
          Main.getIssueLogger()
              .addIssue(new Issue(fh_new.getTargetFile(),
                  "multiple file headers found - with undecidable precedence:old=" + fh_old.getTargetFile() + "("
                      + fh_old.getType() + ");new=" + fh_new.getTargetFile() + "(" + fh_new.getType() + "); policy="
                      + mfhPolicy,
                  Issue.Severity.ERROR, lino));
          // throw new IllegalArgumentException("multiple file headers found - with
          // undecidable
          // precedence:old="+fh_old.getTargetFile()+"("+fh_old.getType()+");new="+fh_new.getTargetFile()+"("+fh_new.getType()+");
          // policy="+mfhPolicy);
        }
        return false;
      }
    } else if (mfhPolicy == MFHPolicy.OneHeaderPerFile) {
      if (!ignoreIssues) {
        Main.getIssueLogger()
            .addIssue(new Issue(fh_new.getTargetFile(),
                "only one file header allowed:old=" + fh_old.getTargetFile() + "(" + fh_old.getType() + ");new="
                    + fh_new.getTargetFile() + "(" + fh_new.getType() + "); policy=" + mfhPolicy,
                Issue.Severity.ERROR, lino));
        // throw new IllegalArgumentException("only one file header
        // allowed:old="+fh_old.getTargetFile()+"("+fh_old.getType()+");new="+fh_new.getTargetFile()+"("+fh_new.getType()+");
        // policy="+mfhPolicy);
      }
      return false;
    } else {
      if (!ignoreIssues) {
        Main.getIssueLogger().addIssue(new Issue(fh_new.getTargetFile(),
            "this multi file header policy is not implemented:" + mfhPolicy, Issue.Severity.ERROR, lino));
        // throw new IllegalArgumentException("this multi file header policy is not
        // implemented:"+mfhPolicy);
      }
      return false;
    }
  }

  public static Collection<String> getAllSrcTraceRepoUrls(final Map<String, FileHeaderReadOnly> fileHeaders) {// distinct
    final List<String> allRepoUrls = new ArrayList<>();
    for (final Entry<String, FileHeaderReadOnly> efh : fileHeaders.entrySet()) {
      if (efh.getValue() != null) {
        allRepoUrls.addAll(efh.getValue().getPcrltSrcs().stream().filter(s -> s.getType() == Keyword.srcTraceRepository)
            .map(s -> (s.getRepoUrl() != null) ? s.getRepoUrl() : "").collect(Collectors.toList()));
      }
    }
    return H.distinct(allRepoUrls);
  }

  public static Collection<String> getAllResolveGitIdentityRepoUrls(final Map<String, FileHeaderReadOnly> fileHeaders) {// distinct
    final List<String> allRepoUrls2 = new ArrayList<>();
    for (final Entry<String, FileHeaderReadOnly> efh : fileHeaders.entrySet()) {
      if (efh.getValue() != null) {
        allRepoUrls2
            .addAll(efh.getValue().getPcrltIdentityGroups().entrySet().stream().filter(e -> e.getValue() != null)
                .flatMap(e -> e.getValue().getPcrltIdentities().entrySet().stream().filter(e1 -> e1.getValue() != null)
                    .flatMap(e2 -> e2.getValue().getRepos().stream()
                        .map(r -> (r.getRepoUrl() != null) ? r.getRepoUrl() : r.getText())))
                .collect(Collectors.toList()));// .stream().filter(s->s.type==Keyword.srcTraceRepository).map(s->(s.repoUrl!=null)?s.repoUrl:"").collect(Collectors.toList()));
      }
    }
    return H.distinct(allRepoUrls2);
  }
}
//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltLicenseExpr.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltLicenseExpr.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding PcrltLicenseExpr.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class PcrltLicenseExpr extends LicenseAnnotation {
  @Expose
  private PcrltLicenseBackground pcrltLicenseBackground;
  @Expose
  private PcrltLicense2CommitMapping pcrltLicense2CommitMapping;
  @Expose
  private ReuseLicenseExpr reuseLicenseExpr;
  @Expose
  private PcrltLicenseExpr next;// more pcrltLicenseExprs

  public PcrltLicenseBackground getPcrltLicenseBackground() {
    return this.pcrltLicenseBackground;
  }

  public PcrltLicense2CommitMapping getPcrltLicense2CommitMapping() {
    return this.pcrltLicense2CommitMapping;
  }

  public ReuseLicenseExpr getReuseLicenseExpr() {
    return this.reuseLicenseExpr;
  }

  public PcrltLicenseExpr getNext() {
    return this.next;
  }

  void setNext(final PcrltLicenseExpr l) {
    this.next = l;
  }

  public PcrltLicenseExpr(final ParCtx parCtx, final String licenseExpr) {
    super(parCtx, licenseExpr);
  }

  public PcrltLicenseExpr(final ReuseLicenseExpr reuseLicenseExpr) {
    super(reuseLicenseExpr.parCtx, reuseLicenseExpr.getLicenseExpr());
    this.reuseLicenseExpr = reuseLicenseExpr;
  }

  public void addLicense2CommitMapping(final String file, final PcrltLicense2CommitMapping l2c, final Integer lino) {
    if (this.pcrltLicense2CommitMapping != null) {
      this.log.createAndLogIssue(Issue.Severity.ERROR, l2c.parCtx,
          "" + l2c.parCtx.keyword + " was already specified for " + this.getLicenseExpr());
    } else {
      this.pcrltLicense2CommitMapping = l2c;
    }
  }

  public void addLicenseBackground(final String file, final PcrltLicenseBackground lb) {
    if (this.pcrltLicenseBackground != null) {
      this.log.createAndLogIssue(Issue.Severity.ERROR, lb.parCtx,
          "" + lb.parCtx.keyword + " was already specified for " + this.getLicenseExpr());
    } else {
      this.pcrltLicenseBackground = lb;
    }
  }

  public static class PcrltLicense2CommitMapping extends LicenseAnnotation {
    @Expose
    String text;

    public PcrltLicense2CommitMapping(final ParCtx parCtx, final String licenseExpr, final String text) {
      super(parCtx, licenseExpr);
      this.text = text;
    }
  }

  public static class PcrltLicenseBackground extends LicenseAnnotation {
    @Expose
    String text;

    public PcrltLicenseBackground(final ParCtx parCtx, final String licenseExpr, final String text) {
      super(parCtx, licenseExpr);
      this.text = text;
    }
  }
}
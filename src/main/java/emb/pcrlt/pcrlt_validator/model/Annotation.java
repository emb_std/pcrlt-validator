//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/Annotation.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/Annotation.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Annotation.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.IssueLogger;
import emb.pcrlt.pcrlt_validator.Main;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class Annotation {
  final IssueLogger log;// to add issues
  @ExposeExtended
  @Expose
  public final ParCtx parCtx;// parserContext

  Annotation(final ParCtx parCtx) {
    if (!Main.isInit()) {
      throw new IllegalStateException("Main needs to be initialized first");
    }
    this.log = Main.getIssueLogger();
    this.parCtx = parCtx;
  }

  @Override
  public String toString() {
    return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
  }
}

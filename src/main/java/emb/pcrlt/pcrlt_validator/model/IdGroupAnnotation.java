//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/IdGroupAnnotation.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/IdGroupAnnotation.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding IdGroupAnnotation.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class IdGroupAnnotation extends Annotation {
  @Expose
  private final String abbrIdExpr;
  @Expose
  private final String text;

  public String getAbbrIdExpr() {
    return this.abbrIdExpr;
  }

  public String getText() {
    return this.text;
  }

  IdGroupAnnotation(final ParCtx parCtx, final String abbrIdExpr, final String text) {
    super(parCtx);
    this.abbrIdExpr = H.strip(abbrIdExpr);
    this.text = H.strip(text);
  }
}
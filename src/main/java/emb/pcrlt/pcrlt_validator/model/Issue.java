//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/Issue.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/Issue.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Issue.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

public class Issue {
  public Issue(final String targetFile, final String issueText, final Issue.Severity level, final Integer lino) {
    this.targetFile = targetFile;
    this.text = issueText;
    this.level = level;
    this.lino = lino;
    this.blockedBadgeLevel = BlockedBadgeLevel.NO_ASSERTION;
  }

  public Issue(final String targetFile, final String issueText, final Issue.Severity level, final Integer lino,
      final BlockedBadgeLevel blockedBadgeLevel) {
    this(targetFile, issueText, level, lino);
    assert blockedBadgeLevel!=null;
    this.blockedBadgeLevel = blockedBadgeLevel;
  }

  @Expose
  public final Severity level;
  public BlockedBadgeLevel blockedBadgeLevel;
  @Expose
  public final String targetFile;
  @Expose
  public final Integer lino;
  @Expose
  public final String text;

  /**
   * Level represents the severity of the issue considering the expected use and
   * the likelihood that the issue is unintended.
   *
   */
  public static enum Severity {
    /**
     * The software was not used in an expected way. Usual guarantees might not hold
     * anymore. This should not affect the safety/security.
     */
    ERROR,
    /**
     * There seems to be a probably unintended or erroneous use. (This assumes a
     * targeted compliance score of at least 1)
     */
    WARNING,
    /**
     * There seems to be a possibly unintended or possibly erroneous use.
     */
    INFO
  }

  @Override
  public String toString() {
    return "" + this.level + ":" + this.text + " (" + this.targetFile + ")" + "(lino:" + this.lino + ")";
  }

  public static enum BlockedBadgeLevel {
    L1, L2, L3,
    /**
     * The pcrlt level requirements are not affected by this issue.
     */
    NONE,
    /**
     * The pcrlt level requirements might or might not be affected by this issue.
     */
    NO_ASSERTION;

    public boolean isBlocking(int cs) {
      if(cs==3) {
        return this==L3||this.isBlocking(2);
      }else if(cs==2) {
        return this==L2||this.isBlocking(1);
      }else if(cs==1) {
        return this==L1;
      }else {
        throw new IllegalArgumentException("illegal argument. has to be one of {1,2,3} but is:"+cs); 
      }
    }
  }
}
//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltIdentityGroup.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltIdentityGroup.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding PcrltIdentityGroup.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.H;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class PcrltIdentityGroup extends IdGroupAnnotation {
  @Expose
  private final Map<String, PcrltIdentity> pcrltIdentities = new LinkedHashMap<>();
  @Expose
  private PcrltRightsH2CommitMapping rightsH2CommitMapping;
  @Expose
  private PcrltCopyrightBackground copyrightBackground;
  @Expose
  private PcrltAuthor2CommitMapping author2CommitMapping;
  @Expose
  private PcrltAuthorBackground authorBackground;

  public Map<String, PcrltIdentity> getPcrltIdentities() {
    return this.pcrltIdentities;
  }

  public PcrltRightsH2CommitMapping getRightsH2CommitMapping() {
    return this.rightsH2CommitMapping;
  }

  public PcrltCopyrightBackground getCopyrightBackground() {
    return this.copyrightBackground;
  }

  public PcrltAuthor2CommitMapping getAuthor2CommitMapping() {
    return this.author2CommitMapping;
  }

  public PcrltAuthorBackground getAuthorBackground() {
    return this.authorBackground;
  }

  public PcrltIdentityGroup(final ParCtx parCtx, final PcrltIdentity pi) {
    super(parCtx, H.strip(pi.getAbbrId()), H.strip(pi.parCtx.text));
    this.pcrltIdentities.put(pi.getAbbrId(), pi);
  }

  public PcrltIdentityGroup(final ParCtx parCtx, final String abbrIdExpr) {
    super(parCtx, H.strip(abbrIdExpr), H.strip(parCtx.text));
  }

  public void addRightsH2CommitMapping(final PcrltRightsH2CommitMapping r2c) {
    if (this.rightsH2CommitMapping != null) {
      this.log.createAndLogIssue(Issue.Severity.ERROR, r2c.parCtx,
          "" + r2c.parCtx.keyword + " was already specified for " + this.getAbbrIdExpr());
    } else {
      this.rightsH2CommitMapping = r2c;
    }
  }

  public void addAuthor2CommitMapping(final PcrltAuthor2CommitMapping a2c) {
    if (this.author2CommitMapping != null) {
      this.log.createAndLogIssue(Issue.Severity.ERROR, a2c.parCtx,
          "" + a2c.parCtx.keyword + " was already specified for " + this.getAbbrIdExpr());
    } else {
      this.author2CommitMapping = a2c;
    }
  }

  public void addCopyrightBackground(final String file, final PcrltCopyrightBackground cb) {
    if (this.copyrightBackground != null) {
      this.log.createAndLogIssue(Issue.Severity.ERROR, cb.parCtx,
          "" + cb.parCtx.keyword + " was already specified for " + this.getAbbrIdExpr());
    } else {
      this.copyrightBackground = cb;
    }

  }

  public void addAuthorBackground(final String file, final PcrltAuthorBackground ab) {
    if (this.authorBackground != null) {
      this.log.createAndLogIssue(Issue.Severity.ERROR, ab.parCtx,
          "" + ab.parCtx.keyword + " was already specified for " + this.getAbbrIdExpr());
    } else {
      this.authorBackground = ab;
    }
  }

  public static class PcrltRightsH2CommitMapping extends IdGroupAnnotation {
    public PcrltRightsH2CommitMapping(final ParCtx parCtx, final String abbrIdExpr, final String text) {
      super(parCtx, abbrIdExpr, text);
    }
  }

  public static class PcrltAuthor2CommitMapping extends IdGroupAnnotation {
    public PcrltAuthor2CommitMapping(final ParCtx parCtx, final String abbrIdExpr, final String text) {
      super(parCtx, abbrIdExpr, text);
    }
  }

  public static class PcrltCopyrightBackground extends IdGroupAnnotation {
    public PcrltCopyrightBackground(final ParCtx parCtx, final String abbrIdExpr, final String text) {
      super(parCtx, abbrIdExpr, text);
    }
  }

  public static class PcrltAuthorBackground extends IdGroupAnnotation {
    public PcrltAuthorBackground(final ParCtx parCtx, final String abbrIdExpr, final String text) {
      super(parCtx, abbrIdExpr, text);
    }
  }

}
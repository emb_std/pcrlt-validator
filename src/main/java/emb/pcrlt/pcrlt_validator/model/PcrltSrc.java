//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltSrc.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/PcrltSrc.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding PcrltSrc.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.Keyword;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class PcrltSrc extends Annotation {
  @Expose
  private final String text;
  @Expose
  private final String year;
  @Expose
  private final String access;// public, private, internal (PCRLTv0.3 does not yet specify the meaning)
  @Expose
  private final String url;
  @Expose
  private final String filePath;
  @Expose
  private final String repoUrl;
  @Expose
  private final Keyword type;// srcTraceRepository or srcTrace

  public String getRepoUrl() {
    return this.repoUrl;
  }

  public Keyword getType() {
    return this.type;
  }

  public PcrltSrc(final ParCtx parCtx, final PcrltSrcParams srcParms) {
    super(parCtx);
    this.text = srcParms.text;
    this.year = srcParms.year;
    this.access = srcParms.access;
    this.url = srcParms.url;
    this.repoUrl = srcParms.repoUrl;
    this.type = srcParms.type;
    this.filePath = srcParms.filePath;
  }

  public String getFilePath() {
    return this.filePath;
  }

  public static class PcrltSrcParams {
    public String text;
    public String year;
    public String access;
    public String url;
    public String repoUrl;
    public Keyword type;
    public String filePath;
  }
}
//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/ReuseLicenseExpr.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/model/ReuseLicenseExpr.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding ReuseLicenseExpr.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import com.google.gson.annotations.Expose;

import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;

public class ReuseLicenseExpr extends LicenseAnnotation {
  private PcrltLicenseExpr pcrltLicenseExpr;
  /**
   * true, if the annotation is in dep5 file *and* has "License:" prefix,
   * otherwise false
   */
  @Expose
  private final boolean isDep5;
  @Expose
  private ReuseLicenseExpr next;// more reuseLicenseExprs

  public PcrltLicenseExpr getPcrltLicenseExpr() {
    return this.pcrltLicenseExpr;
  }

  void setPcrltLicenseExpr(final PcrltLicenseExpr pcrltLicenseExpr) {
    this.pcrltLicenseExpr = pcrltLicenseExpr;
  }

  public ReuseLicenseExpr getNext() {
    return this.next;
  }

  void setNext(final ReuseLicenseExpr next) {
    this.next = next;
  }

  public Boolean getIsDep5() {
    return this.isDep5;
  }

  public ReuseLicenseExpr(final ParCtx parCtx, final String licenseExpr, final boolean isDep5) {
    super(parCtx, licenseExpr);
    this.isDep5 = isDep5;
  }

}
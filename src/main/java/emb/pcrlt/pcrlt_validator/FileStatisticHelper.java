//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/FileStatisticHelper.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/FileStatisticHelper.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding FileStatisticHelper.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.io.IOException;
import java.nio.charset.MalformedInputException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.jgit.dircache.DirCacheIterator;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;

import emb.helper.HelperI;
import emb.pcrlt.pcrlt_validator.model.FileStatistic;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.TextFileStatistic;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.BinaryFileStatistic;
import emb.pcrlt.pcrlt_validator.model.FileStatistic.FileType;

public class FileStatisticHelper {
  public static void createFileStatistic(final Map<String, FileStatistic> fileStatistics, final Repository repo)
      throws MissingObjectException, IncorrectObjectTypeException, CorruptObjectException, IOException {
    final String repoDir = Main.getMainLightLight().getRepositoryDir();
    final AbstractTreeIterator treeIterator = new DirCacheIterator(repo.readDirCache());
    final TreeWalk treeWalk = new TreeWalk(repo);
    treeWalk.addTree(treeIterator);
    treeWalk.setRecursive(true);
    while (treeWalk.next()) {
      final String currFileWithRelPath = treeWalk.getPathString();
      FileType fs_type = FileType.TEXT;
      String fs_fileExtension = FilenameUtils.getExtension(currFileWithRelPath);
      if (fs_fileExtension == null) {
        fs_fileExtension = "";
      }
      Path path = Paths.get(repoDir + "/" + currFileWithRelPath);
      Path path_name = path.getFileName();
      if (path_name != null && fs_fileExtension.length() == path_name.toString().length() - 1) {
        fs_fileExtension = "";// copes with the case of hidden files
        // e.g. .gitignore has not extension "gitignore" but ""
      }
      long fs_fileSize = 0;// bytes
      FileStatistic fileStatistic = null;
      try {
        fs_fileSize = Files.size(path);// bytes
        final String currFileContent = HelperI.DefaultHelper.helper
            .getFileAsString(repoDir + "/" + currFileWithRelPath);
        // final int nChars=currFileContent.length();
        final String[] currFileContentParts = currFileContent.split("\\s+");
        final String currFileContentWithoutWhiteSpace = Arrays.asList(currFileContentParts).stream()
            .collect(Collectors.joining(""));
        final int nPrintableChars = currFileContentWithoutWhiteSpace.length();
        final int fs_nLines = Arrays.asList(currFileContent.split("\n")).size();
        final double fs_n80CharLines = (double) nPrintableChars / 80;// magic number
        fileStatistic = new TextFileStatistic(currFileWithRelPath, fs_type, fs_fileExtension, fs_fileSize, fs_nLines,
            fs_n80CharLines);
        fileStatistics.put(currFileWithRelPath, fileStatistic);
      } catch (final MalformedInputException e) {
        fs_type = FileType.BINARY;
        fileStatistic = new BinaryFileStatistic(currFileWithRelPath, fs_type, fs_fileExtension, fs_fileSize);
        fileStatistics.put(currFileWithRelPath, fileStatistic);
      } catch (final NoSuchFileException e) {
        // issue was already thrown by FileHeaderParser
      }

    }
    treeWalk.close();
  }
}

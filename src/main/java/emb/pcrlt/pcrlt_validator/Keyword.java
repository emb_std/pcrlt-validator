//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/Keyword.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/Keyword.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Keyword.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public enum Keyword {// @formatter:off
  spdxFileCopyrightText("SPDX-FileCopyrightText"),
  spdxLicenseIdentifier("SPDX-License-Identifier"),
  notePcrlt("Note-PCRLT"),
  authors("authors"),
  author("author"),
  authorBackground("author-background"),
  resolveGitIdentity("resolve-git-identity"),
  and("AND"),
  or("OR"),
  resolveIdentity("resolve-identity"),
  specifyIdentity("specify-identity"),
  mapToGitIdentity("map-to-git-identity"),
  specifyIdentityName("specify-identity-name"),
  specifyIdentityAbbreviation("specify-identity-abbreviation"),
  specifyIdentityContact("specify-identity-contact"),
  personalDataPolicy("personal-data-policy"),
  nameAllowed("name-allowed"),
  abbreviationAllowed("abbreviation-allowed"),
  contactAllowed("contact-allowed"),
  srcTrace("src-trace"),
  srcTraceRepository("src-trace-repository"),
  copyrightBackground("copyright-background"),
  mapCopyrightHolderToGitCommits("map-copyright-holder-to-git-commits"),
  mapAuthorToGitCommits("map-author-to-git-commits"),
  copyrightHolderAddRemoveModifyGroupPolicy("copyright-holder-add-remove-modify-group-policy"),
  addAllowed("add-allowed"),
  removeAllowed("remove-allowed"),
  modifyAllowed("modify-allowed"),
  groupAllowed("group-allowed"),
  conditions("conditions"),
  noPermissions("no-permissions"),
  copyrightHolderInLicenseFilesAddRemoveModifyGroupPolicy("copyright-holder-in-license-files-add-remove-modify-group-policy"),
  mapLicenseToGitCommits("map-license-to-git-commits"),
  licenseBackground("license-background"),
  compliantWithSpec("compliant-with-spec"),
  beginFileHeaderFor("begin-file-header-for"),
  endFileHeaderFor("end-file-header-for"),
  files("Files"),
  license("License"),
  copyright("Copyright"),
  externalAnnotations("external-annotations"),
  upstreamName("Upstream-Name"),//experimental support
  upstreamContact("Upstream-Contact"),//experimental support
  source("Source");//experimental support
//@formatter:on

  private String keyword;
  private static Set<Keyword> implementedKeywords = new TreeSet<>();
  private static List<Class<? extends KeywordImplementer>> implementers = Arrays.asList(FileHeaderParser.class);
  static {
    for (final Class<? extends KeywordImplementer> implementer : Keyword.implementers) {
      try {
        Class.forName(implementer.getName());
      } catch (final ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
  }

  Keyword(final String keyword) {
    this.keyword = keyword;
  }

  public static Set<Keyword> getImplementedKeywords() {
    return Keyword.implementedKeywords;
  }

  public static Set<Keyword> getUnimplementedKeywords() {
    final Set<Keyword> diff = EnumSet.allOf(Keyword.class);
    diff.removeAll(Keyword.implementedKeywords);
    return diff;
  }

  public static void registerImplementedKeywords(final Keyword... keywords) {
    Keyword.implementedKeywords.addAll(Arrays.asList(keywords));
  }

  @Override
  public String toString() {
    return this.keyword;
  }

  public static interface KeywordImplementer {

  }
}

//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/IdentityController.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/IdentityController.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding IdentityController.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityParams;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup;

public class IdentityController {

  public static void registerNew(final ParCtx parCtx, final Map<String, PcrltIdentityGroup> pcrltIdentityGroups,
      final String abbrIdExpr, final List<String> abbrIds, final boolean isOptional) {// TODO maybe rename
    if (!pcrltIdentityGroups.containsKey(abbrIdExpr)) {
      final PcrltIdentityGroup pig = new PcrltIdentityGroup(parCtx, abbrIdExpr);
      // pig.abbrIdExpr = abbrIdExpr;

      for (final String abbrId : abbrIds) {
        IdentityController.registerNewImplicitById(parCtx, pcrltIdentityGroups, abbrId, true);
        pig.getPcrltIdentities().put(abbrId, pcrltIdentityGroups.get(abbrId).getPcrltIdentities().get(abbrId));
      }
      pcrltIdentityGroups.put(abbrIdExpr, pig);

    } else {
      if (!isOptional) {
        throw new IllegalStateException("wrong use");
      }
    }

  }

  public static void registerNewImplicitById(final ParCtx parCtx,
      final Map<String, PcrltIdentityGroup> pcrltIdentityGroups, final String abbrId, final boolean isOptional) {
    if (!pcrltIdentityGroups.containsKey(abbrId)) {
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      identityParms.abbrId = abbrId;
      identityParms.isImplicitByAbbrId = true;
      final PcrltIdentity pi = new PcrltIdentity(parCtx, identityParms);
      // pi.setIsImplicitByAbbrId(true);
      pcrltIdentityGroups.put(abbrId, new PcrltIdentityGroup(parCtx, pi));

    } else {
      if (!isOptional) {
        throw new IllegalStateException("wrong use");
      }
    }

  }

  public static void registerNewPcrltIdentity(final Map<String, PcrltIdentityGroup> pcrltIdentityGroups,
      final PcrltIdentity pidr) {
    if (!pcrltIdentityGroups.containsKey(pidr.getAbbrId())) {
      pcrltIdentityGroups.put(pidr.getAbbrId(), new PcrltIdentityGroup(pidr.parCtx, pidr));
    } else {
      final PcrltIdentityGroup oldg = pcrltIdentityGroups.get(pidr.getAbbrId());
      final PcrltIdentity old = oldg.getPcrltIdentities().get(pidr.getAbbrId());
      old.integrate(pidr);
    }

  }

  /**
   * should be called after parsing the complete fileheader.
   * 
   * @param pcrltIdentityGroups
   */
  public static void mergePcrltIdentities(final Map<String, PcrltIdentityGroup> pcrltIdentityGroups, final String file,
      final Integer lino) {
    final Map<String, PcrltIdentityGroup> filteredPcrltIdentityGroupsSingleAndAbbrIdFinal = new LinkedHashMap<>();
    final Map<String, PcrltIdentityGroup> filteredPcrltIdentityGroupsSingleAndAbbrIdNotFinal = new LinkedHashMap<>();
    pcrltIdentityGroups.entrySet().stream()
        .filter(e -> e.getValue().getPcrltIdentities().size() == 1
            && e.getValue().getPcrltIdentities().values().iterator().next().isAbbrIdFinal())
        .forEachOrdered(e2 -> filteredPcrltIdentityGroupsSingleAndAbbrIdFinal.put(e2.getKey(), e2.getValue()));
    pcrltIdentityGroups.entrySet().stream()
        .filter(e -> e.getValue().getPcrltIdentities().size() == 1
            && !e.getValue().getPcrltIdentities().values().iterator().next().isAbbrIdFinal())
        .forEachOrdered(e2 -> filteredPcrltIdentityGroupsSingleAndAbbrIdNotFinal.put(e2.getKey(), e2.getValue()));
    for (final PcrltIdentityGroup g : filteredPcrltIdentityGroupsSingleAndAbbrIdFinal.values()) {
      final PcrltIdentity orig = g.getPcrltIdentities().get(g.getAbbrIdExpr());
      for (final PcrltIdentityGroup g2 : filteredPcrltIdentityGroupsSingleAndAbbrIdNotFinal.values()) {
        final PcrltIdentity pi2 = g2.getPcrltIdentities().get(g2.getAbbrIdExpr());
        if (orig.isReferringToSamePcrltIdentity(pi2)) {
          orig.integrate(pi2);
          pcrltIdentityGroups.remove(pi2.getAbbrId());
        }
      }
    }
  }
}

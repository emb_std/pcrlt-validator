//SPDX-FileCopyrightText: 2021-2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/H.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/pcrlt/pcrlt_validator/H.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding H.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class H {
  public static <T> boolean equals(final Object o1, final Object o2) {
    if (o1 == null) {
      return o1 == o2;
    } else {
      if (o1 instanceof Collection<?> && o2 instanceof Collection<?>) {
        @SuppressWarnings("unchecked")
        final Collection<T> c1 = (Collection<T>) o1;
        @SuppressWarnings("unchecked")
        final Collection<T> c2 = (Collection<T>) o2;
        if (c1.size() == c2.size()) {
          final Set<T> intersect = new HashSet<>(c1);
          intersect.retainAll(new HashSet<>(c2));
          return intersect.size() == c1.size();
        } else {
          return false;
        }
      }
      return o1.equals(o2);
    }
  }

  /**
   * replacement to stream.distinct as the java implementation has bugs:
   * https://bugs.openjdk.java.net/browse/JDK-8223933
   * 
   * @param <T>
   * @param list
   * @return
   */
  public static <T> List<T> distinct(final Collection<T> list) {
    final List<T> distinctElements = new ArrayList<T>();
    for (final T el : list) {
      if (!distinctElements.contains(el)) {
        distinctElements.add(el);
      }
    }
    return distinctElements;
  }

  public static <T> Set<T> intersection(final Collection<T> a, final Collection<T> b) {
    final Set<T> sa = new HashSet<>(a);
    final Set<T> sb = new HashSet<>(b);
    sa.retainAll(sb);
    return sa;
  }

  /**
   * ignores year etc. only compares name, contact using equalsIdenity(Object o)
   * method.
   * 
   * @param <T>
   * @param list
   * @return
   */
  public static <T extends EqualsIdentity> Collection<T> distinctUsingEqualsIdentity(final Collection<T> list) {
    return distinctUsingCustomEquals(list,(a,b)->a.equalsIdentity(b));
  }

  public static <T> Collection<T> distinctUsingCustomEquals(final Collection<T> list, final BiPredicate<T,T> customEquals){
    final List<T> distinctElements = new ArrayList<T>();
    for (final T el : list) {
      if (!H.containsUsingCustomEquals(distinctElements, el,customEquals)) {
        distinctElements.add(el);
      }
    }
    return distinctElements;
  }
  public static <T> boolean containsUsingCustomEquals(final Collection<T> collection,
      final T o,final BiPredicate<T,T> customEquals) {
    final long count = collection.stream().filter(e -> customEquals.test(e,o)).count();
    return count > 0;
  }
  public static <T extends EqualsIdentity> T getUsingCustomEquals(final List<T> list, final T o,BiPredicate<T,T> customEquals) {
    return list.stream().filter(e -> customEquals.test(e,o)).collect(Collectors.toList()).iterator().next();
  }

  public static String strip(final String s) {
    return (s != null && !s.strip().isEmpty()) ? s.strip() : null;
  }

  public static <T> void emptyListAndAddAll(final List<T> orig, final List<T> repl) {
    final List<T> clonedList = new ArrayList<>(repl);
    orig.removeAll(orig);
    orig.addAll(clonedList);
  }

  public static <T> boolean isNotNullAndNotEmpty(final Collection<T> obj) {
    return obj != null && !obj.isEmpty();
  }

  public static <K, V> boolean isNotNullAndNotEmpty(final Map<K, V> obj) {
    return obj != null && !obj.isEmpty();
  }

  /*
   * public static boolean isNotNullAndNotEmpty(String obj) {//currently not
   * needed return obj!=null&&!obj.isEmpty(); }
   */
  public static <K, V> Map<K, V> filterMapInplaceAndConsumeTheRemoved(final Map<K, V> map, final Predicate<K> filter,
      final Consumer<K> consumer) {
    final List<K> removeQue = new ArrayList<>();
    for (final Entry<K, V> e : map.entrySet()) {
      if (filter.test(e.getKey())) {
        removeQue.add(e.getKey());
      }
    }
    for (final K k : removeQue) {
      map.remove(k);
      consumer.accept(k);
    }
    return map;
  }

  public static class StatefulProxy<T> {
    private T last;

    public T saveAndReturn(final T input) {
      this.last = input;
      return input;
    }

    public T returnLastInput() {
      return this.last;
    }
  }
}
// SPDX-FileCopyrightText: 2020-2022 embeach
//
// SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
// Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
// Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
// Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
// 
// Note-PCRLT:src-trace-repository:2022:private:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/helper/HelperI.java
// Note-PCRLT:src-trace-repository:2021:private:git+https://git.mylab.th-luebeck.de/smpc-pa/result-files-to-json-converter@master#src/main/java/HelperI.java
// 
// Note-PCRLT:emb.7:specify-identity-name:embeach
// Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/main/java/emb/helper/HelperI.java
// Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits regarding HelperI.java
//
// Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:all commits regarding HelperI.java

package emb.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
//import java.util.Arrays;
import java.util.Comparator;
//import java.util.Iterator;
//import java.util.List;
//import java.util.jar.JarEntry;
//import java.util.jar.JarFile;
import java.util.stream.Collectors;

/**
 * 
 * @author embeach 2020-2022
 */
public interface HelperI {
  public void createDirsAndWriteFile(String filenameWithPath, String content) throws IOException;

  public void writeFile(String filenameWithPath, String content) throws IOException;

  @SuppressWarnings("rawtypes")
  public URL getResource(String resourceName, Class callingClass, boolean allowNullResult) throws IOException;

  @SuppressWarnings("rawtypes")
  public InputStream getResourceAsStream(String resourceName, Class callingClass) throws IOException;

  public String getInputStreamAsString(InputStream inputStream);

  public String getFileAsString(String filenameWithPath) throws IOException;

  public void deleteDirRecursively(String pathToBeDeleted, long maxFiles) throws IOException;

  public void copyResourcesRecursively(URL originUrl, Path destination) throws IOException;

  // TODO refactor such that there are separate methods for nullable and throwable
  // versions
  // TODO maybe rename to IOHelper - maybe encapsulate readwrite? or as separate
  // modules?
  public static class DefaultHelper implements HelperI {
    // @invariant helper!=null;
    // @static_initializer
    public static final DefaultHelper helper = new DefaultHelper();

    @Override
    public String getFileAsString(final String filenameWithPath) throws IOException {
      return Files.readAllLines(Paths.get(filenameWithPath)).stream().collect(Collectors.joining("\n"));// default utf8
                                                                                                        // encoding
    }

    @Override
    public void createDirsAndWriteFile(final String filenameWithPath, final String content) throws IOException {
      _createDirsAndWriteFile(filenameWithPath, content, true);
    }

    @Override
    public void writeFile(final String filenameWithPath, final String content) throws IOException {
      _createDirsAndWriteFile(filenameWithPath, content, false);
    }

    private static void _createDirsAndWriteFile(final String filenameWithPath, final String content,
        final Boolean createDirs) throws IOException {
      final File outfile = new File(filenameWithPath);
      final Path outputParentDir = outfile.toPath().getParent();
      if (createDirs) {
        if (outputParentDir != null) {
          Files.createDirectories(outputParentDir);
        }
      }
      FileWriter fr = null;
      try {
        fr = new FileWriter(outfile);

        fr.write(content);
      } catch (final IOException e) {// TODO what can happen?
        e.printStackTrace();
      } finally {
        try {
          if (fr != null) {
            fr.close();
          }
        } catch (final IOException e) {
          e.printStackTrace();
        }
      }
    }

    @Override
    @SuppressWarnings("rawtypes")
    public URL getResource(final String resourceName, final Class callingClass, final boolean allowNullResult)
        throws IOException {
      URL url = null;
      final ClassLoader cl = callingClass.getClassLoader();
      if (cl != null) {
        url = cl.getResource(resourceName);
      }

      if (url == null) {
        url = DefaultHelper.class.getClassLoader().getResource(resourceName);
      }

      if (url == null) {
        url = Thread.currentThread().getContextClassLoader().getResource(resourceName);
      }

      if ((url == null) && (resourceName != null)
          && ((resourceName.length() == 0) || (resourceName.charAt(0) != '/'))) {
        url = getResource('/' + resourceName, callingClass, true);
      }
      if (url == null && !allowNullResult) {
        throw new IOException("resource not found:" + resourceName);
      }

      return url;
    }

    @Override
    public InputStream getResourceAsStream(final String resourceName,
        @SuppressWarnings("rawtypes") final Class callingClass) throws IOException {
      final URL url = getResource(resourceName, callingClass, false);
      return url.openStream();
    }

    @Override
    public String getInputStreamAsString(final InputStream inputStream) {
      final BufferedReader bf = new BufferedReader(new InputStreamReader(inputStream));
      return bf.lines().collect(Collectors.joining("\n"));
    }

    /**
     * based on https://www.baeldung.com/java-delete-directory
     * 
     * @param pathToBeDeleted
     * @throws IOException
     */
    @Override
    public void deleteDirRecursively(final String pathToBeDeleted, final long maxFiles) throws IOException {
      final long count = Files.walk(Paths.get(pathToBeDeleted)).count();
      if (count <= maxFiles) {
        Files.walk(Paths.get(pathToBeDeleted)).sorted(Comparator.reverseOrder()).map(Path::toFile)
            .forEach(File::delete);
      } else {
        throw new IOException(
            "The number of files in " + pathToBeDeleted + "is " + count + " which is greater than the current max of "
                + maxFiles + ". If you think this is correct, adjust the safeguard (maxFiles) and proceed.");
      }
    }

    @Override
    public void copyResourcesRecursively(final URL originUrl, final Path destination) throws IOException {
      final URLConnection urlConnection = originUrl.openConnection();
      if (urlConnection instanceof JarURLConnection) {
        // copyJarResourcesRecursively(destination, (JarURLConnection) urlConnection);
      } else {// assumes urlConnection instanceof FileURLConnection //but should not rely on
              // it
        copyFilesRecursively(Paths.get(originUrl.getPath()), destination);
        System.out.println("URLConnection[" + urlConnection.getClass() + "]");
      }
    }

    public void copyFilesRecursively(final Path sourcepath, final Path destinationepath) throws IOException {
      Files.walk(sourcepath).forEach(source -> copy(source, destinationepath.resolve(sourcepath.relativize(source))));
    }

    public void copy(final Path source, final Path dest) {
      try {
        Files.copy(source, dest, StandardCopyOption.REPLACE_EXISTING);
      } catch (final Exception e) {
        throw new RuntimeException(e.getMessage(), e);
      }
    }
  }

}

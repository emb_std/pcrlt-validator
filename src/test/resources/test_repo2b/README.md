<!--
SPDX-FileCopyrightText: 2022 John Doe

SPDX-License-Identifier: MIT

-->
# test_repo2
In this test repository all files have attached REUSE annotations (std, .license, dep5), but no PCRLT annotations. (dep5 and LICENSES are available)

Note: The dep5 file also specifies copyright and license for files in LICENSES folder (this is different from the REUSEv3.0 spec and tool)

The pcrlt-validator is called with validation-flag only.

The expected outcome can be seen in spdx-utils-output.

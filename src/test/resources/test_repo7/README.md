<!--
SPDX-FileCopyrightText: 2016 j..e.4
SPDX-FileCopyrightText: 2015-2016 Future Inc.
SPDX-FileCopyrightText: 2015,2016 B
SPDX-FileCopyrightText: C
SPDX-FileCopyrightText: 2012-2014,2016,2018-2020 D
SPDX-FileCopyrightText: 2012
SPDX-FileCopyrightText:
SPDX-FileCopyrightText:                               
SPDX-FileCopyrightText: <a>
SPDX-FileCopyrightText
test SPDX-FileCopyrightText:
Note-PCRLT:F.I.:specify-identity-name:Future Inc.
Note-PCRLT:F.I.:specify-identity-name:Future2 Inc.
Note-PCRLT:F.I.:specify-identity-contact:future-inc.com
Note-PCRLT:F.I.:specify-identity-contact:future2-inc.com
Note-PCRLT:F.I.:specify-identity-abbreviation:F. I.
Note-PCRLT:F.I.:specify-identity-abbreviation:F2. I.
SPDX-License-Identifier:MIT
SPDX-License-Identifier:

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt:a:b:::
Note-PCRLT:compliant-with-spec:ABCv0.3:https:abc.abc
Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo7.git@master#README.md
Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo7.git@master
Note-PCRLT:src-trace-repository:2022:public:abc
Note-PCRLT:src-trace:2022:public:abc
Note-PCRLT:F.I.:specify-identity-name:Future Inc.
Note-PCRLT:F.I.:specify-identity-wrong-keyword:D
Note-PCRLT:F.I.:resolve-git-identity:abc.git
Note-PCRLT:F.I.:copyright-holder-add-remove-modify-group-policy:no-permissions,add-allowed
Note-PCRLT:j..e.4:copyright-holder-add-remove-modify-group-policy:add-allowed
Note-PCRLT:j..e.4:copyright-holder-add-remove-modify-group-policy:modify-allowed
Note-PCRLT:j..e.4:personal-data-policy:name-allowed
Note-PCRLT:j..e.4:personal-data-policy:contact-allowed

Note-PCRLT:F.I.:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  between (incl.) 2015-01-01 and (incl.) 2016-01-15 regarding file f1.txt
  (first:1234567 2015-01-15,last:1234568 2016-01-15)</text>
Note-PCRLT:F.I.:map-copyright-holder-to-git-commits:test
Note-PCRLT:j..e.4:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  after (incl.) 2016-01-01 regarding file f1.txt
  (first:1234569 2016-01-01)</text>

Note-PCRLT:F.I. AND j..e.4:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  between (incl.) 2016-01-01 and (incl.) 2016-01-15 regarding file f1.txt 
  (first:1234569 2016-01-01,last:1234568 2016-01-15)</text>
Note-PCRLT:F.I. AND j..e.4:copyright-background:<text> j..e.4 was employed at Future Inc. from 2015-01-01 to 2015-12-31, 
  some commits between 2016-01-01 and 2016-01-15 contain snippets copyrighted by F.I. as well as snippets copyrighted by j..e.4
Note-PCRLT:F.I. AND j..e.4:copyright-background: test

Note-PCRLT:MIT:map-license-to-git-commits:<text>test</text>
Note-PCRLT:MIT:map-license-to-git-commits:<text>test2</text>
Note-PCRLT:MIT:license-background:test
Note-PCRLT:MIT:license-background:<text>test2</text>
Note-PCRLT:abc:author:abcd<ab>
Note-PCRLT:authors:<text>
    abd <abd@a>
</text>
Note-PCRLT:unknown-keyword:bla
Note-PCRLT:abc:author-background:<text>
    some note
</text>
Note-PCRLT:abc:author-background:some other note (not allowed)
-->
test_repo7
In this test repository all files have attached REUSE annotations (std, .license, dep5) and PCRLT annotations.
The following PCRLT keywords are used: 
* compliant-with-spec,
* src-trace-repository,
* specify-identity,
* resolve-git-identity
* map-copyright-holder-to-git-commits
* map-license-to-git-commits
* personal-data-policy, name-allowed, abbreviation-allowed
* copyright-holder-add-remove-modify-group-policy, no-permissions, conditions, personal-data-policy
* license-background
* copyright-background

Note: The keywords are the same as in test_repo5, but the AND operator is used for map-copyright-holder-to-git-commits and copyright-background on the left side (see README.md).

Note: The repository contains a binary file with .license-fileheader.

Note: The .java files show the usage with and without spaces. (like test_repo5)

Note: In README.md and .hiddenTestFile2.license single-line and multi-line values are used. (like test_repo5)

Note: The repository is meant to trigger a lot of issues for test coverage.

The pcrlt-validator is called with all-flag (this includes validation and spdx file generation). (like test_repo5)

The expected outcome can be seen in spdx-utils-output.

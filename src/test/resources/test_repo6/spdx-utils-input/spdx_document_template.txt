SPDXVersion: SPDX-<SPDXVersion>
DataLicense: CC0-1.0
SPDXID: SPDXRef-DOCUMENT
DocumentName: <PackageName>-<PackageVersion>
DocumentNamespace: http://spdx.org/spdxdocs/spdx-v<SPDXVersion>-<UUID>
Creator: Person: John Doe ()
Creator: Tool: <SpdxCreatorTool>
Created: <Created>
CreatorComment: <text>This (the spdx1.spdx) document was auto-generated in most parts using the pcrlt-validator tool from gitlab.com/emb_std/pcrlt-validator. The tool was executed with the following relevant options:<PcrltValidatorExecutionOptions></text>

PackageName: <PackageName>
SPDXID: SPDXRef-package-<PackageName>
PackageVersion: <PackageVersion>
PackageFileName: <PackageName>-<PackageVersion>.zip
PackageDownloadLocation: git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/<PackageName>.git
PackageVerificationCode: <PackageVerificationCode> (excludes: ./LICENSE.spdx)
PackageLicenseConcluded: MIT
PackageLicenseInfoFromFiles: CC0-1.0
PackageLicenseInfoFromFiles: CC-BY-ND-4.0
PackageLicenseInfoFromFiles: MIT
PackageLicenseDeclared: MIT
PackageLicenseComments: <text>The expression in PackageLicenseConcluded is the simplest way to choose from the licenses specified in the files. (Note that CC0-1.0 is not mentioned, as it does not seem to have any obligations. Also note that CC-BY-ND-4.0 is only used for license files, and therefore has no effect on the license concluded).
</text>
PackageCopyrightText: gitlab.com/emb_std/pcrlt-validator/src/test/resources/<PackageName> contributors, license-copyright-holders and possibly others

<AllFileInfos>

<!--
SPDX-FileCopyrightText: 2016 j..e.4
SPDX-FileCopyrightText: 2015-2016 Future Inc.
Note-PCRLT:F.I.:specify-identity-name:Future Inc.
SPDX-License-Identifier: MIT
Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt
Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo6.git@master#README.md
Note-PCRLT:F.I.:specify-identity-name:Future Inc.
Note-PCRLT:F.I.:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  between (incl.) 2015-01-01 and (incl.) 2016-01-15 regarding file f1.txt
  (first:1234567 2015-01-15,last:1234568 2016-01-15)</text>
Note-PCRLT:j..e.4:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  after (incl.) 2016-01-01 regarding file f1.txt
  (first:1234569 2016-01-01)</text>

Note-PCRLT:F.I. AND j..e.4:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  between (incl.) 2016-01-01 and (incl.) 2016-01-15 regarding file f1.txt 
  (first:1234569 2016-01-01,last:1234568 2016-01-15)</text>
Note-PCRLT:F.I. AND j..e.4:copyright-background:<text> j..e.4 was employed at Future Inc. from 2015-01-01 to 2015-12-31, 
  some commits between 2016-01-01 and 2016-01-15 contain snippets copyrighted by F.I. as well as snippets copyrighted by j..e.4
-->
test_repo6
In this test repository all files have attached REUSE annotations (std, .license, dep5) and PCRLT annotations.
The following PCRLT keywords are used: 
* compliant-with-spec,
* src-trace-repository,
* specify-identity,
* resolve-git-identity
* map-copyright-holder-to-git-commits
* map-license-to-git-commits
* personal-data-policy, name-allowed, abbreviation-allowed
* copyright-holder-add-remove-modify-group-policy, no-permissions, conditions, personal-data-policy
* license-background
* copyright-background

Note: The keywords are the same as in test_repo5, but the AND operator is used for map-copyright-holder-to-git-commits and copyright-background on the left side (see README.md).

Note: The repository contains a binary file with .license-fileheader.

Note: The .java files show the usage with and without spaces. (like test_repo5)

Note: In README.md and .hiddenTestFile2.license single-line and multi-line values are used. (like test_repo5)

The pcrlt-validator is called with all-flag (this includes validation and spdx file generation). (like test_repo5)

The expected outcome can be seen in spdx-utils-output.

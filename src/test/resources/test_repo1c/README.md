# test_repo1c
This test repository contains files whithout any REUSE or PCRLT annotations. Nevertheless a minimal .reuse/dep5 file and LICENSES/MIT.txt is included.
The file .hiddenTestFile is in the local git index but was deleted afterwards.

The pcrlt-validator is called with all-flag (this includes validation and spdx file generation).

The expected outcome can be seen in spdx-utils-output.

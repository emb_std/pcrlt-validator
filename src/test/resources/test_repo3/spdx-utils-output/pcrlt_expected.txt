Copyright Holders:
name                                                         <contact>                                         : numberOfFiles
John Doe                                                     <null>                                            : 9            
Creative Commons Corporation                                 <creativecommons.org>                             : 2            
Massachusetts Institute of Technology and/or Contributors    <null>                                            : 1            

License Expressions:
License Expression,                                          nFiles*,Files
MIT                                                          7       .hiddenTestFile,.hiddenTestFile2,.hiddenTestFile2.license,.reuse/dep5,README.md,src/TestClass1.java,src/TestClass2.java
CC-BY-ND-4.0                                                 3       LICENSES/CC-BY-ND-4.0.txt,LICENSES/CC0-1.0.txt,LICENSES/MIT.txt
CC0-1.0                                                      2       .gitignore,Makefile
*: nFiles=Number of Files

PCRLT Identities:
abbrId                                                       name                                                         <contact>                                         : numberOfFiles
J..n D.                                                      John Doe                                                     <>                                                : 8            
Creative Commons Corporationcreativecommons.org              Creative Commons Corporation                                 <creativecommons.org>                             : 2            
Massachusetts Institute of Technology and/or Contributors    Massachusetts Institute of Technology and/or Contributors    <>                                                : 1            
J..n D.                                                      John Doe                                                     <jd@example.com>                                  : 1            

Distinct repositories from src-trace-repository annotations:
git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo3.git@master

Distinct repositories from resolve-git-identity annotations:
git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo3.git@master

PCRLT File Scores:
file                                                                                                                    (Score1,Score2,RH,Li,Sp,Sr,IG,RH2C,IGwR,IGwA,L2C)* specVersions                  
.gitignore                                                                                                               3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile                                                                                                          3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile2                                                                                                         3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile2.license                                                                                                 3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.reuse/dep5                                                                                                              3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
Makefile                                                                                                                 3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
README.md                                                                                                                3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
src/TestClass1.java                                                                                                      3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
src/TestClass2.java                                                                                                      3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/CC-BY-ND-4.0.txt                                                                                                3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/CC0-1.0.txt                                                                                                     3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/MIT.txt                                                                                                         3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
*:Score1=Badge level,Score2=Feature Score,RH=RightsHolders,Li=Licenses,Sp=Specifications(SPDX,REUSE,PCRLT),Sr=Sources/Urls,IG=IdentityGroups,RightsHolders2CommitMappings,IGwR=IdentityGroups with repository,IGwA=IdentityGroups with authorAnnotation,L2C=License2CommitMappings

File Statistics:                             nFiles,  Size[KB],    nLines,      n80CharLines 
  All Files:                                 12       35.547       488          378.81      
  Files grouped by LICENSES folder:          
    Files in LICENSES folder:                3        25.077       284          256.49      
    Files not in LICENSES folder:            9        10.470       204          122.33      
      Files grouped by file type (TEXT,BINARY):
        Files of type TEXT:                  9        10.470       204          122.33      
      Files grouped by file extension:       
        Files with extension None:           5        6.366        124          74.50       
        Files with extension ".license":     1        0.876        14           10.38       
        Files with extension ".java":        2        1.857        34           21.63       
        Files with extension ".md":          1        1.371        32           15.83       

PCRLT File Header Statistics (where the number of records (fileHeaders) is 12):
          badgeLevel,nSpecs,nSrcs,nLics,nL2Commits,nLBackgrounds,nRHAnnotations,nRhs, nRHBackgrounds,nRH2Commits,nIdentityGroups,nIdentities,nIdentiesExplicitly,nResolveAnnotations,nMap2GitIdAnnotations,nAuthorAnnotations,nAuthors,nAuthorBackgrounds,nAuthor2Commits,nPersDataPolicies,nAddEtcPolicies,nExtAnnotations
  Sum     36         36     9     12    9          0             17             12    0              9           12              12          9                   9                   0                     0                  0        0                  0               0                 0               3              
  Max     3          3      1     1     1          0             2              1     0              1           1               1           1                   1                   0                     0                  0        0                  0               0                 0               1              
  AVR     3.000      3.000  0.750 1.000 0.750      0.000         1.417          1.000 0.000          0.750       1.000           1.000       0.750               0.750               0.000                 0.000              0.000    0.000              0.000           0.000             0.000           0.250          
  Min     3          3      0     1     0          0             1              1     0              0           1               1           0                   0                   0                     0                  0        0                  0               0                 0               0              

PCRLT Issues:
file                                                                                                                     severity      lino          blockedLevel* issueText                                                                                                                                                                                               
LICENSES/CC-BY-ND-4.0.txt                                                                                                INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
LICENSES/CC0-1.0.txt                                                                                                     INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
LICENSES/MIT.txt                                                                                                         INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
*: blockedLevel refers to the lowest PCRLT badge/level (L1=Bronze, L2=Silver, L3=Gold) whose requirements are not met.

Congratulations! The Git repository is compliant with the *Gold* badge requirements of version 0.4 of the PCRLT specification :-)
Note: The PCRLT specification has 3 badges (Bronze, Silver, Gold) to evaluate how well the repository is documented according to the PCRLT spec.
Note: PCRLT stands for Privacy-aware Content, Rights Holder and License Tracing.

Copyright Holders:
name                                                         <contact>                                         : numberOfFiles
John Doe                                                     <null>                                            : 10           
Creative Commons Corporation                                 <creativecommons.org>                             : 2            
Massachusetts Institute of Technology and/or Contributors    <null>                                            : 1            

License Expressions:
License Expression,                                          nFiles*,Files
MIT                                                          8       .hiddenTestFile,.hiddenTestFile2,.hiddenTestFile2.license,.reuse/dep5,README.md,spdx-utils-input/spdx_document_template.txt,src/TestClass1.java,src/TestClass2.java
CC-BY-ND-4.0                                                 3       LICENSES/CC-BY-ND-4.0.txt,LICENSES/CC0-1.0.txt,LICENSES/MIT.txt
CC0-1.0                                                      2       .gitignore,Makefile
*: nFiles=Number of Files

PCRLT Identities:
abbrId                                                       name                                                         <contact>                                         : numberOfFiles
J..n D.                                                      John Doe                                                     <>                                                : 9            
Creative Commons Corporationcreativecommons.org              Creative Commons Corporation                                 <creativecommons.org>                             : 2            
John Doe                                                     John Doe                                                     <>                                                : 1            
D.3                                                          Mr. Doe                                                      <>                                                : 1            
J..n D.                                                      null                                                         <>                                                : 1            
Massachusetts Institute of Technology and/or Contributors    Massachusetts Institute of Technology and/or Contributors    <>                                                : 1            

Distinct repositories from src-trace-repository annotations:
git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo10.git@master

Distinct repositories from resolve-git-identity annotations:
git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo10.git@master

PCRLT File Scores:
file                                                                                                                    (Score1,Score2,RH,Li,Sp,Sr,IG,RH2C,IGwR,IGwA,L2C)* specVersions                  
.gitignore                                                                                                               3      255    1  1  3  1  3  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile                                                                                                          3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile2.license                                                                                                 3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.reuse/dep5                                                                                                              3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
Makefile                                                                                                                 3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
README.md                                                                                                                3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
spdx-utils-input/spdx_document_template.txt                                                                              3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
src/TestClass1.java                                                                                                      3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
src/TestClass2.java                                                                                                      3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile2                                                                                                         3      253    1  1  3  1  1  1    0    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/CC-BY-ND-4.0.txt                                                                                                3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/CC0-1.0.txt                                                                                                     3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/MIT.txt                                                                                                         3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
README.md.license                                                                                                        0      0      0  0  0  0  0  0    0    0    0                                   
*:Score1=Badge level,Score2=Feature Score,RH=RightsHolders,Li=Licenses,Sp=Specifications(SPDX,REUSE,PCRLT),Sr=Sources/Urls,IG=IdentityGroups,RightsHolders2CommitMappings,IGwR=IdentityGroups with repository,IGwA=IdentityGroups with authorAnnotation,L2C=License2CommitMappings

File Statistics:                             nFiles,  Size[KB],    nLines,      n80CharLines 
  All Files:                                 14       39.498       547          423.81      
  Files grouped by LICENSES folder:          
    Files in LICENSES folder:                3        25.077       284          256.49      
    Files not in LICENSES folder:            11       14.421       263          167.33      
      Files grouped by file type (TEXT,BINARY):
        Files of type TEXT:                  11       14.421       263          167.33      
      Files grouped by file extension:       
        Files with extension None:           5        7.877        150          92.04       
        Files with extension ".txt":         1        1.553        26           17.76       
        Files with extension ".license":     2        1.059        20           12.15       
        Files with extension ".java":        2        1.861        34           21.68       
        Files with extension ".md":          1        2.071        33           23.70       

PCRLT File Header Statistics (where the number of records (fileHeaders) is 14):
          badgeLevel,nSpecs,nSrcs,nLics,nL2Commits,nLBackgrounds,nRHAnnotations,nRhs, nRHBackgrounds,nRH2Commits,nIdentityGroups,nIdentities,nIdentiesExplicitly,nResolveAnnotations,nMap2GitIdAnnotations,nAuthorAnnotations,nAuthors,nAuthorBackgrounds,nAuthor2Commits,nPersDataPolicies,nAddEtcPolicies,nExtAnnotations
  Sum     39         39     10    13    10         5             19             13    2              10          15              15          10                  9                   2                     0                  0        0                  0               1                 1               3              
  Max     3          3      1     1     1          1             2              1     1              1           3               3           1                   1                   1                     0                  0        0                  0               1                 1               1              
  AVR     2.786      2.786  0.714 0.929 0.714      0.357         1.357          0.929 0.143          0.714       1.071           1.071       0.714               0.643               0.143                 0.000              0.000    0.000              0.000           0.071             0.071           0.214          
  Min     0          0      0     0     0          0             0              0     0              0           0               0           0                   0                   0                     0                  0        0                  0               0                 0               0              

PCRLT Issues:
file                                                                                                                     severity      lino          blockedLevel* issueText                                                                                                                                                                                               
README.md                                                                                                                ERROR         null          NO_ASSERTION  multiple file headers found - with undecidable precedence:old=README.md(std);new=README.md(dotLicense); policy=Dep5First                                                                                
README.md.license                                                                                                        WARNING       null          L1            README.md.license is not targeted by any file header (std,.license,dep5)                                                                                                                                
LICENSES/CC-BY-ND-4.0.txt                                                                                                INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
LICENSES/CC0-1.0.txt                                                                                                     INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
LICENSES/MIT.txt                                                                                                         INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
*: blockedLevel refers to the lowest PCRLT badge/level (L1=Bronze, L2=Silver, L3=Gold) whose requirements are not met.

Unfortunately, the Git repository is currently not compliant with PCRLTv0.4 :-(
(See PCRLT report spdx-utils-output/pcrlt.txt in the target repository)

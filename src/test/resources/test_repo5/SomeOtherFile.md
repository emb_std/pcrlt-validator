<!--
SPDX-FileCopyrightText: 2022 J. D.

SPDX-License-Identifier: MIT

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo5.git@master#SomeOtherFile.md

Note-PCRLT:J.4 D.:specify-identity-contact:john@example.com
Note-PCRLT:J.4 D.:specify-identity-abbreviation:J. D.

Note-PCRLT:J.4 D.:author:2021 J. D.
Note-PCRLT:J.4 D.:author:2022 J. D.
Note-PCRLT:authors:<text>
  2020 A.B.
  2021 B.C.
</text>
Note-PCRLT:J.4 D.:author-background:some note
Note-PCRLT:B.C.:map-author-to-git-commits:all commits authored/committed by B.C. regarding README.md
Note-PCRLT:B.C.:map-author-to-git-commits:error test - all commits authored/committed by B.C. regarding README.md
-->
test

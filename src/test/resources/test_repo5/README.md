<!--
SPDX-FileCopyrightText: 2022 John Doe

SPDX-License-Identifier: MIT

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo5.git@master#README.md
Note-PCRLT:src-trace-repository:2020:public:git+https://example.com/somePath.git@master#README.md

Note-PCRLT:J..n D.:specify-identity-name:John Doe
Note-PCRLT:J..n D.:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo5.git@master#README.md
Note-PCRLT:J..n D.:map-copyright-holder-to-git-commits:all commits authored/committed by J..n D. regarding README.md

Note-PCRLT:MIT:map-license-to-git-commits:same as "J..n D.:map-copyright-holder-to-git-commits"

Note-PCRLT:J..n D.:personal-data-policy:name-allowed,abbreviation-allowed
Note-PCRLT:J..n D.:copyright-holder-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy

Note-PCRLT:MIT:license-background:<text>some multi-line note regarding the history of the license annotation</text>
Note-PCRLT:J..n D.:copyright-background: some note regarding the history of the rights holder annotation
-->
# test_repo5
In this test repository all files have attached REUSE annotations (std, .license, dep5) and PCRLT annotations.
The following PCRLT keywords are used: 
* compliant-with-spec,
* src-trace-repository,
* specify-identity,
* resolve-git-identity,
* map-copyright-holder-to-git-commits,
* map-license-to-git-commits,
* personal-data-policy, name-allowed, abbreviation-allowed,
* copyright-holder-add-remove-modify-group-policy, no-permissions, conditions, personal-data-policy,
* resolve-identity, (see .hiddenfile2.license)
* license-background (new in test_repo5)
* copyright-background (new in test_repo5)

Note: The .java files show the usage with and without spaces. (like test_repo4)

Note: In README.md and .hiddenTestFile2.license single-line and multi-line values are used.

Note: In README.md src-trace-repository list view is used and in Makefile src-trace-repository graph view is used

The pcrlt-validator is called with all-flag (this includes validation and spdx file generation).

The expected outcome can be seen in spdx-utils-output.

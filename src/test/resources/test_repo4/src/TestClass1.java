// SPDX-FileCopyrightText: 2022 John Doe
// 
// SPDX-License-Identifier: MIT
// 
// Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
// Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
// Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt
// 
// Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#src/TestClass1.java
// 
// Note-PCRLT:J..n D.:specify-identity:John Doe
// Note-PCRLT:J..n D.:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#src/TestClass1.java
// Note-PCRLT:J..n D.:map-copyright-holder-to-git-commits:all commits authored/committed by J..n D. regarding TestClass1.java
// 
// Note-PCRLT:MIT:map-license-to-git-commits:same as "J..n D.:map-copyright-holder-to-git-commits"
// 
// Note-PCRLT:J..n D.:personal-data-policy:name-allowed,abbreviation-allowed
// Note-PCRLT:J..n D.:copyright-holder-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy

some sourcecode

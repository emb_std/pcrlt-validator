<!--
SPDX-FileCopyrightText: 2022 John Doe

SPDX-License-Identifier: MIT

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#README.md

Note-PCRLT:J..n D.:specify-identity-name:John Doe
Note-PCRLT:J..n D.:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#README.md
Note-PCRLT:J..n D.:map-copyright-holder-to-git-commits:all commits authored/committed by J..n D. regarding README.md

Note-PCRLT:MIT:map-license-to-git-commits:same as "J..n D.:map-copyright-holder-to-git-commits"

Note-PCRLT:J..n D.:personal-data-policy:name-allowed,abbreviation-allowed
Note-PCRLT:J..n D.:copyright-holder-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy
-->
# test_repo4
In this test repository all files have attached REUSE annotations (std, .license, dep5) and PCRLT annotations.
The following PCRLT keywords are used: 
* compliant-with-spec, 
* src-trace-repository,
* specify-identity,
* resolve-git-identity
* map-copyright-holder-to-git-commits
* map-license-to-git-commits
* personal-data-policy, name-allowed, abbreviation-allowed (new in test_repo4)
* copyright-holder-add-remove-modify-group-policy, no-permissions, conditions, personal-data-policy (new in test_repo4)

Note: The .java files show the usage with and without spaces. (like test_repo3)

The pcrlt-validator is called with validation-flag only. (like test_repo3)

The expected outcome can be seen in spdx-utils-output.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: test_repo4
Upstream-Contact: John Doe
Source: https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4

# Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
# Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
# Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt

# Note-PCRLT:begin-file-header-for: .reuse/dep5
# SPDX-FileCopyrightText: 2022 John Doe
# 
# SPDX-License-Identifier: MIT
# 
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#.reuse/dep5
# 
# Note-PCRLT:J..n D.:specify-identity:John Doe
# Note-PCRLT:J..n D.:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#.reuse/dep5
# Note-PCRLT:J..n D.:map-copyright-holder-to-git-commits:all commits authored/committed by J..n D. regarding dep5
# 
# Note-PCRLT:MIT:map-license-to-git-commits:same as "J..n D.:map-copyright-holder-to-git-commits"
# Note-PCRLT:end-file-header-for: .reuse/dep5

Files: LICENSES/CC0-1.0.txt LICENSES/CC-BY-ND-4.0.txt
Copyright: 2002-2013 Creative Commons Corporation <creativecommons.org>
License: CC-BY-ND-4.0
# Note-PCRLT:begin-file-header-for: LICENSES/CC0-1.0.txt
# SPDX-FileCopyrightText: 2002-2013 Creative Commons Corporation <creativecommons.org>
# SPDX-License-Identifier: CC-BY-ND-4.0
# Note-PCRLT:external-annotations:sha1:82da472f6d00dc5f0a651f33ebb320aa9c7b08d0:dep5:https://gitlab.com/emb_std/pcrlt-licenses.git#LICENSES/CC0-1.0.txt
# Note-PCRLT:end-file-header-for: LICENSES/CC0-1.0.txt

# Note-PCRLT:begin-file-header-for: LICENSES/CC-BY-ND-4.0.txt
# SPDX-FileCopyrightText: 2002-2013 Creative Commons Corporation <creativecommons.org>
# SPDX-License-Identifier: CC-BY-ND-4.0
# Note-PCRLT:external-annotations:sha1:aae6c07c13602c2ea3676f57fd754aac2f256aa8:dep5:https://gitlab.com/emb_std/pcrlt-licenses.git#LICENSES/CC-BY-ND-4.0.txt
# Note-PCRLT:end-file-header-for: LICENSES/CC-BY-ND-4.0.txt

Files: LICENSES/MIT.txt
Copyright: 1987-1999 Massachusetts Institute of Technology and/or Contributors
License: CC-BY-ND-4.0
# Note-PCRLT:begin-file-header-for: LICENSES/MIT.txt
# SPDX-FileCopyrightText: 1987-1999 Massachusetts Institute of Technology and/or Contributors
# SPDX-License-Identifier: CC-BY-ND-4.0
# Note-PCRLT:external-annotations:sha1:b8efe7d153cc6e0ba9e9ddca76c23a274dae8ab8:dep5:https://gitlab.com/emb_std/pcrlt-licenses.git#LICENSES/MIT.txt
# Note-PCRLT:end-file-header-for: LICENSES/MIT.txt

Files: someFile.txt
Copyright: 2006 John Doe
           1998 Jane Smith
License: CC-BY-ND-4.0
# Note-PCRLT:begin-file-header-for: someFile.txt
# SPDX-FileCopyrightText: 2006 John Doe
# SPDX-FileCopyrightText: 1998 Jane Smith

# SPDX-License-Identifier: CC-BY-ND-4.0

# Note-PCRLT:src-trace-repository:2022:public: git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#someFile.txt

# Note-PCRLT:alice:specify-identity-name:alice
# Note-PCRLT:alice:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#someFile.txt

# Note-PCRLT:J.D.3:specify-identity-name:John Doe
# Note-PCRLT:J.S.5:specify-identity-name:Jane Smith

# Note-PCRLT:CC-BY-ND-4.0:license-background:<text>
# some note
# </text>
# Note-PCRLT:J.D.3 AND J.S.5:map-copyright-holder-to-git-commits:all commits authored/committed by alice regarding someFile.txt
# Note-PCRLT:J.D.3:map-copyright-holder-to-git-commits:same as "J.D.3 AND J.S.5:map-copyright-holder-to-git-commits"
# Note-PCRLT:J.S.5:map-copyright-holder-to-git-commits:same as "J.D.3 AND J.S.5:map-copyright-holder-to-git-commits"
# Note-PCRLT:CC-BY-ND-4.0:map-license-to-git-commits:same as "J.D.3 AND J.S.5:map-copyright-holder-to-git-commits"
# Note-PCRLT:end-file-header-for: someFile.txt

Files: .hiddenTestFile
Copyright: 2022 John Doe
License: MIT
# Note-PCRLT:begin-file-header-for: .hiddenTestFile
# SPDX-FileCopyrightText: 2022 John Doe
# SPDX-License-Identifier: MIT
# 
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#.hiddenTestFile
# 
# Note-PCRLT:J..n D.:specify-identity:John Doe
# Note-PCRLT:J..n D.:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#.hiddenTestFile
# Note-PCRLT:J..n D.:map-copyright-holder-to-git-commits:all commits authored/committed by J..n D. regarding .hiddenTestFile
# 
# Note-PCRLT:MIT:map-license-to-git-commits:same as "J..n D.:map-copyright-holder-to-git-commits"
# Note-PCRLT:end-file-header-for: .hiddenTestFile

Files: .hiddenTestFile2.license
Copyright: 2022 John Doe
License: MIT
# Note-PCRLT:begin-file-header-for: .hiddenTestFile2.license
# SPDX-FileCopyrightText: 2022 John Doe
# SPDX-License-Identifier: MIT
# 
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#.hiddenTestFile2.license
# 
# Note-PCRLT:J..n D.:specify-identity:John Doe
# Note-PCRLT:J..n D.:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo4.git@master#.hiddenTestFile2.license
# Note-PCRLT:J..n D.:map-copyright-holder-to-git-commits:all commits authored/committed by J..n D. regarding .hiddenTestFile2.license
# 
# Note-PCRLT:MIT:map-license-to-git-commits:same as "J..n D.:map-copyright-holder-to-git-commits"
# Note-PCRLT:end-file-header-for: .hiddenTestFile2.license

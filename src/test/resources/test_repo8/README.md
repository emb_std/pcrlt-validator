<!--
SPDX-FileCopyrightText: 2022 John Doe

SPDX-License-Identifier: MIT

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo8.git@master#README.md

Note-PCRLT:J..n D.:specify-identity-name:John Doe
Note-PCRLT:J..n D.:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo8.git@master#README.md
Note-PCRLT:J..n D.:map-copyright-holder-to-git-commits:all commits authored/committed by J..n D. regarding README.md

Note-PCRLT:MIT:map-license-to-git-commits:same as "J..n D.:map-copyright-holder-to-git-commits"

Note-PCRLT:J..n D.:personal-data-policy:name-allowed,abbreviation-allowed
Note-PCRLT:J..n D.:copyright-holder-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy

Note-PCRLT:MIT:license-background:<text>some multi-line note regarding the history of the license annotation</text>
Note-PCRLT:J..n D.:copyright-background: some note regarding the history of the rights holder annotation
-->
# test_repo8
In this test repository all files have attached REUSE annotations (std, .license, dep5) and PCRLT annotations.
The purpose of this repo is to test the application in case of multiple file headers refering to one file (which should be governed by the multi-file-header commandline argument).
In this repo the expected pcrlt files with appendix "_Dep5First" refer to --multiple-file-header-policy=Deb5First.
In this repo the expected pcrlt files with appendix "_OneHeaderPerFile" refer to --multiple-file-header-policy=OneHeaderPerFile (default).

The pcrlt-validator is called with all-flag (this includes validation and spdx file generation).

The expected outcome can be seen in spdx-utils-output.

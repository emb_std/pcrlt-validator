Copyright Holders:
name                                                         <contact>                                         : numberOfFiles
John Doe                                                     <null>                                            : 9            
Creative Commons Corporation                                 <creativecommons.org>                             : 2            
Massachusetts Institute of Technology and/or Contributors    <null>                                            : 1            
Jane Doe                                                     <null>                                            : 1            

License Expressions:
License Expression,                                          nFiles*,Files
MIT                                                          8       .hiddenTestFile,.hiddenTestFile2,.hiddenTestFile2.license,.reuse/dep5,README.md,spdx-utils-input/spdx_document_template.txt,src/TestClass1.java,src/TestClass2.java
CC-BY-ND-4.0                                                 3       LICENSES/CC-BY-ND-4.0.txt,LICENSES/CC0-1.0.txt,LICENSES/MIT.txt
CC0-1.0                                                      2       .gitignore,Makefile
*: nFiles=Number of Files

PCRLT Identities:
abbrId                                                       name                                                         <contact>                                         : numberOfFiles
J..n D.                                                      John Doe                                                     <>                                                : 8            
Creative Commons Corporationcreativecommons.org              Creative Commons Corporation                                 <creativecommons.org>                             : 2            
John Doe                                                     John Doe                                                     <>                                                : 1            
D.3                                                          Mr. Doe                                                      <>                                                : 1            
J..n D.                                                      null                                                         <>                                                : 1            
Massachusetts Institute of Technology and/or Contributors    Massachusetts Institute of Technology and/or Contributors    <>                                                : 1            
Jane Doe                                                     Jane Doe                                                     <>                                                : 1            

Distinct repositories from src-trace-repository annotations:
git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo8.git@master

Distinct repositories from resolve-git-identity annotations:
git+https://gitlab.com/emb_std/pcrlt-validator/src/test/resources/test_repo8.git@master

PCRLT File Scores:
file                                                                                                                    (Score1,Score2,RH,Li,Sp,Sr,IG,RH2C,IGwR,IGwA,L2C)* specVersions                  
.gitignore                                                                                                               3      255    1  1  3  1  3  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile                                                                                                          3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile2.license                                                                                                 3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.reuse/dep5                                                                                                              3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
Makefile                                                                                                                 3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
spdx-utils-input/spdx_document_template.txt                                                                              3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
src/TestClass1.java                                                                                                      3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
src/TestClass2.java                                                                                                      3      255    1  1  3  1  1  1    1    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
.hiddenTestFile2                                                                                                         3      253    1  1  3  1  1  1    0    0    1     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/CC-BY-ND-4.0.txt                                                                                                3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/CC0-1.0.txt                                                                                                     3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
LICENSES/MIT.txt                                                                                                         3      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
README.md                                                                                                                2      232    1  1  3  0  1  0    0    0    0     SPDXv2.2 REUSEv3.0 PCRLTv0.3  
*:Score1=Badge level,Score2=Feature Score,RH=RightsHolders,Li=Licenses,Sp=Specifications(SPDX,REUSE,PCRLT),Sr=Sources/Urls,IG=IdentityGroups,RightsHolders2CommitMappings,IGwR=IdentityGroups with repository,IGwA=IdentityGroups with authorAnnotation,L2C=License2CommitMappings

File Statistics:                             nFiles,  Size[KB],    nLines,      n80CharLines 
  All Files:                                 13       39.521       552          424.10      
  Files grouped by LICENSES folder:          
    Files in LICENSES folder:                3        25.077       284          256.49      
    Files not in LICENSES folder:            10       14.444       268          167.61      
      Files grouped by file type (TEXT,BINARY):
        Files of type TEXT:                  10       14.444       268          167.61      
      Files grouped by file extension:       
        Files with extension None:           5        8.079        158          94.29       
        Files with extension ".txt":         1        1.553        26           17.76       
        Files with extension ".license":     1        0.990        18           11.38       
        Files with extension ".java":        2        1.857        34           21.63       
        Files with extension ".md":          1        1.965        32           22.56       

PCRLT File Header Statistics (where the number of records (fileHeaders) is 13):
          badgeLevel,nSpecs,nSrcs,nLics,nL2Commits,nLBackgrounds,nRHAnnotations,nRhs, nRHBackgrounds,nRH2Commits,nIdentityGroups,nIdentities,nIdentiesExplicitly,nResolveAnnotations,nMap2GitIdAnnotations,nAuthorAnnotations,nAuthors,nAuthorBackgrounds,nAuthor2Commits,nPersDataPolicies,nAddEtcPolicies,nExtAnnotations
  Sum     38         39     9     13    9          4             20             13    1              9           15              15          9                   8                   2                     0                  0        0                  0               0                 0               3              
  Max     3          3      1     1     1          1             2              1     1              1           3               3           1                   1                   1                     0                  0        0                  0               0                 0               1              
  AVR     2.923      3.000  0.692 1.000 0.692      0.308         1.538          1.000 0.077          0.692       1.154           1.154       0.692               0.615               0.154                 0.000              0.000    0.000              0.000           0.000             0.000           0.231          
  Min     2          3      0     1     0          0             1              1     0              0           1               1           0                   0                   0                     0                  0        0                  0               0                 0               0              

PCRLT Issues:
file                                                                                                                     severity      lino          blockedLevel* issueText                                                                                                                                                                                               
LICENSES/CC-BY-ND-4.0.txt                                                                                                INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
LICENSES/CC0-1.0.txt                                                                                                     INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
LICENSES/MIT.txt                                                                                                         INFO          null          NONE          badgeLevel3 is assumed for the file header since it contains external annotations, but verification of this assumption is not currently implemented.: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false
README.md                                                                                                                INFO          null          L3            something missing for badgeLevel3: isRightsHsResolvable=true isSrcMatchFile=false isSrcMatchUrl=false checkIssues=true                                                                                  
*: blockedLevel refers to the lowest PCRLT badge/level (L1=Bronze, L2=Silver, L3=Gold) whose requirements are not met.

Congratulations! The Git repository is compliant with the *Silver* badge requirements of version 0.4 of the PCRLT specification :-)
Note: The PCRLT specification has 3 badges (Bronze, Silver, Gold) to evaluate how well the repository is documented according to the PCRLT spec.
Note: PCRLT stands for Privacy-aware Content, Rights Holder and License Tracing.

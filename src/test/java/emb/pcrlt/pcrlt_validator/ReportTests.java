//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/ReportTests.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/ReportTests.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding ReportTests.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import emb.helper.HelperI;
import emb.pcrlt.pcrlt_validator.Main.MFHPolicy;
import net.lingala.zip4j.ZipFile;
import picocli.CommandLine;

public class ReportTests {
  private final String testRepoPackageVers = "0.1.0-SNAPSHOT";

  @Test
  public void validate1() throws IOException {// no annotations
    final String repoDirRes = "test_repo1";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithVerifyFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate1b() throws IOException {// no annotations no .reuse/dep5 file
    final String repoDirRes = "test_repo1b";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final CommandLine cmd = new CommandLine(new Main());
    final int exitCode = executeWithVerifyFlag(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate1c() throws IOException {// no annotations. .hiddenTestFile in git index but not in repo dir
    final String repoDirRes = "test_repo1c";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final CommandLine cmd = new CommandLine(new Main());
    final int exitCode = executeWithAllFlag(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkSpdx(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate2() throws IOException {// only reuse annotations
    final String repoDirRes = "test_repo2";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithVerifyFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate2b() throws IOException {// only reuse annotations
    final String repoDirRes = "test_repo2b";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithVerifyFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate3() throws IOException {// pcrlt annotations without personal-data-policy and group policy,
                                              // spaces(see java1,java2)
    final String repoDirRes = "test_repo3";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithVerifyFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate4() throws IOException {// pcrlt annotations with personal-data-policy and copyright holder add
                                              // remove modify group policy
    final String repoDirRes = "test_repo4";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithVerifyFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate5() throws IOException {// pcrlt annotations like validate4() and background notes (incl
                                              // mulit-line), map-to-git-identity and spdx generation
    final String repoDirRes = "test_repo5";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithAllFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkSpdx(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate6() throws IOException {// identities with AND operator, binary file included.
    final String repoDirRes = "test_repo6";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithAllFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkSpdx(repoDir, "");
    checkJson(repoDir, "");
    teardown(repoDir);// teardown repo
  }

  @Test
  public void validate7() throws IOException {// like 6 but trigger a lot issues for testcoverage
    final String repoDirRes = "test_repo7";// repo dir in test resource folder
    final Path repoDir = prepareRepoDir(repoDirRes);// repo dir as tmp folder
    final int exitCode = executeWithAllFlag(new CommandLine(new Main()), repoDir, repoDirRes);
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, "");
    checkSpdx(repoDir, "");
    checkJson(repoDir, "");
    checkCsv(repoDir, "");
    teardown(repoDir);// teardown repo
  }
  /*
   * @Test public void exception1() throws IOException { final String repoDirRes =
   * "test_repo8";// repo dir in test resource folder final Path repoDir =
   * prepareRepoDir(repoDirRes);// repo dir as tmp folder Main main = new Main();
   * CommandLine cmd = new CommandLine(main); cmd.setExecutionExceptionHandler(new
   * IExecutionExceptionHandler() {
   * 
   * @Override public int handleExecutionException(Exception ex, CommandLine
   * commandLine, ParseResult parseResult) throws Exception { //throw ex;
   * assertEquals("only one file header allowed:old=README.md(std);new=README.md(dep5); policy=OneHeaderPerFile"
   * , ex.getMessage()); return 1; }
   * 
   * }); int exitCode = executeWithAllFlag(cmd, repoDir); assertEquals(1,
   * exitCode);
   * HelperI.DefaultHelper.helper.deleteDirRecursively(repoDir.toString(), 110); }
   */

  @Test
  public void validate8() throws IOException {
    final String repoDirRes = "test_repo8";// repo dir in test resource folder
    final String appendix = "_Dep5First";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlagAndMfhPolicy(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.Dep5First, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  @Test
  public void validate8b() throws IOException {
    final String repoDirRes = "test_repo8";// repo dir in test resource folder
    final String appendix = "_Dep5First_skip";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlagAndMfhPolicyAndSkip(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.Dep5First, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  @Test
  public void validate9() throws IOException {
    final String repoDirRes = "test_repo8";// repo dir in test resource folder//test_repo8 is reused but get its own dir
                                           // tmp_test_repo9
    final String appendix = "_OneHeaderPerFile";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlag(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.OneHeaderPerFile, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  @Test
  public void validate9b() throws IOException {
    final String repoDirRes = "test_repo8";// repo dir in test resource folder//test_repo8 is reused but get its own dir
                                           // tmp_test_repo9
    final String appendix = "_OneHeaderPerFile_skip";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlagAndSkip(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.OneHeaderPerFile, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  @Test
  public void validate10() throws IOException {
    final String repoDirRes = "test_repo10";// repo dir in test resource folder
    final String appendix = "_OneHeaderPerFile";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlag(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.OneHeaderPerFile, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  @Test
  public void validate10b() throws IOException {
    final String repoDirRes = "test_repo10";// repo dir in test resource folder
    final String appendix = "_OneHeaderPerFile_skip";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlagAndSkip(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.OneHeaderPerFile, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  @Test
  public void validate11() throws IOException {
    final String repoDirRes = "test_repo10";// repo dir in test resource folder
    final String appendix = "_Dep5First";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlagAndMfhPolicy(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.Dep5First, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  @Test
  public void validate11b() throws IOException {
    final String repoDirRes = "test_repo10";// repo dir in test resource folder
    final String appendix = "_Dep5First_skip";
    final Path repoDir = prepareRepoDir(repoDirRes, appendix);// repo dir as tmp folder
    final Main main = new Main();
    final CommandLine cmd = new CommandLine(main);
    final int exitCode = executeWithAllFlagAndMfhPolicyAndSkip(cmd, repoDir, repoDirRes);
    Assertions.assertEquals(MFHPolicy.Dep5First, main.getMfhPolicy());
    Assertions.assertEquals(0, exitCode);
    checkReport(repoDir, appendix);
    checkSpdx(repoDir, appendix);
    checkJson(repoDir, appendix);
    teardown(repoDir);
  }

  private static Path prepareRepoDir(final String repoDirRes) throws IOException {
    return prepareRepoDir(repoDirRes, "");
  }

  private static Path prepareRepoDir(final String repoDirRes, final String appendix) throws IOException {
    final URL origDirURL = HelperI.DefaultHelper.helper.getResource(repoDirRes, ReportTests.class, false);// copy source
    final Path destDir = Files.createDirectory(Path.of("tmp_" + repoDirRes + appendix));// copy destination
    HelperI.DefaultHelper.helper.copyResourcesRecursively(origDirURL, destDir);// copy
    final Path repoDir = destDir;// repo dir as tmp folder
    final ZipFile zip = new ZipFile(repoDir.toString() + "/.git.zip");
    zip.extractAll(repoDir.toString());
    zip.getFile().delete();
    zip.close();
    final File gitignore = new File(repoDir.toString() + "/.gitignore.deactivated");
    if (gitignore.exists()) {
      gitignore.renameTo(new File(repoDir.toString() + "/.gitignore"));
    }
    return repoDir;
  }

  private int executeWithVerifyFlag(final CommandLine cmd, final Path repoDir, final String packageName) {
    return cmd.execute(repoDir.toString(), "-v", "--package-name", packageName, "--package-version",
        this.testRepoPackageVers, "--spdx-version","2.2");
  }

  private int executeWithAllFlag(final CommandLine cmd, final Path repoDir, final String packageName) {
    return cmd.execute(repoDir.toString(), "-a", "--package-name", packageName, "--package-version",
        this.testRepoPackageVers, "--spdx-version","2.2");
  }

  private int executeWithAllFlagAndMfhPolicy(final CommandLine cmd, final Path repoDir, final String packageName) {
    return cmd.execute(repoDir.toString(), "-a", "--multiple-file-header-policy", "Dep5First", "--package-name",
        packageName, "--package-version", this.testRepoPackageVers, "--spdx-version","2.2");
  }

  private int executeWithAllFlagAndMfhPolicyAndSkip(final CommandLine cmd, final Path repoDir,
      final String packageName) {
    return cmd.execute(repoDir.toString(), "-a", "--multiple-file-header-policy", "Dep5First",
        "--skip-parsing-unused-file-headers", "--package-name", packageName, "--package-version",
        this.testRepoPackageVers, "--spdx-version","2.2");
  }

  private int executeWithAllFlagAndSkip(final CommandLine cmd, final Path repoDir, final String packageName) {
    return cmd.execute(repoDir.toString(), "-a", "--skip-parsing-unused-file-headers", "--package-name", packageName,
        "--package-version", this.testRepoPackageVers, "--spdx-version","2.2");
  }

  private static void checkReport(final Path repoDir, final String appendix) throws IOException {
    final String expectedReport = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt_expected" + appendix + ".txt");
    final String actualReport = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt.txt");
    Assertions.assertEquals(expectedReport, actualReport);
    final String expectedJson = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt_expected" + appendix + ".json");
    final String actualJson = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt.json");
    Assertions.assertEquals(expectedJson, actualJson);

  }

  private static void checkSpdx(final Path repoDir, final String appendix) throws IOException {
    final String expectedSpdx = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt_expected" + appendix + ".spdx");
    final String actualSpdx = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt.spdx");
    Assertions
        .assertEquals(
            expectedSpdx.lines()
                .filter(
                    l -> !l.contains("DocumentNamespace: http://spdx.org/spdxdocs/spdx-v") && !l.contains("Created: 2"))
                .collect(Collectors.joining("\n")),
            actualSpdx.lines()
                .filter(
                    l -> !l.contains("DocumentNamespace: http://spdx.org/spdxdocs/spdx-v") && !l.contains("Created: 2"))
                .collect(Collectors.joining("\n")));
  }

  private static void checkJson(final Path repoDir, final String appendix) throws IOException {
    final String expectedJson = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt_expected" + appendix + ".json");
    final String actualJson = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt.json");
    Assertions.assertEquals(expectedJson, actualJson);
  }

  private static void checkCsv(final Path repoDir, final String appendix) throws IOException {
    final String expectedJson = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt_expected" + appendix + ".csv");
    final String actualJson = HelperI.DefaultHelper.helper
        .getFileAsString(repoDir.toString() + "/spdx-utils-output/pcrlt.csv");
    Assertions.assertEquals(expectedJson, actualJson);
  }

  private static void teardown(final Path repoDir) throws IOException {
    teardown(repoDir, 200);
  }

  private static void teardown(final Path repoDir, final int maxFiles) throws IOException {
    HelperI.DefaultHelper.helper.deleteDirRecursively(repoDir.toString(), maxFiles);
  }

}

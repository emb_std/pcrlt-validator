//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/IdentityControllerTests.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/IdentityControllerTests.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding IdentityControllerTests.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.google.gson.GsonBuilder;

import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentityGroup;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityParams;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH.ReuseRightsHParams;

public class IdentityControllerTests extends TestInitialization {
  String expected1 = "{\n" + // @formatter:off
      "  \"John Doe\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"John Doe\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"John Doe\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"John Doe\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected2="{\n" + 
      "  \"J.D.\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"J.D.\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": false,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"J.D.\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"J.D.\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected3="{\n" + 
      "  \"J.D.\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"J.D.\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": false,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"J.D.\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"J.D.\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected4="{\n" + 
      "  \"J.D.\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"J.D.\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": false,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"J.D.\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"J.D.\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  },\n" + 
      "  \"John Doe\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"John Doe\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"John Doe\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"John Doe\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected5="{\n" + 
      "  \"John Doe\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"John Doe\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"John Doe\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"John Doe\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  },\n" + 
      "  \"J.D.\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"J.D.\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": false,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"J.D.\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"J.D.\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected6="{\n" + 
      "  \"J.D.\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"J.D.\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [\n" + 
      "          \"c@d\"\n" + 
      "        ],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": false,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"J.D.\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"J.D.\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected7="{\n" + 
      "  \"John Doea@b\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"John Doea@b\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [\n" + 
      "          \"a@b\"\n" + 
      "        ],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"contact\": \"a@b\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"John Doea@b\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"John Doea@b\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  },\n" + 
      "  \"J.D.\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"J.D.\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [\n" + 
      "          \"c@d\"\n" + 
      "        ],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": false,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"J.D.\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"J.D.\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected8="{\n" + 
      "  \"J.D.\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"J.D.\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [\n" + 
      "          \"c@d\"\n" + 
      "        ],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": false,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"contact\": \"c@d\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"J.D.\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"J.D.\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected9="{\n" + 
      "  \"c@d\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"c@d\": {\n" + 
      "        \"contacts\": [\n" + 
      "          \"c@d\"\n" + 
      "        ],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"contact\": \"c@d\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"c@d\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"c@d\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  },\n" + 
      "  \"John Doec@d\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"John Doec@d\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [\n" + 
      "          \"c@d\"\n" + 
      "        ],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"contact\": \"c@d\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"John Doec@d\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"John Doec@d\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";
  String expected10="{\n" + 
      "  \"John Doe\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"John Doe\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"John Doe\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"John Doe\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  },\n" + 
      "  \"John Doec@d\": {\n" + 
      "    \"pcrltIdentities\": {\n" + 
      "      \"John Doec@d\": {\n" + 
      "        \"name\": \"John Doe\",\n" + 
      "        \"contacts\": [\n" + 
      "          \"c@d\"\n" + 
      "        ],\n" + 
      "        \"abbrs\": [],\n" + 
      "        \"isImplicitByAbbrId\": false,\n" + 
      "        \"isImplicitByRightsH\": true,\n" + 
      "        \"isImplicitByAuthor\": false,\n" + 
      "        \"repos\": [],\n" + 
      "        \"resolveDescrs\": [],\n" + 
      "        \"reuseRightsHs\": [\n" + 
      "          {\n" + 
      "            \"name\": \"John Doe\",\n" + 
      "            \"contact\": \"c@d\",\n" + 
      "            \"isDep5\": false,\n" + 
      "            \"parCtx\": {}\n" + 
      "          }\n" + 
      "        ],\n" + 
      "        \"authorAnnotations\": [],\n" + 
      "        \"abbrId\": \"John Doec@d\",\n" + 
      "        \"parCtx\": {}\n" + 
      "      }\n" + 
      "    },\n" + 
      "    \"abbrIdExpr\": \"John Doec@d\",\n" + 
      "    \"parCtx\": {}\n" + 
      "  }\n" + 
      "}";// @formatter:on

  @Test
  void specifyIdentityAfterSpdxCopyrightText_test() {
    final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.name = "John Doe";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr);
    Assertions.assertEquals(this.expected1, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
    final PcrltIdentityParams identityParms = new PcrltIdentityParams();
    identityParms.abbrId = "J.D.";
    identityParms.name = "John Doe";
    final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pid);
    Assertions.assertEquals(this.expected5, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
    IdentityController.mergePcrltIdentities(pcrltIdentityGroups, null, null);
    Assertions.assertEquals(this.expected2, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
  }

  @Test
  void spdxCopyrightTextAfterspecifyIdentity_test() {
    final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final PcrltIdentityParams identityParms = new PcrltIdentityParams();
    identityParms.abbrId = "J.D.";
    identityParms.name = "John Doe";
    final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pid);
    Assertions.assertEquals(this.expected3, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.name = "John Doe";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr);
    Assertions.assertEquals(this.expected4, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
    IdentityController.mergePcrltIdentities(pcrltIdentityGroups, null, null);
    Assertions.assertEquals(this.expected2, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
  }

  @Test
  void shouldBeIdentical1_test() {
    final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.name = "John Doe";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr);
    final PcrltIdentityParams identityParms = new PcrltIdentityParams();
    identityParms.abbrId = "J.D.";
    identityParms.name = "John Doe";
    identityParms.contact = "c@d";
    final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pid);
    IdentityController.mergePcrltIdentities(pcrltIdentityGroups, null, null);
    Assertions.assertEquals(this.expected6, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
  }

  @Test
  void shouldBeDifferent1_test() {
    final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.name = "John Doe";
    rightsHParms.contact = "a@b";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr);
    final PcrltIdentityParams identityParms = new PcrltIdentityParams();
    identityParms.abbrId = "J.D.";
    identityParms.name = "John Doe";
    identityParms.contact = "c@d";
    final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pid);
    IdentityController.mergePcrltIdentities(pcrltIdentityGroups, null, null);
    Assertions.assertEquals(this.expected7, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
  }

  @Test
  void shouldBeIdentical2_test() {
    final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.contact = "c@d";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr);
    final PcrltIdentityParams identityParms = new PcrltIdentityParams();
    identityParms.abbrId = "J.D.";
    identityParms.name = "John Doe";
    identityParms.contact = "c@d";
    final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pid);
    IdentityController.mergePcrltIdentities(pcrltIdentityGroups, null, null);
    Assertions.assertEquals(this.expected8, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
  }

  @Test
  void shouldBeDifferent2_test() {
    final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.contact = "c@d";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr);
    final ReuseRightsHParams rightsHParms2 = new ReuseRightsHParams();
    rightsHParms2.name = "John Doe";
    rightsHParms2.contact = "c@d";
    rightsHParms2.isDep5 = false;
    final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms2);
    final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr2);
    IdentityController.mergePcrltIdentities(pcrltIdentityGroups, null, null);
    Assertions.assertEquals(this.expected9, new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
        .create().toJson(pcrltIdentityGroups));
  }

  @Test
  void shouldBeDifferent3_test() {
    final Map<String, PcrltIdentityGroup> pcrltIdentityGroups = new LinkedHashMap<>();
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.name = "John Doe";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr);
    final ReuseRightsHParams rightsHParms2 = new ReuseRightsHParams();
    rightsHParms2.name = "John Doe";
    rightsHParms2.contact = "c@d";
    rightsHParms2.isDep5 = false;
    final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms2);
    final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
    IdentityController.registerNewPcrltIdentity(pcrltIdentityGroups, pidr2);
    IdentityController.mergePcrltIdentities(pcrltIdentityGroups, null, null);
    Assertions.assertEquals(this.expected10, new GsonBuilder().setPrettyPrinting()
        .excludeFieldsWithoutExposeAnnotation().create().toJson(pcrltIdentityGroups));
  }

  @Test
  void isReferringToSameIdentity1_test() {
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.year = "2020";
    rightsHParms.name = "John Doe";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    final ReuseRightsHParams rightsHParms2 = new ReuseRightsHParams();
    rightsHParms2.year = "2020";
    rightsHParms2.name = "John Doe";
    rightsHParms2.isDep5 = false;
    final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms2);
    final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
    Assertions.assertEquals(true, pidr2.isReferringToSamePcrltIdentity(pidr));
    Assertions.assertEquals(true, pidr.isReferringToSamePcrltIdentity(pidr2));
  }

  @Test
  void isReferringToSameIdentity2_test() {
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.year = "2020";
    rightsHParms.name = "John Doe";
    rightsHParms.contact = "a@b";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    final ReuseRightsHParams rightsHParms2 = new ReuseRightsHParams();
    rightsHParms2.year = "2022";
    rightsHParms2.name = "John Doe";
    rightsHParms2.isDep5 = false;
    final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms2);
    final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
    Assertions.assertEquals(false, pidr2.isReferringToSamePcrltIdentity(pidr));
    Assertions.assertEquals(false, pidr.isReferringToSamePcrltIdentity(pidr2));
  }

  @Test
  void isReferringToSameIdentity3_test() {
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final PcrltIdentityParams identityParms = new PcrltIdentityParams();
    identityParms.abbrId = "J.D.";
    identityParms.name = "John Doe";
    identityParms.contact = "a@b";
    final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
    final ReuseRightsHParams rightsHParms2 = new ReuseRightsHParams();
    rightsHParms2.year = "2022";
    rightsHParms2.name = "John Doe";
    rightsHParms2.isDep5 = false;
    final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms2);
    final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
    Assertions.assertEquals(true, pidr2.isReferringToSamePcrltIdentity(pid));
    Assertions.assertEquals(true, pid.isReferringToSamePcrltIdentity(pidr2));
  }

}

//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/IdentityControllerExceptionTests.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/IdentityControllerExceptionTests.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding IdentityControllerExceptionTests.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity;
import emb.pcrlt.pcrlt_validator.model.PcrltIdentity.PcrltIdentityParams;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH.ReuseRightsHParams;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH;

@Generated // used to skip from code coverage - also working with methods but maybe not
           // reliable
public class IdentityControllerExceptionTests extends TestInitialization {

  @Test
  void integrate1_test() {
    final Throwable exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      identityParms.abbrId = "J.D.";
      identityParms.name = "John Doe";
      identityParms.contact = "a@b";
      final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
      final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
      rightsHParms.text = "SPDX-FileCopyrightText: 2022 John Doe";
      rightsHParms.year = "2022";
      rightsHParms.name = "John Doe";
      rightsHParms.isDep5 = false;
      final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms);
      final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
      pidr2.integrate(pid);
    });
    Assertions.assertEquals("this case is not implemented", exception.getMessage());
  }

  @Test
  void integrate2_test() {
    final Throwable exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      identityParms.abbrId = "J.D.";
      identityParms.name = "John Doe";
      identityParms.contact = "a@b";
      final PcrltIdentity pid = new PcrltIdentity(ctx, identityParms);
      final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
      rightsHParms.text = "SPDX-FileCopyrightText: 2022 John Doe b@b";
      rightsHParms.year = "2022";
      rightsHParms.name = "John Doe";
      rightsHParms.contact = "b@b";
      rightsHParms.isDep5 = false;
      final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms);
      final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
      pidr2.integrate(pid);
    });
    Assertions.assertEquals("wrong use - identities are different", exception.getMessage());
  }

  @Test
  void pcrltIdentity1_test() {
    final Throwable exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
      final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
      final PcrltIdentityParams identityParms = new PcrltIdentityParams();
      @SuppressWarnings("unused")
      final PcrltIdentity pi = new PcrltIdentity(ctx, identityParms);
    });
    Assertions.assertEquals("abbrId==null||abbrId.strip().isEmpty() not allowed", exception.getMessage());
  }

}

//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/HTests.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/HTests.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding HTests.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class HTests {
  @Test
  public void equals_test1() {
    Assertions.assertEquals(true, H.equals(null, null));
    Assertions.assertEquals(true, H.equals(new ArrayList<String>(), new ArrayList<String>()));
    Assertions.assertEquals(true, H.equals(Arrays.asList("test1", "test2"), Arrays.asList("test1", "test2")));
    Assertions.assertEquals(false, H.equals(Arrays.asList("test1", "test3"), Arrays.asList("test1", "test2")));
  }

  @Test
  public void filterMap_test1() {
    final Map<String, Integer> map = new TreeMap<>();
    map.put("a", 1);
    map.put("c", 3);
    map.put("b", 2);
    final List<String> list = new ArrayList<>();
    H.filterMapInplaceAndConsumeTheRemoved(map, k -> k == "b", k -> {
      list.add(k);
    });
    Assertions.assertEquals("b", list.stream().collect(Collectors.joining(",")));

  }
}

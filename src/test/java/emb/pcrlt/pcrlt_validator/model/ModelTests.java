//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/model/ModelTests.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/model/ModelTests.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding ModelTests.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import emb.helper.HelperI;
import emb.pcrlt.pcrlt_validator.Keyword;
import emb.pcrlt.pcrlt_validator.TestInitialization;
import emb.pcrlt.pcrlt_validator.FileHeaderParser.ParCtx;
import emb.pcrlt.pcrlt_validator.model.ReuseRightsH.ReuseRightsHParams;

public class ModelTests extends TestInitialization {
  @Test
  public void filterReuseRigthsHForSPDX_test() {
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.text = "Copyright: 2022 RightsHolder1";
    rightsHParms.year = "2022";
    rightsHParms.name = "RightsHolder1";
    rightsHParms.isDep5 = true;
    final ReuseRightsH r1 = new ReuseRightsH(ctx, rightsHParms);// "Copyright: 2022 RightsHolder1", true);
    // r1.isDep5 = true;
    final ReuseRightsHParams rightsHParms2 = new ReuseRightsHParams();
    rightsHParms2.text = "SPDX-FileCopyrightText: 2022 RightsHolder1";
    rightsHParms2.year = "2022";
    rightsHParms2.name = "RightsHolder1";
    rightsHParms2.isDep5 = false;
    final ReuseRightsH r2 = new ReuseRightsH(ctx, rightsHParms2);// "SPDX-FileCopyrightText: 2022
                                                                 // RightsHolder1", false);
    // r2.isDep5 = false;

    final List<ReuseRightsH> rs = new ArrayList<>();
    rs.add(r1);
    rs.add(r2);
    final String expected = "SPDX-FileCopyrightText: 2022 RightsHolder1";
    Assertions.assertEquals(expected,
        FileHeader.filterReuseRigthsHForSPDX(rs).stream().map(r -> r.getText()).collect(Collectors.joining()));
  }

  @Test
  public void implementedKeywords_test() throws IOException {
    // int actual=PcrltKeyword.getImplementedKeywords().size();
    // assertEquals(expectedSize, actual);
    final String expectedKeywords = HelperI.DefaultHelper.helper.getInputStreamAsString(
        HelperI.DefaultHelper.helper.getResourceAsStream("implementedKeywords.txt", this.getClass()));
    final String actualKeywords = Keyword.getImplementedKeywords().stream().map(c -> "" + c).sorted()
        .collect(Collectors.joining("\n"));
    Assertions.assertEquals(expectedKeywords, actualKeywords);
    final String expectedUIKeywords = HelperI.DefaultHelper.helper.getInputStreamAsString(
        HelperI.DefaultHelper.helper.getResourceAsStream("unimplementedKeywords.txt", this.getClass()));
    final String actualUIKeywords = Keyword.getUnimplementedKeywords().stream().map(c -> "" + c).sorted()
        .collect(Collectors.joining("\n"));
    Assertions.assertEquals(expectedUIKeywords, actualUIKeywords);
  }

  @Test
  public void keyword_test() {
    final String expected = "" + Keyword.spdxFileCopyrightText;
    final String actual = "SPDX-FileCopyrightText";
    Assertions.assertEquals(expected, actual);
  }

  @Test
  public void misc_test() {
    String str = "Note-PCRLT:a:b:::";
    Assertions.assertEquals(3, str.split(":").length);
    str = "Note-PCRLT:a:b::c:";
    Assertions.assertEquals(5, str.split(":").length);
    str = "Note-PCRLT:a:b:::";
    Assertions.assertEquals(6, str.split(":", -1).length);
  }

  @Test
  void isReferringToSameIdentity4_test() {
    final ParCtx ctx = new ParCtx(null, null, null, null, null, null, null);
    final ReuseRightsHParams rightsHParms = new ReuseRightsHParams();
    rightsHParms.year = "2020";
    rightsHParms.name = "John Doe";
    rightsHParms.isDep5 = false;
    final ReuseRightsH rrh = new ReuseRightsH(ctx, rightsHParms);
    final PcrltIdentity pidr = new PcrltIdentity(rrh);
    pidr.setAbbrId("test1");// this is for test coverage - it is unlikely to happen in real.
    final ReuseRightsHParams rightsHParms2 = new ReuseRightsHParams();
    rightsHParms2.year = "2020";
    rightsHParms2.name = "John Doe";
    rightsHParms2.isDep5 = false;
    final ReuseRightsH rrh2 = new ReuseRightsH(ctx, rightsHParms2);
    final PcrltIdentity pidr2 = new PcrltIdentity(rrh2);
    pidr.getAbbrs();// for coverage
    pidr.getAuthorAnnotations();// for coverage
    pidr.getReuseRightsHs();// for coverage
    Assertions.assertEquals(true, pidr2.isReferringToSamePcrltIdentity(pidr));
    Assertions.assertEquals(true, pidr.isReferringToSamePcrltIdentity(pidr2));
  }

}

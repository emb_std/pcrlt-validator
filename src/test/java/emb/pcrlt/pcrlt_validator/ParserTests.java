//SPDX-FileCopyrightText: 2022 embeach
//
//SPDX-License-Identifier: MIT OR Apache-2.0 OR GPL-3.0-or-later
//
//Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
//Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
//Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
//
//Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/ParserTests.java
//
//Note-PCRLT:emb.7:specify-identity-name:embeach
//Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#src/test/java/emb/pcrlt/pcrlt_validator/ParserTests.java
//Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding ParserTests.java
//
//Note-PCRLT:MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

package emb.pcrlt.pcrlt_validator;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import emb.helper.HelperI;
import emb.pcrlt.pcrlt_validator.Main.MFHPolicy;
import emb.pcrlt.pcrlt_validator.model.FileHeader;
import emb.pcrlt.pcrlt_validator.model.FileHeader.FileHeaderType;

public class ParserTests extends TestInitialization {
  @Test
  public void dep5_test1_empty() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v1";
    final String dep5ExpectedPcrltJson = "dep5_v1_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,true);
  }

  @Test
  public void dep5_test2_mini() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v2";
    final String dep5ExpectedPcrltJson = "dep5_v2_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,true);
  }

  @Test
  public void dep5_test3_dublicate1() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v3";
    final String dep5ExpectedPcrltJson = "dep5_v3_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,true);
  }

  @Test
  public void dep5_test4_pcrlt1() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v4";
    final String dep5ExpectedPcrltJson = "dep5_v4_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,false);
  }

  @Test
  public void dep5_test5_pcrlt2() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v5";
    final String dep5ExpectedPcrltJson = "dep5_v5_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,false);
  }

  @Test
  public void dep5_test6_dublicate2() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v6";
    final String dep5ExpectedPcrltJson = "dep5_v6_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,true);
  }

  @Test
  public void dep5_test6_dublicate3() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v6";
    final String dep5ExpectedPcrltJson = "dep5_v6_pcrlt_expected2.json";
    ParserTests.dep5_test_helper(MFHPolicy.Dep5First, dep5FileName, dep5ExpectedPcrltJson,true);
  }

  @Test
  public void dep5_test7_big() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v7";
    final String dep5ExpectedPcrltJson = "dep5_v7_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,false);
  }
  @Test
  public void dep5_test8_multiline_copyright() throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileName = "dep5_v8";
    final String dep5ExpectedPcrltJson = "dep5_v8_pcrlt_expected.json";
    ParserTests.dep5_test_helper(MFHPolicy.OneHeaderPerFile, dep5FileName, dep5ExpectedPcrltJson,false);
  }

  private static void dep5_test_helper(final MFHPolicy mfhPolicy, final String dep5FileName,
      final String dep5ExpectedPcrltJson,boolean isExtended) throws IOException, NoSuchAlgorithmException, ParseException {
    final String dep5FileContent = HelperI.DefaultHelper.helper
        .getInputStreamAsString(HelperI.DefaultHelper.helper.getResourceAsStream(dep5FileName, ParserTests.class));
    final MainLight mainLight = Main.getMainLight();
    FileHeaderParser.parseAndValidateDep5(mainLight.getFileHeaders(), mfhPolicy, dep5FileContent);
    final String act = (isExtended)?"" + mainLight.toStringExtended():mainLight.toString();
    final String exp = HelperI.DefaultHelper.helper.getInputStreamAsString(
        HelperI.DefaultHelper.helper.getResourceAsStream(dep5ExpectedPcrltJson, ParserTests.class));
    Assertions.assertEquals(exp, act);
  }

  @Test
  public void dep5Attributes_test() throws IOException {
    final FileHeader fh = new FileHeader(FileHeaderParser.DEP5_FILE_PATH, FileHeaderType.std);
    final String dep5Str = "Upstream-Name: tool1\n" + "Upstream-Contact: contact1\n" + "Source: source1";
    FileHeaderParser.parseDep5Attributes(fh, Arrays.asList(dep5Str.split("\n")));
    final String exp = HelperI.DefaultHelper.helper.getInputStreamAsString(
        HelperI.DefaultHelper.helper.getResourceAsStream("dep5_attributes_v1_pcrlt_expected.json", ParserTests.class));
    Assertions.assertEquals(exp, "" + fh);
  }

  @Test
  public void regex1() {
    final String file1 = "some/path/to/file.spdx";
    final String file2 = "some/path/to/file.spdxz";
    // String regex1=".*[.]spdx$";
    Assertions.assertEquals(true, FileHeaderParser.isSkipBasedOnPath(file1));// file1.matches(regex1));
    Assertions.assertEquals(false, FileHeaderParser.isSkipBasedOnPath(file2)); // file2.matches(regex1));
  }
}

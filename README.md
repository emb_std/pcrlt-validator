<!--
SPDX-FileCopyrightText: 2021-2022 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#README.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt-validator.git@master#README.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding README.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

REUSE-IgnoreStart
-->
# PCRLT-Validator
Java CLI tool for compliance with [PCRLT](https://gitlab.com/emb_std/pcrlt) recommendations in Git repositories (PCRLT means "Privacy-aware Content, Rights Holder and License Tracing" and extends the [REUSE](https://reuse.software/spec/) recommendations).
The tool can parse PCRLT annotations, create a report with detailed compliance assessment and create an [SPDX](https://spdx.github.io/spdx-spec/) document with copyright and license information for all files.

## Background
[PCRLT](https://gitlab.com/emb_std/pcrlt) means "Privacy-aware Content, Rights Holder and License Tracing" and extends the [REUSE](https://reuse.software/spec/) recommendations. In short: each file in the targeted git repository will be targeted by a so called "file header" that is of one of the following types (same as in REUSE): 
* standard (within the file itself) 
* dotLicense (within accompanied .license file) 
* dep5 (within the central .reuse/dep5 file)

In each file header there are possible annotations. The most important ones are listed below in a simple example:
* `SPDX-FileCopyrightText: 2022 John Doe <jdoe@example.com>`
* `SPDX-License-Identifier: MIT`
* `Note-PCRLT:src-trace-repository:2022:public:git+https://example.git#example-path/file.txt`
* `Note-PCRLT:J.D.:specify-identity:John Doe <jdoe@example.com>`
* `Note-PCRLT:J.D.:resolve-git-identity:git+https://example.git#example-path/file.txt`
* `Note-PCRLT:J.D.:map-copyright-holder-to-git-commits:all commits authored/committed by J.D. regarding file.txt`
* `Note-PCRLT:MIT:map-license-to-git-commits:same as "J.D.:map-copyright-holder-to-git-commits"`

But PCRLT can also cope with more difficult cases were the rights holder name changes or the rights holder has multiple Git identities or multiple rights holders are involved with different licenses were each apply to different commits or different snippets. See [PCRLT](https://gitlab.com/emb_std/pcrlt) for the full spec.

The pcrlt-validator helps to track, validate, score and export these annotations.

## Install
Usually the [spdx-utils](https://gitlab.com/emb_std/spdx-utils)[^spdx-utils] repository should be used to setup the toolchain and prepare the repository.

If this is not desired for some reason, proceed.

It will require Java 8+, maven, git, bash and gnu-make. 
It was never tested on Windows!
It was tested on debian 10 with openjdk-11-jdk as follows:
```console
user@host:~$ sudo apt update
user@host:~$ sudo apt install git maven make bash openjdk-11-jdk
```

Then to install the pcrlt-validator:
```console
user@host:~$ cd some_path
user@host:~/some_path$ git clone https://gitlab.com/emb_std/pcrlt-validator.git
user@host:~/some_path$ cd pcrlt-validator
user@host:~/some_path/pcrlt-validator$ make install
```
The `make install` command will run `mvn install -DskipTests`. Maven can also be called directly, of course.

## Usage
Usually the [spdx-utils](https://gitlab.com/emb_std/spdx-utils) repository should be used to run the software. 

If this is not desired for some reason, proceed.

Suppose the target Git repository (i.e., the one whose compliance is to be assessed) has the path `path-to-target-repo` (which might be relative or absolute),
ensure that the target repository meets the following 4 conditions:
1. It contains `/.tmpCopyFilesForVerificationCode/` in the repository root .gitignore
2. It contains a file `target-repository/spdx-utils-input/spdx_document_template.txt` (can be empty)
3. It contains a folder `spdx-utils-output` for the output files (this is customizable)
4. the repository is backed up (because the tool will write in this folder)

Then the report and the pcrlt.spdx SPDX-document can be generated with the following call:
```shell
user@host:~/some_path/pcrlt-validator$ java -jar target/pcrlt-validator.jar path-to-target-repo -a
```
If no exceptions have been thrown, the files pcrlt.txt, pcrlt.json, pcrlt.csv and pcrlt.spdx will appear in the target repository spdx-utils-output folder.  
Use the report to successively improve the PCRLT level to at least 2 (silver badge) for all files.  
Note that the file `spdx-utils-input/spdx_document_template.txt` in the pcrlt-validator repository can be customized and used for the target repository for the SPDX document generation. See API section for the available template tags (place holders).  
Also note that the `spdx-utils-output` folder of the pcrlt-validator repository can be inspected to see how the output files could look like. Also the `src/test/resources/test_repo*` folders can be used to see what kind of repository outputs which report and with which listed issues.  
See API for further CLI options.  

## API
### CLI
```console
user@host:~/some_path/pcrlt-validator$ java -jar target/pcrlt-validator.jar --help
Usage: <main class> [-achsvV] [--extendedJson]
                    [--skip-parsing-unused-file-headers] [-m=<maxFiles>]
                    [--max-file-header-size=<maxFileHeaderSize>]
                    [--multiple-file-header-policy=<mfhPolicyStr>]
                    [--package-name=<packageName>]
                    [--package-version=<packageVersion>]
                    [--pcrlt-csv-file-out=<pcrltCsvFile>]
                    [--pcrlt-json-file-out=<pcrltJsonFile>]
                    [--pcrlt-report-file-out=<pcrltReportFile>]
                    [--skip-files-for-parsing=<skipFilesRegexForParsing>]
                    [--skip-files-for-verification-code=<skipFilesRegexForSpdxVe
                    rificationCode>]
                    [--spdx-creator-person=<spdxCreatorPerson>]
                    [--spdx-file-out=<spdxDocFile>]
                    [--spdx-version=<spdxVersion>] <repositoryDir>
      <repositoryDir>   the git repository dir.
  -a, --all             like --spdxdoc --validation --create-csv
  -c, --create-csv      Create csv report where each line represents a targeted
                          file.
      --extendedJson    Create extended version of the JSON report (the file
                          name will be the JSON report file name with ".
                          extended.json" extension).
  -h, --help            display a help message
  -m, --max-files=<maxFiles>
                        estimation of the maximal number of files the
                          respository contains. Its a safety parameter to
                          detect path issues and limit damages. Default 30
      --max-file-header-size=<maxFileHeaderSize>
                        Max file Header size as number of lines. This can be
                          used if the contents of the file contain one of the
                          PCRLT or REUSE keywords at the beginning of a line.
                          Default is unlimited.
      --multiple-file-header-policy=<mfhPolicyStr>
                        Policy how the application should handle multiple file
                          headers per file. Default is 'oneHeaderPerFile',
                          which throws an exception if more than one is found.
                          Another option is 'Dep5First', which results in dep5
                          headers taking precedence.
      --package-name=<packageName>
                        Sets the package name of the target repository (this
                          will be used for the spdx file generation). Default
                          is the .reuse/dep5 upstream name if available,
                          otherwise the directory name of the target repository.
      --package-version=<packageVersion>
                        Sets the package version of the target repository (this
                          will be used for the spdx file generation). Default
                          is "unknown-version"
      --pcrlt-csv-file-out=<pcrltCsvFile>
                        File path and name of the PCRLT csv file. Default is
                          spdx-utils-output/pcrlt.csv (relative to target
                          repository dir).
      --pcrlt-json-file-out=<pcrltJsonFile>
                        File name the parsed PCRLT annotations should be saved
                          in (in JSON format). Default is
                          spdx-utils-output/pcrlt.json (relative to target
                          repository dir).
      --pcrlt-report-file-out=<pcrltReportFile>
                        File name the PCRLT report should be saved in. Default
                          is spdx-utils-output/pcrlt.txt (relative to target
                          repository dir).
  -s, --spdxdoc         generate the spdx document template.
      --skip-files-for-parsing=<skipFilesRegexForParsing>
                        Java regex to exclude files from parsing (this is
                          recommended if the file might contain REUSE or PCRLT
                          keywords without a file header). Default is '.*[.]
                          spdx' .
      --skip-files-for-verification-code=<skipFilesRegexForSpdxVerificationCode>
                        Java regex to exclude files from spdx document
                          verification code (this will be used for the spdx
                          file generation). Default is LICENSE.spdx .
      --skip-parsing-unused-file-headers
                        Skip parsing of file headers, that will not be added
                          due to the multi-file-header-policy. This might
                          reduce/hide some issues in combination with Dep5First
                          policy.
      --spdx-creator-person=<spdxCreatorPerson>
                        Set name and optional contact of the creator of the
                          spdx document for the target repository package (this
                          will be used for the spdx file generation). Default
                          is "anonymous ()".
      --spdx-file-out=<spdxDocFile>
                        File name the generated SPDX document should be saved
                          in. Default is spdx-utils-output/pcrlt.spdx (relative
                          to target repository dir).
      --spdx-version=<spdxVersion>
                        SPDX version used in the SPDX document (output file).
                          The validator will raise issues, if a different
                          version is found in source files. Default is 2.3.
  -v, --validation      parse and validate PCRLT annotations and create a json
                          and a report file.
  -V, --version         Print version info from the pcrlt-validator.jar file's
                          /META-INF/MANIFEST.MF and exit
```

### SPDX Document Template API
Available template tags (place holders):
* `<SPDXVersion>`
* `<PackageName>`
* `<PackageVersion>`
* `<UUID>`
* `<SpdxCreatorPerson>`
* `<SpdxCreatorTool>`
* `<Created>`
* `<PcrltValidatorExecutionOptions>`
* `<PackageVerificationCode>`
* `<PackageVerificationCodeExcludedFiles>`
* `<AllFilesSummary>`
* `<LicenseExprTable>`
* `<AllFileInfos>`

Note that the `spdx-utils-input` and `spdx-utils-output` folder of the pcrlt-validator repository can be inspected to see how the spdx_document_template.txt can be used and how the spdx document (pcrlt.spdx) could look like.

## Dependencies
The following maven dependencies are present (2022-09-30):
```console
user@host:~/some_path/pcrlt-validator$ mvn dependency:tree
...
emb.pcrlt:pcrlt-validator:jar:0.2.0-SNAPSHOT
[INFO] +- info.picocli:picocli:jar:4.6.1:compile
[INFO] +- org.eclipse.jgit:org.eclipse.jgit:jar:6.3.0.202209071007-r:compile
[INFO] |  +- com.googlecode.javaewah:JavaEWAH:jar:1.1.13:compile
[INFO] |  \- org.slf4j:slf4j-api:jar:1.7.30:compile
[INFO] +- org.spdx:tools-java:jar:1.1.0:compile
[INFO] |  \- org.spdx:spdx-tagvalue-store:jar:1.1.0:compile
[INFO] +- org.spdx:java-spdx-library:jar:1.1.0:compile
[INFO] +- com.google.code.gson:gson:jar:2.8.9:compile
[INFO] +- org.junit.jupiter:junit-jupiter-api:jar:5.9.0:test
[INFO] |  +- org.opentest4j:opentest4j:jar:1.2.0:test
[INFO] |  +- org.junit.platform:junit-platform-commons:jar:1.9.0:test
[INFO] |  \- org.apiguardian:apiguardian-api:jar:1.1.2:test
[INFO] +- commons-io:commons-io:jar:2.8.0:compile
[INFO] +- net.lingala.zip4j:zip4j:jar:2.11.1:test
[INFO] \- org.apache.commons:commons-csv:jar:1.9.0:compile
```
The pcrlt-validator.jar file (dependencies included) has a size of 4.7MB (with the jgit dependency having the largest share of this).  
The .java files in the pcrlt-validator repository have a total size of about 315KB.

## Limitations
The pcrlt-validator currently has some limitations. Some of them can be addressed by using the reuse-tool from the spdx-utils repository in combination.
* The check whether the LICENSES folder contains all required licenses is currently not implemented. The fsfe reuse-tool can be used for this.
* The DEP-5 syntax in the .reuse/dep5 file is currently not fully supported. 
    * No support of bulk selectors - just space-separated file lists
    * No support of "Copyright" keyword within the Copyright field (e.g. "Copyright: Copyright 2022 John Doe" will cause an error)
    * "License" keyword has to be placed after "Copyright" keyword.
    * If multiple lines are used in the "Copyright" field, each line has to contain a valid copyright holder expression (i.e. `[yearExpr] name [<contact>]`) or only whitespace characters.
    * Only the header fields "Upstream-Name", "Source" and "Upstream-Contact" are parsed. "Upstream-Name" and "Source" are used in validation tasks. Other fields are ignored.
    * "License" fields are expected to be one line SPDX license expressions. Other lines might cause errors.
    * "License" fields with fulltext licenses at the end (after files sections) are not supported and might cause errors.
* Like specified in PCRLTv0.4, the only allowed format for REUSE copyright expressions is `SPDX-FileCopyrightText: [yearExpr] name [<contact>]` (brackets indicate optional parts, yearExpr can contain comma-separated lists and/or year ranges). Multiline expressions are not supported (only the first line will be stored in this case). 
* License expressions are currently treated as strings only - no parsing or validation. The fsfe reuse-tool can be used for validating license expressions.
* By correctly using the keyword "external-annotations" (mentioned in PCRLTv0.4) in a file header, the PCRLT level is assumed to be 3 (gold), but verification of this assumption (i.e. by parsing the remotes) is currently not implemented.
* The "PackageLicenseInfoFromFiles:" statements must be manually inserted into the spdx document template. Automatic generation is not supported - however, it can be copied from reuse.spdx.
* The "LicenseID:" sections must be manually inserted into the spdx document template (if this is desired). Automatic generation is not supported - however, it can be copied from reuse.spdx.
* Besides commits, snippet-level annotations are currently not supported in the PCRLT spec and the pcrlt-validator.

## Maintainer
embeach

## Contributing
TODO

## Disclaimer
This is an experimental software project. Use it at your own risk.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

## License
The repository is compliant with PCRLT[^pcrlt] and REUSE[^reuse] - i.e. copyright and license information are provided on a per-file basis. A summary is provided in LICENSE.spdx as SPDX[^spdx] document. Full license texts can be found in the LICENSES folder. 

The repository contains references to the following licenses: 0BSD, Apache-2.0, CC0-1.0, CC-BY-4.0, CC-BY-ND-4.0, GPL-3.0-or-later, MIT. Whereby most files have multiple license options, i.e. the licensee can choose.  
CC-BY-ND-4.0 is only used for normative files and therefore excluded from the concluded package license. The other files have at least one license option, that is "compatible" with the open source MIT license. Since CC0-1.0 seems to have no additional obligations when combined with e.g. MIT, the license option can be further simplified. The condensed form of the simplest package license options (SPDX PackageLicenseConcluded) is "MIT OR Apache-2.0 OR GPL-3.0-or-later". This means that the licensee can choose any license from the available options. E.g. "MIT". (See LICENSE.spdx for further explanations). 

### MIT terms
"Software" (in the context of the MIT license in this repository) means, for a particular git version of this repository, all files or file parts that are licensed under MIT or that are multi-licensed with an MIT option. To determine which files and file parts are licensed under a particular license or that are multi-licensed with an option for that particular license, the REUSE and PCRLT annotations can be analyzed.  
"Copyright Holders" (in the context of the MIT license in this repository) are all copyright holders that hold copyright in parts of the "Software". The names can be determined by analyzing the REUSE and PCRLT annotations.  

## References
[^spdx]: SPDXv2.3: <https://spdx.github.io/spdx-spec/>  
[^reuse]: REUSEv3.0: <https://reuse.software/spec/>  
[^pcrlt]: PCRLTv0.4: <https://gitlab.com/emb_std/pcrlt>  
[^spdx-utils]: spdx-utils: <https://gitlab.com/emb_std/spdx-utils>
<!--
REUSE-IgnoreEnd
-->
